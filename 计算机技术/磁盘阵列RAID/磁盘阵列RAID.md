# 磁盘阵列RAID

RAID(Redundant Array of Independent Disks)，独立磁盘冗余阵列。

RAID主要利用镜像、数据条带和数据检验三种技术来获取高性能、可靠性、容错能力和扩展性。根据对这三种技术的使用策略和组合架构，可以把RAID分为不同的等级，以满足不用数据应用的要求。

目前业界与学术界公认的标准是RAID0-RAID6，而在实际应用领域中使用最多的RAID等级是RAID0、RAID1、RAID3、RAID5、RAID6和RAID10。



## 镜像技术

镜像技术是一种冗余技术，为磁盘提供数据备份功能，防止磁盘发生故障而造成数据丢失。

采用镜像技术典型的用法就是，在磁盘阵列中产生两个完全一样的数据副本，并且分布在两个不同的磁盘上。

当一个数据副本失效不可用时，外部系统仍可正常访问另一副本，不会对应用系统运行和性能产生影响。而且，镜像不需要额外的计算和检验，故障修复快，直接复制就可以。镜像技术可以从多个副本进行并发读取数据，提供更高的读性能。但写多个副本会导致一定的I/O性能下降。



## 数据条带技术

数据条带化是一种自动将I/O操作负载到多个物理磁盘的技术。具体来说，就是将一块连续的数据分为很多小部分并把它们存储到不同的磁盘上，这样就可以并发读取数据的多个不同部分，极大的提高了I/O性能。



## 数据检验技术

数据检验技术是指，RAID在写入数据的同时进行检验计算，并将得到的检验数据存储在磁盘中。检验数据可以集中保存在某个磁盘或分散在多个不同磁盘中。当其中一部分数据出错时，就可以对剩余数据和检验数据进行反检验计算重建丢失的数据。



## 常见RAID等级详解

### JBOD

JBOD，Just a Bunch of Disks，磁盘簇。表示一个没有控制软件提供协调控制的磁盘集合。JBOD将多个磁盘串联起来，提供一个巨大的逻辑磁盘。

JBOD的数据存放机制是由第一块磁盘开始按顺序往后存储。



### RAID0

RAID0是一种简单的，无数据检验的数据条带化技术。实际上并不是一种真正的RAID，因为它并不提供任何形式的冗余策略。RAID0将所在磁盘条带化后组成大容量的存储空间，将数据分散存储在所有磁盘中，以独立访问方式实现多块磁盘的并读访问。

RAID0不需要进行数据检验，是所有RAID等级中性能最高的。RAID0具有低成本、高读写性能，100%的高存储空间利用率等优点，但是它并不提供数据冗余保护，一旦数据损坏，将无法恢复。

应用场景：对数据的顺序读写要求不高，对数据的安全性和可靠性要求不高，但对系统性能要求很高的场景。



### RAID1

RAID1就是镜像技术，它将数据完全一致的写入到工作镜像和备份镜像，空间利用率为50%。

当工作磁盘发生故障，系统将自动切换到镜像磁盘，不会影响使用。

应用场景：对顺序读写要求高，对数据安全性要求较高的场景。



### RAID10

先做条带，再做镜像。先将进来的数据分散到不同的磁盘，再将磁盘中的数据做镜像。

![img](img/bca06e60e8bda707eaf8f832.jpg)



### RAID01

先做镜像，再做条带。

![img](img/7c57d03940901ab03b87ce3c.jpg)