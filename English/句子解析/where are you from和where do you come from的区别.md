# where are you from和where do you come from的区别？

where are you from？和where do you come from？的意思都是：你来自哪里？

但是

1. 用法不一样。前者是系动词用法，后者是实意动词用法。
2. 语境不同。前者倾向于问你是哪里人。后者倾向于问你从什么地方来。
