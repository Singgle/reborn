# 控制层调用日志记录

> 最近项目进入联调阶段，服务层的接口需要和协议层进行交互，协议层需要将入参[json字符串]组装成服务层所需的json字符串，组装的过程中很容易出错。入参出错导致接口调试失败问题在联调中出现很多次，因此就想写一个请求日志切面把入参信息打印一下，同时协议层调用服务层接口名称对不上也出现了几次，通过请求日志切面就可以知道上层是否有没有发起调用，方便前后端甩锅还能拿出证据



## 请求日志切面编写

使用`@Pointcut`定义切点

```java
@Pointcut("execution(* your_package.controller..*(..))")
public void requestServer() {
}
```

@Pointcut定义了一个切点，匹配的是controller包下的所有类下的方法。



使用@Before在切点前处理

```java
@Before("requestServer()")
public void doBefore(JoinPoint joinPoint) {
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();

    log.info("=================== Start ======================");
    log.info("IP:                {}", request.getRemoteAddr());
    log.info("URL:               {}", request.getRequestURL().toString());
    log.info("HTTP METHOD        {}", request.getMethod());
    log.info("Class Method:      {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
}
```

在进入Controller方法前，打印出调用方ip、请求URL、HTTP请求类型、调用的方法名



使用@Around打印出控制层的入参

```java
@Around("requestServer()")
public Object doAround(ProceedingJoinPoint  proceedingJoinPoint) throws Throwable {
    long start = System.currentTimeMillis();
    Object result = proceedingJoinPoint.proceed();
    log.info("Request Params:     {}", getQequestParams(proceedingJoinPoint));
    log.info("Result:             {}", result);
    log.info("Time Cost:          {}", System.currentTimeMillis() - start);

    return result;
}
```

打印了入参、结果以及耗时

- getRquestParams方法

  ```java
  private Map<String, Object> getQequestParams(ProceedingJoinPoint proceedingJoinPoint) {
      Map<String, Object> requestParams = new HashMap<String, Object>();
  
      //参数名
      String[] paramNames = ((MethodSignature)proceedingJoinPoint.getSignature()).getParameterNames();
      //参数值
      Object[] paramValues = proceedingJoinPoint.getArgs();
  
      for (int i = 0; i < paramNames.length; i++) {
          Object value = paramValues[i];
  
          //如果是文件对象
          if (value instanceof MultipartFile) {
              MultipartFile file = (MultipartFile) paramValues[i];
              value = file.getOriginalFilename(); //获取文件名
          }
  
          requestParams.put(paramNames[i], value);
      }
  
      return requestParams;
  }
  ```

  通过 @PathVariable以及@RequestParam注解传递的参数无法打印出参数名，因此需要手动拼接一下参数名，同时对文件对象进行了特殊处理，只需获取文件名即可



@After方法调用后执行

```java
@After("requestServer()")
public void doAfter(JoinPoint joinPoint) {
    log.info("==================== End ========================");
}
```

没有业务逻辑只是打印了End



完整切面代码

```java
@Component
@Aspect
@Slf4j
public class RequestLogAspect {

  @Pointcut("execution(* your_package.controller..*(..))")
  public void requestServer() {
  }

  @Before("requestServer()")
  public void doBefore(JoinPoint joinPoint) {
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();

    log.info("=================== Start ======================");
    log.info("IP:                {}", request.getRemoteAddr());
    log.info("URL:               {}", request.getRequestURL().toString());
    log.info("HTTP METHOD        {}", request.getMethod());
    log.info("Class Method:      {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
  }

  @Around("requestServer()")
  public Object doAround(ProceedingJoinPoint  proceedingJoinPoint) throws Throwable {
    long start = System.currentTimeMillis();
    Object result = proceedingJoinPoint.proceed();
    log.info("Request Params:     {}", getQequestParams(proceedingJoinPoint));
    log.info("Result:             {}", result);
    log.info("Time Cost:          {}", System.currentTimeMillis() - start);

    return result;
  }

  @After("requestServer()")
  public void doAfter(JoinPoint joinPoint) {
    log.info("==================== End ========================");
  }

  /**
   * 获取入参
   * @param proceedingJoinPoint
   * @return
   */
  private Map<String, Object> getQequestParams(ProceedingJoinPoint proceedingJoinPoint) {
    Map<String, Object> requestParams = new HashMap<String, Object>();

    //参数名
    String[] paramNames = ((MethodSignature)proceedingJoinPoint.getSignature()).getParameterNames();
    //参数值
    Object[] paramValues = proceedingJoinPoint.getArgs();

    for (int i = 0; i < paramNames.length; i++) {
      Object value = paramValues[i];

      //如果是文件对象
      if (value instanceof MultipartFile) {
        MultipartFile file = (MultipartFile) paramValues[i];
        value = file.getOriginalFilename(); //获取文件名
      }

      requestParams.put(paramNames[i], value);
    }

    return requestParams;
  }

}
```



## 高并发下请求日志切面

每个信息打印一行，在高并发请求下会出现请求之间打印日志串行的问题。解决日志串行的问题只要将多行打印信息合并为一行就可以了，因此构造一个对象：

```java
@Data
public class RequestInfo {

  private String ip;
  private String url;
  private String httpMethod;
  private String classMethod;
  private Object requestParams;
  private Object result;
  private Long timeCost;

}
```



环绕通知方法体

```java
@Around("requestServer()")
public Object doAround(ProceedingJoinPoint  proceedingJoinPoint) throws Throwable {
    long start = System.currentTimeMillis();
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();
    Object result = proceedingJoinPoint.proceed();
    RequestInfo requestInfo = new RequestInfo();
    requestInfo.setIp(request.getRemoteAddr());
    requestInfo.setUrl(request.getRequestURL().toString());
    requestInfo.setHttpMethod(request.getMethod());
    requestInfo.setClassMethod(String.format("%s.%s", proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName()));
    requestInfo.setRequestParams(getQequestParamsByProceedingJoinPoint(proceedingJoinPoint));
    requestInfo.setResult(result);
    requestInfo.setTimeCost(System.currentTimeMillis() - start);
    log.info("Request Info:       {}", JSON.toJSONString(requestInfo));

    return result;
}
```

将url、http request这些信息组装成RequestInfo对象，再序列化打印对象
打印**序列化**对象结果而不是直接打印对象是因为序列化有更直观、更清晰，同时可以借助[在线解析工具](https://www.json.cn/)对结果进行解析



在解决高并发下请求串行问题的同时添加了对**异常请求信息的打印**，通过使用 @AfterThrowing注解对抛出异常的方法进行处理

```java
@Data
public class RequestErrorInfo {

  private String ip;
  private String url;
  private String httpMethod;
  private String classMethod;
  private Object requestParams;
  private RuntimeException exception;

}
```

```java
  public void  doAfterThrow(JoinPoint joinPoint, RuntimeException e) {
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();
    RequestErrorInfo requestErrorInfo = new RequestErrorInfo();
    requestErrorInfo.setIp(request.getRemoteAddr());
    requestErrorInfo.setUrl(request.getRequestURL().toString());
    requestErrorInfo.setHttpMethod(request.getMethod());
    requestErrorInfo.setClassMethod(String.format("%s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
    requestErrorInfo.setRequestParams(getQequestParamsByJoinPoint(joinPoint));
    requestErrorInfo.setException(e);
    log.info("Request Error Info:     {}", JSON.toJSONString(requestErrorInfo));
  }
```

对于异常，耗时是没有意义的，因此不统计耗时，而是添加了异常的打印



最后放一下完整日志请求切面代码：

```java
@Component
@Aspect
@Slf4j
public class RequestLogAspect {

  @Pointcut("execution(* your_package.controller..*(..))")
  public void requestServer() {
  }

  @Around("requestServer()")
  public Object doAround(ProceedingJoinPoint  proceedingJoinPoint) throws Throwable {
    long start = System.currentTimeMillis();
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();
    Object result = proceedingJoinPoint.proceed();
    RequestInfo requestInfo = new RequestInfo();
    requestInfo.setIp(request.getRemoteAddr());
    requestInfo.setUrl(request.getRequestURL().toString());
    requestInfo.setHttpMethod(request.getMethod());
    requestInfo.setClassMethod(String.format("%s.%s", proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName()));
    requestInfo.setRequestParams(getQequestParamsByProceedingJoinPoint(proceedingJoinPoint));
    requestInfo.setResult(result);
    requestInfo.setTimeCost(System.currentTimeMillis() - start);
    log.info("Request Info:       {}", JSON.toJSONString(requestInfo));

    return result;
  }

  public void  doAfterThrow(JoinPoint joinPoint, RuntimeException e) {
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();
    RequestErrorInfo requestErrorInfo = new RequestErrorInfo();
    requestErrorInfo.setIp(request.getRemoteAddr());
    requestErrorInfo.setUrl(request.getRequestURL().toString());
    requestErrorInfo.setHttpMethod(request.getMethod());
    requestErrorInfo.setClassMethod(String.format("%s.%s", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));
    requestErrorInfo.setRequestParams(getQequestParamsByJoinPoint(joinPoint));
    requestErrorInfo.setException(e);
    log.info("Request Error Info:     {}", JSON.toJSONString(requestErrorInfo));
  }

  private Map<String, Object> getQequestParamsByProceedingJoinPoint(ProceedingJoinPoint proceedingJoinPoint) {
    Map<String, Object> requestParams = new HashMap<String, Object>();

    //参数名
    String[] paramNames = ((MethodSignature)proceedingJoinPoint.getSignature()).getParameterNames();
    //参数值
    Object[] paramValues = proceedingJoinPoint.getArgs();

    for (int i = 0; i < paramNames.length; i++) {
      Object value = paramValues[i];

      //如果是文件对象
      if (value instanceof MultipartFile) {
        MultipartFile file = (MultipartFile) paramValues[i];
        value = file.getOriginalFilename(); //获取文件名
      }

      requestParams.put(paramNames[i], value);
    }

    return buildRequestParams(paramNames, paramValues);
  }

  private Map<String, Object> getQequestParamsByJoinPoint(JoinPoint joinPoint) {
    //参数名
    String[] paramNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
    //参数值
    Object[] paramValues = joinPoint.getArgs();

    return buildRequestParams(paramNames, paramValues);
  }

  private Map<String, Object> buildRequestParams(String[] paramNames, Object[] paramValues) {
    Map<String, Object> requestParams = new HashMap<String, Object>();
    for (int i = 0; i < paramNames.length; i++) {
      Object value = paramValues[i];

      //如果是文件对象
      if (value instanceof MultipartFile) {
        MultipartFile file = (MultipartFile) value;
        value = file.getOriginalFilename();  //获取文件名
      }

      requestParams.put(paramNames[i], value);
    }

    return requestParams;
  }
```





## 参考资料

[写个日志请求切面，前后端甩锅更方便](https://juejin.cn/post/6844904087964614670)