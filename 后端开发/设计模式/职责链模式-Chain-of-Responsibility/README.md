---
title: 职责链模式(Chain of Responsibility)
tags:
  - 设计模式
categories:
  - 设计模式
date: 2021-06-13 18:17:08
---

## 类图

职责链的类图如下：

![职责链模式类图](README/20210601301.png)

职责链模式包含如下角色：

- 抽象处理者(AbstractHandler)角色：定义一个处理请求的接口，包含抽象处理方法和后继连接
- 具体处理者(ConcreteHandler)角色：实现抽象处理者的处理方法，判断能够处理本次请求，如果能处理则处理请求，否则将请求转给它的后继者。
- 客户端(Client)角色：创建处理链，并将请求提交给链头的具体处理者



## 实例

公司有两个角色，manager和boss。

当员工需要报销，需要向manager提交申请。

manager在500元以下可以同意报销。金额超出或等于500元，需要将报销请求提交给boss。

boss在5000以下可以同意报销。



### Handler

```java
@Data
public abstract class Handler {

    private Handler processor;

    public abstract void handleRequest(int price);

}
```



### Manager

```java
public class Manager extends Handler {

    @Override
    public void handleRequest(int price) {
        if (price < 500) {
            System.out.println("经理同意报销成功");
        } else {
            if (getProcessor() != null) {
                getProcessor().handleRequest(price);
            }
        }
    }

}
```



### Boss

```java
public class Boss extends  Handler {

    @Override
    public void handleRequest(int price) {
        if (price < 5000) {
            System.out.println("boss同意报销");
        } else {
            System.out.println("boss不允许报销");
        }
    }
}
```



### Client

```java
public class Client {

    public static void main(String[] args) {

        //创建职责链
        Handler manager = new Manager();
        manager.setProcessor(new Boss());

        //请求报销 100 元
        manager.handleRequest(100);

        //请求报销 600 元
        manager.handleRequest(600);

        //请求报销 5100 元
        manager.handleRequest(5100);
    }
}
```



### 执行结果

```
经理同意报销成功
boss同意报销
boss不允许报销
```


