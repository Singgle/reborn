---
title: 外观模式(Facade Pattern)
tags:
  - 设计模式
categories:
  - 设计模式
date: 2021-06-23 21:15:03
---



## 外观模式具有的角色

外观模式具有以下两个角色：

- 外观(Facade)角色：该角色会将客户端发送的所有请求委托给相应的子系统。
- 子系统(Subsystem)角色：一个系统可以同时有一个或多个子系统。每个子系统不是一个单独的类，而是类的集合。



## 代码示例

### 外观角色

```java
public class ShapeMaker {

    private Shape rectangle;
    private Shape square;
    private Shape circle;

    public ShapeMaker() {
        rectangle = new Rectangle();
        square = new Square();
        circle = new Circle();
    }

    public void drawRectangle() {
        rectangle.draw();
    }

    public void drawSquare() {
        square.draw();
    }

    public void drawCircle() {
        circle.draw();
    }
}
```



### 子系统角色

```java
public interface Shape {

    void draw();
}
```

```java
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Rectangle::draw()");
    }
}
```

```java
public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Square::draw()");
    }
}
```

```java
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Circle::draw()");
    }
}
```



### Client

```java
public class Client {

    public static void main(String[] args) {
        ShapeMaker shapeMaker = new ShapeMaker();
        shapeMaker.drawRectangle();
        shapeMaker.drawSquare();
        shapeMaker.drawCircle();
    }
}
```
