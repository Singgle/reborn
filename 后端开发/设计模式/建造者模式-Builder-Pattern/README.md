---
title: 建造者模式(Builder Pattern)
tags:
  - 设计模式
categories:
  - 设计模式
date: 2021-06-23 17:36:47
---



## 类图

![建造者模式类图](README/epub_679952_23.png)

建造者模式包含的角色如下：

- 导演者(Director)角色：安排产品各部分的构建顺序，并通知Builder进行构建。
- 抽象构建者(Builder)角色：规范产品的组成部分，并进行抽象。
- 具体构建者(ConcreteBuilder)角色：实现抽象构建者，实现产品各部分的构建方法，并返回构建好的产品。
- 产品(Product)角色：该角色为系统中需要构建的复杂对象。

**注：**导演者角色是对产品构建的封装，减少耦合。导演者对象可以有多个，也可以没有，需要根据实际需求判断。



## 构建者模式的作用

**定义：**将复杂对象的构建与其表示分离，使得相同的构建过程可以构建出不同的表示。

**使用场景：**在系统中需要构建一个复杂对象，该复杂对象有多个部分。根据需求不同，该复杂对象的部分会发生变化，但是组合这些部分的算法基本不变。



## 代码示例(一个导演者角色)

以构建一个计算机为例，需要组装主板，安装操作系统，安装显示器。



### 产品角色

```java
@ToString
public abstract class Computer {

    protected String board;
    protected String display;
    protected String os;

    public void setBoard(String board) {
        this.board = board;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public abstract void setOs();
}
```

```java
public class MacBook extends Computer {

    @Override
    public void setOs() {
        os = "Mac OS";
    }
}
```



### 抽象构建者角色

```java
public abstract class Builder {

    abstract void buildBoard(String board);
    abstract void buildDisplay(String display);
    abstract void buildOs();

    abstract Computer build();
}
```



### 具体构建者角色

```java
public class MacBookBuilder extends Builder {

    private Computer computer = new MacBook();


    @Override
    void buildBoard(String board) {
        computer.setBoard(board);
    }

    @Override
    void buildDisplay(String display) {
        computer.setDisplay(display);
    }

    @Override
    void buildOs() {
        computer.setOs();
    }

    @Override
    Computer build() {
        return computer;
    }
}
```



### 导演者角色

```java
public class Director {

    Builder builder = null;

    Director(Builder builder) {
        this.builder = builder;
    }

    public void construct(String board, String display) {
        builder.buildBoard(board);
        builder.buildDisplay(display);
        builder.buildOs();
    }
}
```



### Client

```java
public class Client {

    public static void main(String[] args) {
        Builder builder = new MacBookBuilder();
        Director director = new Director(builder);
        director.construct("主板A", "显示屏A");

        Computer computer = builder.build();
        System.out.println(computer.toString());
    }
}
```

执行结果：

```
Computer(board=主板A, display=显示屏A, os=Mac OS)
```





## 代码示例(无导演者)

在有些时候，为了简化系统结构，可以将导演者和抽象构建者Builder角色进行合并，在Builder中提供创建复杂对象的construct方法。

**场景：**

一个快餐店案例。

一个套餐由一个汉堡Burger和冷饮ColdDrink组成。

一个汉堡可以是素食汉堡VegBurger，也可以是鸡肉汉堡ChickenBurger，它们使用纸盒Wrapper包装。

一杯冷饮可以是可口可乐Coke，也可以是百事可乐Pepsi，它们用瓶子Bottle装着。



### 食品条目和食品包装接口定义

```java
public interface Item {
    String name();
    Packing packing();
    float price();
}
```

```java
public interface Packing {
    String pack();
}
```



### 实现Packing 接口的实体类：Wrapper和Bottle

```java
public class Wrapper implements Packing {
    
    @Override
    public String pack() {
    	return "Wrapper";
    }
    
}
```

```java
public class Bottle implements Packing {
    
    @Override
    public String pack() {
        return "Bottle";
    }
}
```



### 实现Item接口的抽象类：Burger和ColdDrink

```java
public abstract class Burger implements Item {
    
    @Override
    public Packing packing() {
        return new Wrapper();
    }
}
```

```java
public abstract class ColdDrink implements Item {
    
    @Override
    public Packing packing() {
        return new Bottle();
    }
}
```



### 创建扩展了Burger和ColdDrink的实体类

```java
public class VegBurger extends Burger {
    
    @Override
    public String name() {
        return "VegBurger";
    }
    
    @Override
    public float price() {
        return 50f;
    }
}
```

```java
public class ChickenBurger extends Burger {
    
    @Override
    public String name() {
        return "Chicken Burger";
    }
    
    @Override
    public float price() {
        return 55f;
    }
}
```

```java
public class Coke extends ColdDrink {
    
    @Override
    public String name() {
        return "Coke";
    }
    
    @Override
    public float price() {
        return 20f;
    }
}
```

```java
public class Pepsi extends ColdDrink {
    
    @Override
    public String name() {
        return "Pepsi";
    }
    
    @Override
    public float price() {
        return 25f;
    }
}
```



### Meal

```java
public class Meal {
    
    private List<Item> items = new ArrayList<Item>();
    
    public void addItem(Item item) {
        items.add(item);
    }
    
    public float getPrice() {
        float cost = 0f;
        for(Item item : items) {
            cost+=item.price();
        }
        return cost;
    }
    
    public void showItems() {
        for(Item item : items) {
            System.out.print("Item : "+item.name());
         	System.out.print(", Packing : "+item.packing().pack());
         	System.out.println(", Price : "+item.price());
        }
    }
}
```



### MealBuilder

```java
public class MealBuilder {
    
    public Meal prepareVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());
        return meal;
    }
    
    public Meal prepareNonVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Pepsi());
        return meal;
    }
}
```



### Client

```java
public class Client {
    
    public static void main(String[] args) {
        MealBuilder mealBuilder = new MealBuilder();
        
        Meal vegMeal = mealBuilder.prepareVegMeal();
        vegMeal.showItems();
        
        System.out.println();
        
        Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
        nonVegMeal.showItems();
    }
}
```

执行结果：

```
Item : VegBurger, Packing : Wrapper, Price : 50.0
Item : Coke, Packing : Bottle, Price : 20.0

Item : VegBurger, Packing : Wrapper, Price : 50.0
Item : Pepsi, Packing : Bottle, Price : 25.0
```

