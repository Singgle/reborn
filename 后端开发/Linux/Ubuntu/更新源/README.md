# 更新源

Ubuntu默认使用的国外源太慢了，所以我们需要修改为国内源



1. 备份 `/etc/apt/sources.list`

   ```bash
   cp /etc/apt/sources.list /etc/apt/sources.list.bak
   ```

2. 添加阿里源

   在`/etc/apt/sources.list`文件前面添加下面内容

   ```bash
   #添加阿里源
   deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
   ```

3. 最后执行下面命令更新源

   ```bash
   sudo apt-get update
   sudo apt-get upgrade
   ```



## 参考文章

[Ubuntu更新源](https://www.cnblogs.com/ssxblog/p/11357126.html)

