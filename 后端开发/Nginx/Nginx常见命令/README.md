# nginx常见命令



## 查看nginx版本号

```bash
nginx -v
```



## 启动nginx

```bash
nginx
```



## 关闭nginx

```bash
nginx -s stop
```



## 重新加载nginx配置文件

```bash
nginx -s reload
```

