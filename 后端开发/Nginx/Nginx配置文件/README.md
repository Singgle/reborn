# Nginx配置文件



## nginx配置文件路径

nginx配置文件在 `/etc/nginx/nginx.conf`



## nginx配置文件组成

nginx配置文件由三部分组成

```conf
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
	# multi_accept on;
}

http {

	##
	# Basic Settings
	##

	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	# server_tokens off;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	##
	# Logging Settings
	##

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;
}
```





### 第一部分：全局块

从配置文件开始到 events 块之间的内容，主要会设置一些影响nginx服务器整体运行的配置命令，主要包括配置运行Nginx服务器的用户(组)、允许生成的worker process数、进程PID存放路径、日志存放路径和类型以及配置文件的引入等。



### 第二部分：events块

events块涉及的指令主要影响Nginx服务器与用户的网络连接，常用的设置包括是否开启对多work process下的网络连接进行序列化、是否允许同时接受多个网络连接、选取哪种事件驱动模型来处理连接请求、每个work process可以同时支持的最大连接数等。



### 第三部分：http块

这是Nginx服务器配置中最频繁的部分，代理、缓存和日志定义等绝大多数功能和第三方模块的配置都在这里。

需要注意的是：http块也可以包括 **http全局块、server块**

- **http全局块**

  配置的指令包括文件引入、MIME-TYPE定义、日志自定义、连接超时时间、单链接请求数上限等。

- **server块**

  这块和虚拟主机有密切关系，虚拟主机从用户角度看，和一台独立的硬件主机是完全一样的，该技术的产生是为了节省互联网服务器硬件成本。

  每个http块可以包括多个server块，而每个server块就相当于一个虚拟主机。

  而每个server块也分为 全局server块，以及可以同时包含多个location块。

  1. 全局server块

     最常见的配置是本虚拟机的监听配置和本虚机主机的名称或IP配置。

  2. location块

     一个server块可以配置多个location块。

     该块的主要作用是基于Nginx服务器接收到的请求字符串(例如 server_name/uri-string)，对虚拟主机名称(也可以是IP别名)之外的字符串(例如 前面的 /uri-string)进行匹配，对特定的请求进行处理。地址定向、数据缓存和应答控制等功能，还有很多第三方模块的配置也在这里进行。

  















