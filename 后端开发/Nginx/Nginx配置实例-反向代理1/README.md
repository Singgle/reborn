# Nginx配置实例-反向代理1

**实现目标：**

浏览器访问`http://localhost:80`，niginx反向代理到`http://localhost:8080`

> 实验前提准备：tomcat安装并启动、nginx安装



nginx配置如下：

在http块中新增server块，server块内容如下：

```conf
server {
    listen      80;
    server_name localhost;

    location / {
    	proxy_pass      http://127.0.0.1:8080;
    }
}
```



重新加载配置文件

``` bash
sudo nginx -s reload
```



验证：

在浏览器输入`http://localhost:80`，会返回tomcat的启动页面，表示配置成功。

