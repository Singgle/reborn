# Nginx配置实例-动静分离

**实现效果：**

请求静态资源(如html、css、js)文件的时候访问Nginx服务器，请求动态资源(如Servlet、JSP)的时候访问tomcat服务器。



> 准备工作：(这里不演示动态资源的访问，只演示静态资源的访问)
>
> 1. 在nginx下准备静态资源
>
>    在服务器根目录创建 /data/www目录，在里面放a.html文件

> a.html文件内容如下：
>
> ```html
> <h1>test html</h1>
> ```



执行步骤：

1. 配置Nginx配置文件

   ```conf
   location /www/ {
   	root	/data/;
   }
   ```

2. 打开浏览器输入[http://localhost/www/a.html](http://localhost/www/a.html)

   出现下面页面，表示成功

   ![](./img/image-20201021235111066.png)

## 扩展

### 目录索引

```conf
location /www/ {
	root	/data/;
	autoindex	on;
}
```

对应页面：

![](./img/image-20201021235602779.png)



### 缓存

location中，通过expires参数设置，可以设置浏览器缓存过期时间，减少与服务器之间的请求和流量。这种方式不适合经常变动的资源。

我们设置3d，表示在这3天之内访问这个URL，发送一个请求，对比服务器该文件最后更新时间没有变化，则不会从服务器抓取，返回状态码304；如果有修改，则直接从服务器直接下载，返回状态码200。