# Nginx安装

> 本教程是在Ubuntu 18.04.4 上安装，下面的命令也是Ubuntu的命令



## 依赖环境安装

### GCC环境

```bash
sudo apt-get install gcc
```



### PCRE

PCRE(Perl Compatible Regular Expressions)是一个Perl库，包括 perl 兼容的正则表达式库。

nginx的http模块使用pcre来解析正则表达式，所以需要在linux上安装pcre库。

> 注：libpcre3-dev是使用pcre开发的一个二次开发库。nginx也需要此库。

```bash
sudo apt-get install libpcre3 libpcre3-dev
```



### zlib

zlib库提供了很多种压缩和解压缩的方式，nginx使用zlib对http包的内容进行gzip，所以需要在linux上安装zlib库。

```bash
sudo apt-get install zlib1g-dev
```



### OpenSSL

OpenSSL 是一个强大的安全套接字层密码库，囊括主要的密码算法、常用的密钥和证书封装管理功能及SSL协议，并提供丰富的应用程序供测试或其它目的使用。

nginx不仅支持http协议，还支持https（即在ssl协议上传输http），所以需要在linux安装openssl库。

```bash
sudo apt-get install openssl libssl-dev
```



## 安装并启动nginx

### 安装nginx

```bash
sudo apt-get install nginx
```



### 启动nginx

```bash
service nginx start
```



### 查看nginx进程是否启动

```bash
xgc@xgc-ubuntu:~$ ps -ef | grep nginx
root       888     1  0 23:38 ?        00:00:00 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
www-data   889   888  0 23:38 ?        00:00:00 nginx: worker process
www-data   890   888  0 23:38 ?        00:00:00 nginx: worker process
xgc       1877  1861  0 23:40 pts/0    00:00:00 grep --color=auto nginx
```

我们看到上面有`worker process`，说明nginx进程启动了



### 访问nginx

在浏览器输入当前主机IP地址，出现下面页面，表示nginx启动成功

![image-20201013234616578](./imgs/20201013234701.png)



## 参考文章

[nginx安装及其配置详细教程](https://www.cnblogs.com/lywJ/p/10710361.html)