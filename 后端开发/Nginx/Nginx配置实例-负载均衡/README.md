# Nginx配置实例-负载均衡

**实现效果：**

在浏览器输入`http://localhost/edu/a.html`，实现负载均衡效果，将请求平均分配到8080端口和8081端口的两个tomcat上。



> 准备工作：
>
> 1.准备两台tomcat服务器，端口分别是8080和8081
>
> 2.在两台服务器上webapps目录下，创建名称是edu文件夹，在edu文件内创建a.html文件，用于测试

> a.html文件内容如下：(端口根据当前tomcat的端口更换)
>
> ```html
> <h1>
>     8080 !!!
> </h1>
> ```



实现步骤：

1. 修改nginx配置文件

   在http块中增加下面内容

   ```conf
   upstream myserver {
   	server	localhost:8080;
   	server	localhost:8081;
   }
   ```

   ```conf
   server {
   	listen	80;
   	server_name	localhost:
   	
   	location / {
   		proxy_pass	http://myserver;
   	}
   }
   ```

2. 打开浏览器，输入 [http://localhost/edu/a.html](http://localhost/edu/a.html)

   当我们刷新页面的时候，会轮流出现下面两个页面表示配置成功

   ![](./img/20201021211701.png)

   

   ![](./img/image-20201021211959755.png)



## Nginx的负载均衡策略

Nginx提供了多种负载均衡策略：

1. 轮询(默认)

   每个请求按时间顺序逐一分配到不同的后端服务器。如果后端服务器down掉，能自动剔除。

2. weight

   权重默认为1，权重越高的服务器，收到的请求几率越高。

   weight和访问比例呈正比，通常用于后端服务器性能不均的情况。

   配置如下：

   ```conf
   upstream server_pool {
   	server	localhost:8080	weight=10;
   	server	localhost:8081	weight=5;
   }
   ```

3. ip_hash

   每个请求按访问的ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。

   配置如下：

   ```conf
   upstream server_pool {
   	ip_hash;
   	server	localhost:8080;
   	server	localhost:8081;
   }
   ```

4. fair(第三方)

   按后端服务器的响应时间来分配请求，响应时间短的优先分配。

   配置如下：

   ```conf
   upstream server_pool {
   	fair;
   	server	localhost:8080;
   	server	localhost:8081;
   }
   ```

   