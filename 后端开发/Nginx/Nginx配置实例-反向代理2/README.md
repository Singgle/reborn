# Nginx配置实例-反向代理2

**实现目标**

使用nginx反向代理，根据访问的路径跳转到不同端口的服务中。

nginx监听端口为9001

访问`http://localhost:9001/edu`直接跳转到`http://localhost:8080`

访问`http://localhost:9001/vod`直接跳转到`http://localhost:8081`



> 1. 准备工作：安装两个tomcat，端口分别是8080和8081
>
> 2. 创建文件夹和测试页面：端口为8080的tomcat创建edu文件夹，里面放a.html。端口为8081的tomcat创建vod文件夹，里面放a.html。

> a.html文件内容如下：(里面的端口写当前tomcat的端口号)
>
> ```html
> <h1>
>     8080!!!
> </h1>
> ```



实现步骤：

1. 配置nginx配置文件： 

   找到http块，在里面增加一个server块：

   ```conf
   server {
   	listen	9001;
   	server_name	localhost;
   	
   	location ~ /edu/ {
   		proxy_pass	http://127.0.0.1:8080;
   	}
   	
   	location ~ /vod/ {
   		proxy_pass	http://127.0.0.1:8081;
   	}
   }
   ```

2. 重新加载niginx配置文件

   执行下面命令

   ```bash
   sudo nginx -s reload
   ```

3. 访问浏览器，验证配置是否成功

   访问 `http://localhost:9001/edu/a.html`

   出现

   ![](./img/20201020221601.png)

   

   访问 `http://localhost:9001/vod/a.html`

   出现

   ![](./img/20201020221801.png)

   

   说明配置成功



## 知识点

**location指令说明**

该指令用于匹配URL。

语法如下：

```
location [ = | ~ | ~* | ^~ ] uri {
	
}
```

1. =：用于不含正则表达式的uri前，要求请求字符与uri严格匹配，如果匹配成功，就停止继续向下搜索并立即处理该请求
2. ~：用于表示uri包含正则表达式，并且区分大小写
3. ~*：用于表示uri包含正则表达式，并且不区分大小写
4. ^~：用于不含正则表达式的uri前，要求Nginx服务器找到表示uri和请求字符串匹配度最高的location后，立即使用此location处理请求，而不再使用location块中的正则uri和请求字符串做匹配。

