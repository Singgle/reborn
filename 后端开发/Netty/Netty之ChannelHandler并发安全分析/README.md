---
title: Netty之ChannelHandler并发安全分析
tags:
  - ChannelHandler
  - Netty
categories:
  - Netty
date: 2021-07-15 21:51:56
---

## 引言

ServiceHandler中有一个count共享变量，那么在多线程环境下，下面代码是否是线程安全的？

```java
public class ServiceHandler extends ChannelInboundHandlerAdapter {

	private int count = 0;

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		while (count < 10) {
			count++;
			System.out.println("读取" + count + "次");
		}
	}

}
```



## 串行执行后的ChannelHandler

**如果ChannelHandler是非共享的，则它就是线程安全的。**

这是因为：每个Channel都有各自的ChannelHandler实例。从消息接收到消息响应都是同一个NioEventLoop线程进行处理。这就不会存在多线程同时访问共享变量的问题，也就线程安全了。

```java
ServerBootstrap serverBootstrap = new ServerBootstrap()
    .group(bossGroup, workerGroup)
    .channel(NioServerSocketChannel.class)
    .option(ChannelOption.SO_BACKLOG, 128)
    .childOption(ChannelOption.SO_KEEPALIVE, true)
    .childHandler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            ch.pipeline().addLast(new ServiceHandler());
        }
    });
```

串行执行的ChannelHandler的工作原理：

![串行执行的ChannelHandler的工作原理](README/20200113175229452.png)



## 跨链路共享的ChannelHandler

现在我们只初始化一次ChannelHandler，然后加入到多个Channel的ChannelPipeline中。

由于不同的Channel可能注册到不同的NioEventLoop线程，这样ChannelHandler就可能被多个线程访问，就存在并发访问风险了。

```java
EventLoopGroup eventLoopGroup = new NioEventLoopGroup();

// 只实例化一个ChannelHandler
ChannelHandler channelHandler = new ServiceHandler();
Bootstrap bootstrap = new Bootstrap()
    .group(eventLoopGroup)
    .channel(NioSocketChannel.class)
    .handler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            ch.pipeline().addLast(channelHandler);
        }
    });

List<ChannelFuture> channelFutures = new ArrayList<>();
// 模拟4个客户端启动访问,每个客户端链路都共用一个ChannelHandler
for(int i = 0; i < 4; i++){
    ChannelFuture channelFuture = bootstrap.connect(HOST, PORT).sync();
    channelFutures.add(channelFuture);
}
channelFutures.forEach(channelFuture -> {
    try {
        channelFuture.channel().closeFuture().sync();
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
});
eventLoopGroup.shutdownGracefully();
```

执行后报错如下：

```
18:52:45.384 [nioEventLoopGroup-2-2] WARN  io.netty.channel.ChannelInitializer - Failed to initialize a channel. Closing: [id: 0xb6df6234]
io.netty.channel.ChannelPipelineException: com.manling.netty.handler.security.ServiceHandler is not a @Sharable handler, so can't be added or removed multiple times.
	at io.netty.channel.DefaultChannelPipeline.checkMultiplicity(DefaultChannelPipeline.java:600)
	at io.netty.channel.DefaultChannelPipeline.addLast(DefaultChannelPipeline.java:202)
	at io.netty.channel.DefaultChannelPipeline.addLast(DefaultChannelPipeline.java:381)
	at io.netty.channel.DefaultChannelPipeline.addLast(DefaultChannelPipeline.java:370)
	at com.manling.netty.test.NettyClient$1.initChannel(NettyClient.java:33)
```

报错是因为，我们尝试将同一个ChannelHandler加入到多个ChannelPipeline。

---

如果我们要ChannelHandler能够共享，通过`@ChannelHandler.Sharable`注解就可以实现。

```java
@ChannelHandler.Sharable
public class ServiceHandler extends ChannelInboundHandlerAdapter {
    ...
}
```



## 参考文章

[Netty-ChannelHandler并发安全分析](https://blog.csdn.net/MarchRS/article/details/103961106)


