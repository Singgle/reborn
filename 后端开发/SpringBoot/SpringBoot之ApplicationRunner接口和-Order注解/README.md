---
title: SpringBoot之ApplicationRunner接口和@Order注解
tags:
  - SpringBoot
categories:
  - SpringBoot
date: 2021-07-15 21:50:36
---

在开发中，可能会有这样的需求。需要在容器启动时执行一些操作，比如，读取配置文件，创建数据库连接等。

SpringBoot提供了ApplicationRunner接口来帮助我们实现这种需求。该接口执行时机为容器启动完成的时候。



## ApplicationRunneri接口

示例代码如下：

```java
@Component
public class PreOperation implements ApplicationRunner {

	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("容器启动后执行读取配置文件");
	}

}
```



## @Order注解

如果有多个实现类，而你需要他们按一定顺序执行的话，可以在实现类上加上@Order注解。@Order(value=整数值)。SpringBoot会按照@Order中的value值从小到大依次执行。

```java
@Component
@Order(2)
public class PreOperation implements ApplicationRunner {

	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("容器启动后执行读取配置文件");
	}

}
```


