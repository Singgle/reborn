# Redis持久化之RDB(Redis Database)

## RDB是什么

在指定的时间间隔内将内存中的数据集快照写入磁盘，也就是行话讲的Snapshot快照，它恢复时是将快照文件直接读到内存里。

Redis会单独创建(fork)一个进程来进行持久化，会先将数据写入到一个临时文件中，待持久化过程都结束了，再用这个临时文件替换上次持久化好的文件。

整个过程中，主进程是不进行任何IO操作的，这就确保了极高的性能。

如果需要进行大规模数据的恢复，且对于数据恢复的完整性不是非常敏感，那RDB方式要比AOF方式更加的高效。RDB的缺点是最后一次持久化后的数据可能丢失。



RDB保存的是dump.rdb文件



## RDB配置

### save

在配置文件中，我们可以配置 Redis保存rdb文件 的策略。

```conf
# save <seconds> <changes>
save 900 1		# 15分钟一次
save 300 10		# 5分钟10次
save 60 10000	# 1分钟1万次
```

以`save 900 1`为例，表示在900秒以内有一个key的值变更了，Redis就保存当前内存中数据集快照。

如果我们想禁用RDB持久化的策略，只要不设置任何save指令，或者给save传入一个空字符串参数也可以。



同时，当我们关闭Redis的时候，Redis也会将当前内存中的数据集快照保存下来。



### stop-writes-on-bgsave-error

```conf
stop-writes-on-bgsave-error yes
```

当bgsave快照操作出错时停止写数据到磁盘，设置为yes表示启用。



### rdbcompression

```conf
rdbcompression yes
```

对于存储在磁盘中的快照，可以设置是否进行压缩存储。如果是的话，redis会采用LZF算法进行压缩。如果你不想消耗CPU来进行压缩的话，可以设置为关闭此功能。



### rdbchecksum

```conf
rdbchecksum yes
```

在存储快照后，还可以让redis使用CRC64算法来进行数据校验，但是这样做会增加大约10%的性能消耗。如果希望获取到最大的性能提升，可以关闭此功能。



## 如何触发RDB快照

### 命令save或者 bgsave

save：save时只管保存，其他不管，全部阻塞。

bgsave：Redis会在后台异步进行快照操作，快照同时还可以响应客户端请求。可以通过lastsave命令获取最后一次成功执行快照的时间。



### 执行FLUSHALL命令

当我们执行FLUSHALL命令的时候，Redis会生成一个空的rdb文件覆盖原来的rdb文件。



## 劣势

1. 最后一次快照后的数据可能会丢失
2. Fork的时候，内存中的数据被克隆了一份，大致2倍的膨胀需要考虑。