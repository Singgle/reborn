# 常用五大数据类型



## String

string是redis最基本的类型。

string类型是二进制安全的，意思是redis的string可以包含任何数据，比如jpg图片或者序列化的对象。

redis中一个字符串value最多可以是512M



## Hash

哈希，类似Java里的Map.

Redis hash是一个键值对集合。

Redis hash是一个string类型的field和value的映射表，hash特别适合用于存储对象。



## List

Redis list是简单的字符串列表，按照插入顺序排序。可以添加一个元素到列表的头部或尾部。

它的底层实现是链表。



## Set

Redis的Set是string类型的无序集合。它是通过HashTable实现的。



## Zset(sorted set)

Redis Zset 和 set 一样也是string 类型元素的集合，且不允许重复的元素。

不同的是，Zset中的每个元素都会关联一个double类型的分数

redis正是通过分数来为集合中的成员进行从小到大的排序。Zset的成员是唯一的，但是分数(score)却可以重复。

