# Redis持久化之AOF

## AOF是什么

**以日志的形式来记录每个操作**，将Redis执行过的所有写指令记录下来(读操作不记录)，只允许追加文件但不允许改写文件，Redis启动之初会读取该文件重新构建数据，换言之，redis重启的时候就根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作。



## 检查AOF文件

在Redis运行的时候，可能会出现错误导致aof文件写入的时候发生错误，从而导致redis启动错误。我们可以通过redis的bin目录下面的redis-check-aof来修复aof文件。

```bash
redis-check-aof --fix appendonly.aof
```

这个命令会将aof文件中所有不符合语法规范的删除。



## AOF与RDB共存

AOF和RDB可以同时存在，但是当Redis重新启动的时候，Redis会先从AOF文件中读取记录。



## AOF常见配置

```conf
# 默认是no，表示不开始AOF。如果想开启，可以设置为yes
appendonly no

# aof文件名设置
appendfilename "appendonly.aof"

# 设置aof保存策略，有三个值：always、everysec、no
# always:同步持久化 每次发生数据变更会被立即记录到磁盘，性能较差但数据完整性比较好
# everysec:出厂默认推荐，异步操作，每秒记录。如果一秒内宕机，有数据丢失
# no : 永不保存
# appendfsync always
appendfsync everysec
# appendfsync no
```



## Rewrite

什么是重写？

AOF采用文件追加的形式，文件会越来越大，为避免出现此种情况，新增了重写机制，当AOF文件的大小超过所设定的阈值，Redis就会启动AOF的内容压缩，只保留可以恢复数据的最小指令集，可以使用命令bgrewriteaof。



重写的原理：

AOF文件持续增长过过大时，Redis会fork出一条新进程来将文件重写(也是先写临时文件最后再rename)，遍历新进程的内存中数据，每条记录有一条的set语句。重写aof文件的操作，并没有读取旧的aof文件，而是将整个内存中的数据库内容用命令的方式重写了一个新的aof文件，这点和快照有点类似。



触发机制：

Redis会记录上次重写的AOF大小，默认配置是当AOF文件大小是上次rewrite后大小的一倍且文件大于64M时触发。



相关配置

```conf
# 设置重写时是否可以用appendfsync，默认no,保证数据安全性
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-rewrite-min-size 64mb
```