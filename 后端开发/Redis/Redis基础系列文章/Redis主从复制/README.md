# Redis主从复制

## 什么是主从复制

主从复制，就是主机数据更新后根据配置和策略，自动同步到备机的master/slave机制，Master以写为主，slave以读为主。



## 主从复制实例

### 中心化的主从复制

1. 先复制出多个redis.conf文件，修改pidfile、 logfile、dbfilename、port，用于启动不同的Redis服务器

   端口分别是6379，6380，6381。

   

2. 启动客户端连接到不同的Redis服务器上

   ```bash
   redis-cli -p 6379
   redis-cli -p 6380
   redis-cli -p 6381
   ```

   

3. 查看三台服务器当前的主从情况(当前三台都是master)

   ```redis
   127.0.0.1:6379> info replication
   # Replication
   role:master
   connected_slaves:0
   master_replid:67f4e10a891d0606bc03e7629cee7e7bbc7699d7
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:0
   second_repl_offset:-1
   repl_backlog_active:0
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:0
   repl_backlog_histlen:0
   ```

   ```redis
   127.0.0.1:6380> info replication
   # Replication
   role:master
   connected_slaves:0
   master_replid:5439ea174362916350cdf87747cbc39240d25377
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:0
   second_repl_offset:-1
   repl_backlog_active:0
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:0
   repl_backlog_histlen:0
   ```

   ```redis
   127.0.0.1:6381> info replication
   # Replication
   role:master
   connected_slaves:0
   master_replid:42245842628686efb796c742403f7ec3d344c9bc
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:0
   second_repl_offset:-1
   repl_backlog_active:0
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:0
   repl_backlog_histlen:0
   ```

   

4. 服务器6379设置了值，然后此时6380，6381没有数据。使用命令`SLAVEOF 127.0.0.1 6379`然后6380，6381查看数据，发现服务器6379设置的值备份到6380，6381中了。

   ```redis
   127.0.0.1:6379> set k1 v1
   OK
   ```

   ```redis
   127.0.0.1:6380> get k1
   (nil)
   127.0.0.1:6380> SLAVEOF 127.0.0.1 6379
   OK
   127.0.0.1:6380> get k1
   "v1"
   ```

   ```redis
   127.0.0.1:6381> get k1
   (nil)
   127.0.0.1:6381> SLAVEOF 127.0.0.1 6379
   OK
   127.0.0.1:6381> get k1
   "v1"
   ```

   

5. 此时再次调用命令`info replication`。此时6379是master，6380，6381是slave。

   ```redis
   127.0.0.1:6379> INFO replication
   # Replication
   role:master
   connected_slaves:2
   slave0:ip=127.0.0.1,port=6380,state=online,offset=196,lag=0
   slave1:ip=127.0.0.1,port=6381,state=online,offset=196,lag=0
   master_replid:f4b7c535757d9fa921bf4d79aee64b902d84c9ff
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:196
   second_repl_offset:-1
   repl_backlog_active:1
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:1
   repl_backlog_histlen:196
   ```

   ```redis
   127.0.0.1:6380> info replication
   # Replication
   role:slave
   master_host:127.0.0.1
   master_port:6379
   master_link_status:up
   master_last_io_seconds_ago:4
   master_sync_in_progress:0
   slave_repl_offset:672
   slave_priority:100
   slave_read_only:1
   connected_slaves:0
   master_replid:f4b7c535757d9fa921bf4d79aee64b902d84c9ff
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:672
   second_repl_offset:-1
   repl_backlog_active:1
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:1
   repl_backlog_histlen:672
   ```

   ```redis
   127.0.0.1:6381> info replication
   # Replication
   role:slave
   master_host:127.0.0.1
   master_port:6379
   master_link_status:up
   master_last_io_seconds_ago:4
   master_sync_in_progress:0
   slave_repl_offset:700
   slave_priority:100
   slave_read_only:1
   connected_slaves:0
   master_replid:f4b7c535757d9fa921bf4d79aee64b902d84c9ff
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:700
   second_repl_offset:-1
   repl_backlog_active:1
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:127
   repl_backlog_histlen:574
   ```

   

6. 在6380上设置值，会出现错误。这表示只有master才能写，而slave只能读。

   ```redis
   127.0.0.1:6381> set k2 v2
   (error) READONLY You can't write against a read only replica.
   ```

   

7. 此时master6379关闭，使用命令`info replication`，发现此时6380、6381还是slave

8. 重新启动6379，6380和6381还是可以实现主从复制。

9. 关闭slave6380，再重新启动。此时6380的状态是master，不能备份6379的数据。(这是因为slave6380是在命令行中配置的，重新启动后之前配置失效。如果配置在conf中就可以继续备份)





### 链式的主从复制

中心化的主从复制能够实现数据的备份，但是它存在着一些缺点：

1. 所有slave都连接master主机，从master中获取要备份的数据。当连接数多时，master的压力会过大。

2. 当slave挂掉后，重新连接master的时候，会先将原先所有的数据清掉，然后再将master的数据全部备份。



实例：(此时3台服务器都是互相独立的)

1. slave6380同步master6379

   ```redis
   127.0.0.1:6380> SLAVEOF 127.0.0.1 6379
   OK
   ```

2. slave6381同步slave6380

   ```redis
   127.0.0.1:6381> SLAVEOF 127.0.0.1 6380
   OK
   ```

3. 这样的话，当master增加了值，两个salve都能同步到数据

   ```redis
   127.0.0.1:6379> set k1 v1
   OK
   ```

   ```redis
   127.0.0.1:6380> get k1
   "v1"
   ```

   ```redis
   127.0.0.1:6381> get k1
   "v1"
   ```

   

### SLAVE成为MASTER

现在，6380和6381都是6379的从机。然后master6379挂掉了

```redis
127.0.0.1:6379> SHUTDOWN
not connected>
```

此时6380执行命令

```redis
127.0.0.1:6380> SLAVEOF no one
OK
127.0.0.1:6380> INFO replication
# Replication
role:master
connected_slaves:0
master_replid:2fd7d2195d13be71f38c921e2c4fb75ae41b2671
master_replid2:5b03ac216717543549f42f7d9715396fd4801e2a
master_repl_offset:1145
second_repl_offset:1146
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:1145
```

可以看到此时6380成为了master。但是6381还是等待6379。

此时6381，执行

```redis
127.0.0.1:6381> SLAVEOF 127.0.01 6380
OK
```

这样，6381就可以备份6380的数据了。



## 复制原理

1. Slave成功连接到Master后会发送一个sync命令

2. Master接到命令启动后台的存盘进程，同时收集所有接受到的用于修改数据集命令，在后台进程执行完毕之后，master将传送整个数据文件到Slave，以完成一次完全同步

   全量复制：而Slave服务在接收到数据库文件数据后，将其存盘并加载到内存中。

   增量赋值：Master继续将新的收集到的修改命令依次传给Slave，以完成同步

   但是只要是重新连接master，一次完全同步(全量复制)将被自动执行



## 哨兵模式(sentinel)

能够自动后台监控Master是否故障，如果故障了根据投票数自动将从库转换成主库。



### 实例

1. 准备三台redis服务器，其中6380和6381是6379的从机。

2. 新建sentinel.conf文件

   ```conf
   sentinel monitor 被监控数据库名字(自己起名字) 127.0.0.1 6379 1
   ```

   ```conf
   sentinel monitor redis6379 127.0.0.1 6379 1
   ```

   上面最后一个数字1，表示主机挂掉后slave投票看让谁接替成为主机，得票数多少后成为主机

3. 启动哨兵

   ```bash
   [root@xgc station]# ~/software/redis-6.0.9/src/redis-sentinel /etc/redis/sentinel.conf 
   2818:X 14 Nov 2020 22:59:05.710 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
   2818:X 14 Nov 2020 22:59:05.710 # Redis version=6.0.9, bits=64, commit=00000000, modified=0, pid=2818, just started
   2818:X 14 Nov 2020 22:59:05.710 # Configuration loaded
                   _._                                                  
              _.-``__ ''-._                                             
         _.-``    `.  `_.  ''-._           Redis 6.0.9 (00000000/0) 64 bit
     .-`` .-```.  ```\/    _.,_ ''-._                                   
    (    '      ,       .-`  | `,    )     Running in sentinel mode
    |`-._`-...-` __...-.``-._|'` _.-'|     Port: 26379
    |    `-._   `._    /     _.-'    |     PID: 2818
     `-._    `-._  `-./  _.-'    _.-'                                   
    |`-._`-._    `-.__.-'    _.-'_.-'|                                  
    |    `-._`-._        _.-'_.-'    |           http://redis.io        
     `-._    `-._`-.__.-'_.-'    _.-'                                   
    |`-._`-._    `-.__.-'    _.-'_.-'|                                  
    |    `-._`-._        _.-'_.-'    |                                  
     `-._    `-._`-.__.-'_.-'    _.-'                                   
         `-._    `-.__.-'    _.-'                                       
             `-._        _.-'                                           
                 `-.__.-'                                               
   
   2818:X 14 Nov 2020 22:59:05.711 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
   2818:X 14 Nov 2020 22:59:05.721 # Sentinel ID is 0b3a0c103e95344efd82b304aa932eae79547900
   2818:X 14 Nov 2020 22:59:05.721 # +monitor master redis6379 127.0.0.1 6379 quorum 1
   2818:X 14 Nov 2020 22:59:05.722 * +slave slave 127.0.0.1:6380 127.0.0.1 6380 @ redis6379 127.0.0.1 6379
   2818:X 14 Nov 2020 22:59:05.724 * +slave slave 127.0.0.1:6381 127.0.0.1 6381 @ redis6379 127.0.0.1 6379
   ```

4. 关闭Master6379，然后使用info replication。我们会发现从机6380成为主机了。

   ```redis
   127.0.0.1:6379> SHUTDOWN
   not connected> 
   ```

   ```redis
   127.0.0.1:6380> info replication
   # Replication
   role:master
   connected_slaves:1
   slave0:ip=127.0.0.1,port=6381,state=online,offset=5528,lag=0
   master_replid:59f3050407dc0f5a7eb0daa9844439909d1b019e
   master_replid2:d6cf75c371a2b38847b530dbdda136cf073fad0a
   master_repl_offset:5528
   second_repl_offset:4688
   repl_backlog_active:1
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:1
   repl_backlog_histlen:5528
   ```

5. 当6379重新启动之后，6379就会变成从机

   ```
   [root@xgc station]# ~/software/redis-6.0.9/src/redis-server /etc/redis/redis6379.conf
   [root@xgc station]# ~/software/redis-6.0.9/src/redis-cli -p 6379
   127.0.0.1:6379> info replication
   # Replication
   role:slave
   master_host:127.0.0.1
   master_port:6380
   master_link_status:up
   master_last_io_seconds_ago:1
   master_sync_in_progress:0
   slave_repl_offset:11425
   slave_priority:100
   slave_read_only:1
   connected_slaves:0
   master_replid:59f3050407dc0f5a7eb0daa9844439909d1b019e
   master_replid2:0000000000000000000000000000000000000000
   master_repl_offset:11425
   second_repl_offset:-1
   repl_backlog_active:1
   repl_backlog_size:1048576
   repl_backlog_first_byte_offset:11001
   repl_backlog_histlen:425
   ```

   

## 复制的缺点

由于所有的写操作都是现在master上操作，然后同步更新到Slave上，所以从master同步到slave机器有一定的延迟，当系统很繁忙的时候，延迟问题会更加严重，slave机器数量的增加也会使这个问题更加严重。