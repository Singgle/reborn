# Redis列表

## lpush/rpush/lrange

lpush将值插入到列表的左边

rpush将值插入到列表的右边

lrange获取列表指定范围的数据

```redis
127.0.0.1:6379> LPUSH list01 1 2 3 4 5
(integer) 5
127.0.0.1:6379> RPUSH list02 1 2 3 4 5
(integer) 5
127.0.0.1:6379> LRANGE list01 0 -1
1) "5"
2) "4"
3) "3"
4) "2"
5) "1"
127.0.0.1:6379> LRANGE list02 0 -1
1) "1"
2) "2"
3) "3"
4) "4"
5) "5"
```



## lpop/rpop

lpop 将列表左边的第一个值弹出

rpop 将列表右边的第一个值弹出

```redis
127.0.0.1:6379> LRANGE list01 0 -1
1) "5"
2) "4"
3) "3"
4) "2"
5) "1"
127.0.0.1:6379> LPOP list01
"5"
127.0.0.1:6379> RPOP list01
"1"
127.0.0.1:6379> LRANGE list01 0 -1
1) "4"
2) "3"
3) "2"
```



## lindex

查看列表指定索引的元素

```redis
127.0.0.1:6379> LINDEX list02 1
"2"
```



## llen

查看列表长度

```redis
127.0.0.1:6379> LLEN list02
(integer) 5
```



## lrem key num value

删除多个值

```redis
127.0.0.1:6379> LPUSH list03 1 1 2 2 3 3 3 4 4
(integer) 9
127.0.0.1:6379> LREM list03 2 1
(integer) 2
127.0.0.1:6379> LRANGE list03 0 -1
1) "4"
2) "4"
3) "3"
4) "3"
5) "3"
6) "2"
7) "2"
127.0.0.1:6379> LREM list03 3 4
(integer) 2
```



## ltrim key start end

截取指定范围的值，然后在赋值给key

```redis
127.0.0.1:6379> LPUSH list04 1 2 3 4 5
(integer) 5
127.0.0.1:6379> LTRIM list04 1 2
OK
127.0.0.1:6379> LRANGE list04 0 -1
1) "4"
2) "3"
```



## rpoplpush src dest

将源列表右边的第一个元素弹出，压入到目标列表的左边。

```redis
127.0.0.1:6379> LPUSH list01 1 2 3 4 5
(integer) 5
127.0.0.1:6379> LPUSH list02 a b c
(integer) 3
127.0.0.1:6379> RPOPLPUSH list01 list02
"1"
127.0.0.1:6379> LRANGE list01 0 -1
1) "5"
2) "4"
3) "3"
4) "2"
127.0.0.1:6379> LRANGE list02 0 -1
1) "1"
2) "c"
3) "b"
4) "a"
```



## lset key index value

设置值设置到列表指定的位置

```redis
127.0.0.1:6379> LSET list01 0 a
OK
127.0.0.1:6379> LRANGE list01 0 -1
1) "a"
2) "4"
3) "3"
4) "2"
```



## linsert key before/after value1 value2

将value2插入到列表中的value1的前面或后面

```redis
127.0.0.1:6379> LRANGE list01 0 -1
1) "a"
2) "4"
3) "3"
4) "2"
127.0.0.1:6379> LINSERT list01 BEFORE 4 b
(integer) 5
127.0.0.1:6379> LRANGE list01 0 -1
1) "a"
2) "b"
3) "4"
4) "3"
5) "2"
```



## 性能总结

它是一个字符串链表，两边都可插入。

如果键不存在，创建新的链表。

如果键已存在，新增内容。

如果值全部移除，对应的键也就消失了。

链表的操作无论是头和尾效率都很高，但如果是对中间元素进行操作，效率就很惨淡了。