# Redis事务

## Redis事务命令

| 命令    | 描述                                                         |
| ------- | ------------------------------------------------------------ |
| DISCARD | 取消事务，放弃执行事务块内的所有命令                         |
| EXEC    | 执行事务块内的所有命令                                       |
| MULTI   | 标记一个事务块的开始                                         |
| UNWATCH | 取消WATCH 命令对所有key的监视                                |
| WATCH   | 监视一个(或多个)key，如果在事务执行之前这个(或这些)key被其他命令所改动，那么事务将被打断 |



## 事务实例

### 正常执行

```redis
> MULTI
OK
> set k1 v1
QUEUED
> set k2 v2
QUEUED
> set k3 v3
QUEUED
> get k3
QUEUED
> set k4 v4
QUEUED
> EXEC
1) OK
2) OK
3) OK
4) "v3"
5) OK
```



### 放弃事务

```redis
> keys *
1) "k1"
2) "k4"
3) "k3"
4) "k2"
> MULTI
OK
> set k1 11
QUEUED
> get k1
QUEUED
> DISCARD
OK
> get k1
"v1"
```



### 事务失败

事务失败可以分为两种，它们之间事务的处理也是不一致的。

**第一种**：在输入的时候就报错了，结果为一条命令都没有执行

```redis
127.0.0.1:6379> keys *
(empty array)
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> getset k1
(error) ERR wrong number of arguments for 'getset' command
127.0.0.1:6379> set k3 v3
QUEUED
127.0.0.1:6379> EXEC
(error) EXECABORT Transaction discarded because of previous errors.
127.0.0.1:6379> get k1
(nil)
127.0.0.1:6379> get k3
(nil)

```



**第二种**：输入的时候没有报错，执行后出现错误的命令没有执行，但是其他命令正常执行了。

```redis
127.0.0.1:6379> set k1 v1
OK
127.0.0.1:6379> set k2 v2
OK
127.0.0.1:6379> set k3 v3
OK
127.0.0.1:6379> set k4 v4
OK
127.0.0.1:6379> keys *
1) "k1"
2) "k3"
3) "k2"
4) "k4"
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> set k1 11
QUEUED
127.0.0.1:6379> INCR k2
QUEUED
127.0.0.1:6379> set kk3 33
QUEUED
127.0.0.1:6379> set k4 44
QUEUED
127.0.0.1:6379> EXEC
1) OK
2) (error) ERR value is not an integer or out of range
3) OK
4) OK
127.0.0.1:6379> get k1
"11"
127.0.0.1:6379> get k2
"v2"
```



**总结**：

Redis的事务是部分支持。我们可以理解为第一种是编译时失败，第二种是运行时失败。



## watch监控

开启两个Redis客户端，表示两个客户。他们会对Redis中一个数据同时进行改动，为保证数据一致性和安全性，我们要使用watch对key进行监控。

首先，设置两个值balance表示信用卡额度，debt表示要还款的金额。

```redis
127.0.0.1:6379> set balance 100
OK
127.0.0.1:6379> set debt 0
OK
```



client1：客户1要花费20元，将要执行的事务如下：

```redis
127.0.0.1:6379> WATCH balance
OK
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> DECRBY balance 20
QUEUED
127.0.0.1:6379> INCRBY debt 20
QUEUED
```



client2：在client1执行事务之前，client2花费了20元。

```redis
127.0.0.1:6379> get balance
"100"
127.0.0.1:6379> set balance 80
OK
```



client1：在client2改变了balance值后，执行事务。就会发现事务执行失败。

```redis
127.0.0.1:6379> EXEC
(nil)
127.0.0.1:6379> get balance
"80"
```



此时client1的操作应该是：放弃监控。然后重新监控，执行事务，直到事务成功执行为止。

```redis
127.0.0.1:6379> UNWATCH
OK
127.0.0.1:6379> WATCH balance
OK
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> DECRBY balance 20
QUEUED
127.0.0.1:6379> INCRBY debt 20
QUEUED
127.0.0.1:6379> EXEC
1) (integer) 20
2) (integer) 60
```

