# Jedis

Maven增加依赖

```xml
<!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>3.3.0</version>
</dependency>
```



如果连接的是远程服务器上的redis，那么需要额外配置：(这里我连接的aliyun服务器)

1. 服务器安全组必须开放6379端口

   ![](./img/image-20201115132331295.png)

2. 服务器防火墙开放6379端口

   这里我服务器关闭了防火墙，所以就不用设置了

3. 在配置文件中关闭redis保护模式

   在保护模式下，redis只监听127.0.0.1的6379端口

   ```xml
   protected-mode no
   ```

4. 在配置文件中，注释掉`bind 127.0.0.1`





## 测试连通性

```java
public class TestPing {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("123.57.159.15", 6379);
        System.out.println(jedis.ping());
    }
}
```

```
PONG
```



## 常见API

```java
/**
 * 测试常见API
 */
public class TestAPI {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("123.57.159.15", 6379);

        jedis.set("k1", "v1");
        jedis.set("k2", "v2");
        jedis.set("k3", "v3");

        System.out.println(jedis.get("k1"));

        Set<String> keys = jedis.keys("*");
        System.out.println(keys.size());
    }
}
```

```
v1
3
```



## 事务

```java
public class TestTransaction {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("123.57.159.15", 6379);

        Transaction transaction = jedis.multi();
        transaction.set("k4", "v4");
        transaction.set("k5", "v5");

        //提交事务
        //transaction.exec();
        //放弃事务
        transaction.discard();

    }
}
```



**使用watch**

1. 准备数据

   ```redis
   127.0.0.1:6379> set balance 100
   OK
   127.0.0.1:6379> set debt 0
   OK
   ```

2. 先运行程序

   ```java
   package com.xgc.jedis;
   
   import redis.clients.jedis.Jedis;
   import redis.clients.jedis.Transaction;
   
   import java.sql.SQLOutput;
   
   public class TestWatch {
   
       public boolean transMethod() throws InterruptedException {
           Jedis jedis = new Jedis("123.57.159.15", 6379);
           int balance; //可用余额
           int debt; //欠额
           int amountToSubtract = 10; //实刷额度
   
           jedis.watch("balance");
           // jedis.set("balance", "5"); //此句不该出现，为方便。模拟其他程序已经修改了balance
           Thread.sleep(7000);
           balance = Integer.parseInt(jedis.get("balance"));
           if (balance < amountToSubtract) {
               jedis.unwatch();
               System.out.println("modify");
               return false;
           } else {
               System.out.println("**************transaction");
               Transaction transaction = jedis.multi();
               transaction.decrBy("balance", amountToSubtract);
               transaction.incrBy("debt", amountToSubtract);
               transaction.exec();
               balance = Integer.parseInt(jedis.get("balance"));
               debt = Integer.parseInt(jedis.get("debt"));
   
               System.out.println("*********" + balance);
               System.out.println("*********" + debt);
               return true;
           }
       }
   
       public static void main(String[] args) throws InterruptedException {
           TestWatch testWatch = new TestWatch();
           boolean retValue = testWatch.transMethod();
           System.out.println("main retValue ------- : " + retValue);
       }
   }
   ```

3. 在7秒内在redis-cli中执行命令(模拟并发时，balance被其他用户修改了)

   ```redis
   127.0.0.1:6379> set balance 5
   OK
   ```

4. 执行结果

   ```
   modify
   main retValue ------- : false
   ```

5. 修改balance为100，再次执行程序，不过这次就不在redis-cli中执行任何操作，结果如下

   ```
   **************transaction
   *********90
   *********10
   ```



## 主从复制

准备两台redis服务器，6379和6380，各自独立

```java
public class TestMS {

    public static void main(String[] args) {
        Jedis jedis_m = new Jedis("123.57.159.15", 6379);
        Jedis jedis_s = new Jedis("123.57.159.15", 6380);

        jedis_s.slaveof("127.0.0.1", 6380);
        jedis_m.set("k1", "v1");
        System.out.println(jedis_s.get("k1"));
    }
}
```

```
v1
```



## JedisPool

获取JedisPool类:

```java
public class JedisPoolUtil {

    private static volatile JedisPool jedisPool = null;

    public static JedisPool getJedisPoolInstance() {
        if (jedisPool == null) {
            synchronized (JedisPoolUtil.class) {
                if (jedisPool == null) {
                    JedisPoolConfig poolConfig = new JedisPoolConfig();
                    //设置最大连接数
                    poolConfig.setMaxTotal(1000);
                    //设置一个pool中最多有几个空闲的redis
                    poolConfig.setMaxIdle(32);
                    //表示当borrow一个jedis实例时，最大等待时间。超出这个时间，则直接抛JedisConnectionException。
                    poolConfig.setMaxWaitMillis(10*1000);
                    //获得一个jedis实例的时候是否检查连接可用性，如果为true,则得到的jedis实例均是可用的
                    poolConfig.setTestOnBorrow(true);
                    jedisPool = new JedisPool(poolConfig, "123.57.59.15", 6379);
                }
            }
        }
        return jedisPool;
    }

    public static void release(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }
}
```

获取Jedis测试类：

```java
public class TestPool {

    public static void main(String[] args) {

        Jedis jedis = null;
        JedisPool jedisPool = JedisPoolUtil.getJedisPoolInstance();
        jedis = jedisPool.getResource();
        System.out.println(jedis.ping());
    }
}
```

结果

```
PONG
```



