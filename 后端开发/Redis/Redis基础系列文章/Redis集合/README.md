# Redis集合(Set)

## sadd/smembers/sismember

sadd 为集合增加值

smembers 查看集合的元素

sisimember 判断某个元素是否是集合中的元素

```redis
127.0.0.1:6379> SADD set01 1 2 3 4 5 5
(integer) 5
127.0.0.1:6379> SMEMBERS set01
1) "1"
2) "2"
3) "3"
4) "4"
5) "5"
127.0.0.1:6379> SISMEMBER set01 6
(integer) 0
```



## scard

获取集合里元素格式

```redis
127.0.0.1:6379> SCARD set01
(integer) 5
```



## srem key value

删除集合里面的元素

```redis
127.0.0.1:6379> SREM set01 1 2 3
(integer) 3
127.0.0.1:6379> SMEMBERS set01
1) "4"
2) "5"
127.0.0.1:6379> SREM set01 1 
(integer) 0
```



## srandmember key num

从集合中随机获取指定数量个元素

```redis
127.0.0.1:6379> SMEMBERS set01
1) "1"
2) "2"
3) "3"
4) "4"
5) "5"
6) "6"
7) "7"
8) "8"
9) "9"
127.0.0.1:6379> SRANDMEMBER set01
"8"
127.0.0.1:6379> SRANDMEMBER set01
"1"
127.0.0.1:6379> SRANDMEMBER set01
"6"
127.0.0.1:6379> SRANDMEMBER set01
"3"
127.0.0.1:6379> SRANDMEMBER set01 2
1) "5"
2) "6"
127.0.0.1:6379> SRANDMEMBER set01 2
1) "8"
2) "9"
127.0.0.1:6379> SRANDMEMBER set01 2
1) "6"

```



## spop key

随机出栈

```redis
127.0.0.1:6379> SPOP set01
"8"
127.0.0.1:6379> SPOP set01 2
1) "4"
2) "9"
127.0.0.1:6379> SPOP set01 2
1) "2"
2) "7"
127.0.0.1:6379> SMEMBERS set01
1) "1"
2) "3"
3) "5"
4) "6"
```



## smove key1 key2 value

将集合1中的某个值移动到集合2

```redis
127.0.0.1:6379> keys *
1) "set01"
127.0.0.1:6379> SMOVE set01 set02 3
(integer) 1
127.0.0.1:6379> keys *
1) "set02"
2) "set01"
127.0.0.1:6379> SMEMBERS set02
1) "3"
```



## 数学集合类

### sdiff 差集

在第一个set里面而不在后面任何一个set里的项

```redis
127.0.0.1:6379> SDIFF set01 set02
1) "1"
2) "5"
3) "6"
127.0.0.1:6379> SDIFF set01 set02 set03
1) "1"
2) "5"
3) "6"
```



### sinter 交集

取出所有集合都有的项



### sunion 并集

取出全部集合的项

