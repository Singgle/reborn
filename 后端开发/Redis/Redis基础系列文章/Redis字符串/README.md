# Redis字符串

## set/get/del/append/strlen

set设置值

get获取值

del删除值

append向字符串追加值

strlen查看字符串长度

```redis
127.0.0.1:6379> SET k1 123
OK
127.0.0.1:6379> APPEND k1 456
(integer) 6
127.0.0.1:6379> GET k1
"123456"
127.0.0.1:6379> STRLEN k1
(integer) 6
127.0.0.1:6379> DEL k1
(integer) 1
```



## incr/decr/incrby/decrby

一定要是字符串才能加减

incr自增

decr自减

incrby 以指定步长增加

decrby 以指定步长减少

```redis
127.0.0.1:6379> SET k2 1
OK
127.0.0.1:6379> INCR k2
(integer) 2
127.0.0.1:6379> DECR k2
(integer) 1
127.0.0.1:6379> INCRBY k2 3
(integer) 4
127.0.0.1:6379> DECRBY k2 4
(integer) 0
```



## getrange/setrange

getrange 获取指定区间范围内的值

getrange 0 -1表示全部

setrange key index value 设置指定区间范围内的值

```redis
127.0.0.1:6379> SET k3 123456789
OK
127.0.0.1:6379> GETRANGE k3 1 6
"234567"
127.0.0.1:6379> SETRANGE k3 1 asd
(integer) 9
127.0.0.1:6379> GETRANGE k3 0 -1
"1asd56789"
```



## setex(set with expire)

在设置值的同时给定过期时间

```redis
127.0.0.1:6379> SETEX k4 10 v4
OK
127.0.0.1:6379> TTL k4
(integer) 6
```



## setnx(set if not exist)

设置值(只有在指定的key不存在时才设置值)

```redis
127.0.0.1:6379> SETNX k3 4
(integer) 0
127.0.0.1:6379> SETNX k4 v4
(integer) 1
```



## mset/mget/msetnx

mset同时设置多个值

mget同时获取多个值

msetnx同时设置多个值(只有在指定的key不存在时才设置)：只有有其中一个key存在，那个所有的key都会设置失败

```redis
127.0.0.1:6379> MSET k1 v1 k2 v2 k3 v3
OK
127.0.0.1:6379> MGET k1 k2 k3
1) "v1"
2) "v2"
3) "v3"
127.0.0.1:6379> MSETNX k3 a k4 v4 
127.0.0.1:6379> MSETNX k5 v5 k6 v6
(integer) 1
```



## getset

先获取值再设置值

```redis
127.0.0.1:6379> GETSET k7 v7
(nil)
127.0.0.1:6379> GETSET k7 v8
"v7"
```

