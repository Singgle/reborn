# Redis哈希

KV模式不变，但是V是一个键值对



## hset/hget/hmset/hmget/hgetall/hdel

hset 设置值

hget 获取值

hmset 设置多个值

hmget 获取多个值

hgetall 获取全部值

hdel 删除值

```redis
127.0.0.1:6379> HSET student id 1
(integer) 1
127.0.0.1:6379> HGET student id
"1"
127.0.0.1:6379> HMSET student name xiaohong age 18
OK
127.0.0.1:6379> HMGET student id name age
1) "1"
2) "xiaohong"
3) "18"
127.0.0.1:6379> HGETALL student
1) "id"
2) "1"
3) "name"
4) "xiaohong"
5) "age"
6) "18"
127.0.0.1:6379> HDEL student name
(integer) 1
127.0.0.1:6379> HGETALL student
1) "id"
2) "1"
3) "age"
4) "18"
```



## hlen

获取哈希的大小

```redis
127.0.0.1:6379> HLEN student
(integer) 2
```



## hexists key value-key

判断key里面的某个值的key是否存在

```redis
127.0.0.1:6379> HEXISTS student id
(integer) 1
127.0.0.1:6379> HEXISTS student name
(integer) 0
```



## hkeys/hvals

hkeys 取出某个key中的值的所有key

hvals 取出某个key中的值的所有value

```redis
127.0.0.1:6379> HKEYS student
1) "id"
2) "age"
127.0.0.1:6379> HVALS student
1) "1"
2) "18"
```



## hincrby/hincrbyfloat

将哈希里面的某个值 增加指定值

```redis
127.0.0.1:6379> HGETALL student
1) "id"
2) "1"
3) "age"
4) "18"
127.0.0.1:6379> HINCRBY student age 2
(integer) 20
127.0.0.1:6379> HINCRBYFLOAT student age 0.5
"20.5"
127.0.0.1:6379> HINCRBY student age 0.5
(error) ERR value is not an integer or out of range
```

 

## hsetnx

只有在指定key中的 value不存在的时候，才会设置值。

```redis
127.0.0.1:6379> HGETALL student
1) "id"
2) "1"
3) "age"
4) "20.5"
127.0.0.1:6379> HSETNX student name xiaoming
(integer) 1
127.0.0.1:6379> HSETNX student age 25
(integer) 0
127.0.0.1:6379> HGETALL student
1) "id"
2) "1"
3) "age"
4) "20.5"
5) "name"
6) "xiaoming"
```

