# Redis配置文件

## Units单位

```conf
# 1k => 1000 bytes
# 1kb => 1024 bytes
# 1m => 1000000 bytes
# 1mb => 1024*1024 bytes
# 1g => 1000000000 bytes
# 1gb => 1024*1024*1024 bytes
```

配置大小单位，定义了基本的度量单位，只支持bytes，不支持bit

大小写不敏感



## INCLUDES包含

可以通过INCLUDES将其他redis配置文件包含进来

```conf
# include /path/to/local.conf
# include /path/to/other.conf
```



## NETWORK

```conf
# redis监听端口
port 6379

tcp-backlog 511

# 绑定主机地址
bind 127.0.0.1

# Close the connection after a client is idle for N seconds (0 to disable)
timeout 0

# tcp keepalive检测，每隔指定时间进行一个存活检查。单位为秒，设置为0表示不进行keepalive检测
tcp-keepalive 300
```

tcp-backlog 设置tcp的backlog，backlog其实是一个连接队列，backlog队列总和 = 未完成三次握手队列 + 已经完成三次握手队列。

在高并发环境下，你需要一个高backlog值来避免客户端连接问题。注意Linux内核会将这个值减小到/proc/sys/net/core/somaxconn的值。

所以需要确认增大somaxconn和tcp_max_syn_backlog两个值来达到想要的效果。



## GENERAL

```conf
# By default Redis does not run as a daemon. Use 'yes' if you need it.
# Note that Redis will write a pid file in /var/run/redis.pid when daemonized.
daemonize no

# 修改进程管道id文件存放路径
pidfile /var/run/redis_6379.pid

# 设置日志级别，可以是 debug verbose notice warning中其中一个
loglevel notice

# 设置日志文件名，为空字符串表示输出到标准输出
logfile ""

# 是否启动系统日志
# syslog-enabled no

# 指定系统日志以redis开头，可修改
# syslog-ident redis

# 指定syslog设备. Must be USER or between LOCAL0-LOCAL7.
# syslog-facility local0

# 设置数据库数量
databases 16
```



## SNAPSHOTTING

```conf

```



## REPLICATION 复制



## SECURITY 安全

注：以下是命令

获取密码

```redis
127.0.0.1:6379> config get requirepass
1) "requirepass"
2) ""
```

设置密码

```redis
127.0.0.1:6379> config set requirepass '123456'
OK
```

退出redis命令行，再重新进入

```redis
127.0.0.1:6379> ping
(error) NOAUTH Authentication required.
127.0.0.1:6379> AUTH 123456
OK
127.0.0.1:6379> ping
PONG
```

我们看到直接输入命令不允许了，但输入密码之后，就可以执行命令了。



## CLIENTS

```redis
# 最大连接数
# maxclients 10000
```





## MEMORY MANAGEMENT

```conf
# 最大内存
# maxmemory <bytes>

# 最大内存策略
# maxmemory-policy noeviction

# maxmemory-samples 5
```

maxmemory-policy，内存管理策略。当内存不足时，如何清理内存：

volatile-lru -> 最近最少使用，仅对设置了expire的键有效

allkeys-lru -> 对所有的键使用最近最少使用

volatile-lfu -> 最近经常使用，仅对设置了expire的键有效

allkeys-lfu -> 最近经常使用

volatile-random -> 随机删除key，仅对设置了expire的键有效

allkeys-random ->  所有的键中随机删除

volatile-ttl -> 移除TTL最小的key

noeviction -> 永远不过期，内存不够时抛出异常，默认



maxmemory-samples：设置样本数量，LRU算法和最小TTL算法都并非是精确的算法，而是估算值，所以你可以设置样本的大小。

redis默认会检查这么多个key并选择其中LRU的那个。