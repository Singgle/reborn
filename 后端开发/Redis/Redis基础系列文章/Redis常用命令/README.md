# Redis常用命令

## 杂记



### PING

PING命令使用客户端向 Redis 服务器发送一个 PING ，如果服务器运作正常的话，会返回一个 PONG 

```redis
> ping
PONG
```



### SELECT 切换数据库

```redis
127.0.0.1:6379> SELECT 1
OK
127.0.0.1:6379[1]> 
```



### DBSIZE

DBSIZE查看当前数据库的key数量

```redis
127.0.0.1:6379> DBSIZE
(integer) 0
```



### FLUSHDB

清空当前库



### FLUSHALL

清空所有数据库



## 键 key

### keys *

列出当前数据库的所有的键

```redis
127.0.0.1:6379> keys *
1) "k1"
```



### exists key

判断指定的key是否存在

```redis
127.0.0.1:6379> EXISTS k1
(integer) 1
127.0.0.1:6379> EXISTS k2
(integer) 0
```



### move key db

将指定的键移动到指定的数据库中

```redis
127.0.0.1:6379> move k1 2
(integer) 1
```



### expire key

为指定的key设置过期时间

下面设置k1  10秒后过期

```redis
127.0.0.1:6379> EXPIRE k1 10
(integer) 1
```



### ttl key

查看指定的键还有多少秒过期，-1表示永不过期，-2表示已经过期

```redis
127.0.0.1:6379> TTL k1
(integer) 1
127.0.0.1:6379> TTL k1
(integer) -2
```



### type key

查看指定的key是什么类型

```redis
127.0.0.1:6379> TYPE k1
string
```







