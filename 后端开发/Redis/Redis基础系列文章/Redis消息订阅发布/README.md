# Redis消息订阅发布

现在有两个客户端，分别代表订阅方和发布方。

sub：订阅方订阅了c1 c2 c3三个频道

```redis
127.0.0.1:6379> SUBSCRIBE c1 c2 c3
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "c1"
3) (integer) 1
1) "subscribe"
2) "c2"
3) (integer) 2
1) "subscribe"
2) "c3"
3) (integer) 3
```



pub：然后发布方在c2频道发布了 hello-world消息

```redis'
127.0.0.1:6379> PUBLISH c2 hello-world
(integer) 1
```



sub：订阅方就会收到发布方推送的消息

```redis
127.0.0.1:6379> SUBSCRIBE c1 c2 c3
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "c1"
3) (integer) 1
1) "subscribe"
2) "c2"
3) (integer) 2
1) "subscribe"
2) "c3"
3) (integer) 3
1) "message"
2) "c2"
3) "hello-world"
```



订阅方也可以使用通配符方式来订阅多个频道

```redis
PSUBSCRIBE new*
```

那么以new开头的频道都会接受到消息