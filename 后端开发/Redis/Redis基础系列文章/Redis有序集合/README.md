# Redis有序集合(Zset)

在set基础上，加上一个score值。

之前set是k1 v2 k2 v2

现在Zset是k1 score1 v1 k2 score2 v2



## zadd/zrange

zadd 增加元素

zrange 查询有序集合元素

```redis
127.0.0.1:6379> ZADD zset01 60 v1 70 v2 80 v3 90 v4 100 v5
(integer) 5
127.0.0.1:6379> ZRANGE zset01 0 -1
1) "v1"
2) "v2"
3) "v3"
4) "v4"
5) "v5"
127.0.0.1:6379> ZRANGE zset01 0 -1 withscores
 1) "v1"
 2) "60"
 3) "v2"
 4) "70"
 5) "v3"
 6) "80"
 7) "v4"
 8) "90"
 9) "v5"
10) "100"
```



## zrangebyscore key startScore endScore

查询指定分数范围内的数据

- withscores可以带上分数

- ( 表示不包含

- limit的作用是返回限制，第一个参数是下标，从这个下标的元素开始截取；第二个参数是要截取的个数

```
127.0.0.1:6379> ZRANGEBYSCORE zset01 60 90
1) "v1"
2) "v2"
3) "v3"
4) "v4"
127.0.0.1:6379> ZRANGEBYSCORE zset01 60 90 withscores
1) "v1"
2) "60"
3) "v2"
4) "70"
5) "v3"
6) "80"
7) "v4"
8) "90"
127.0.0.1:6379> ZRANGEBYSCORE zset01 (60 90
1) "v2"
2) "v3"
3) "v4"
127.0.0.1:6379> ZRANGEBYSCORE zset01 (60 (90
1) "v2"
2) "v3"
127.0.0.1:6379> ZRANGEBYSCORE zset01 60 90 limit 2 2
1) "v3"
2) "v4"
127.0.0.1:6379> ZRANGEBYSCORE zset01 60 90 limit 2 1
1) "v3"
```



## zrem key value

删除元素

```redis
127.0.0.1:6379> ZRANGE zset01 0 -1 withscores
 1) "v1"
 2) "60"
 3) "v2"
 4) "70"
 5) "v3"
 6) "80"
 7) "v4"
 8) "90"
 9) "v5"
 10) "100"
127.0.0.1:6379> ZREM zset01 v2
(integer) 1
127.0.0.1:6379> ZRANGE zset01 0 -1 withscores
1) "v1"
2) "60"
3) "v3"
4) "80"
5) "v4"
6) "90"
7) "v5"
8) "100"
```



## zcard/zcount/zrank key values

zcard 查看有序集合的元素个数

zcount key score1 score2 查看指定分数区间的元素个数

zrank key value 查看指定值在有序集合中的下标

zscore key value 获取指定值对应的分数

```redis
127.0.0.1:6379> ZCARD zset01
(integer) 4
127.0.0.1:6379> ZCOUNT zset01 60 90
(integer) 3
127.0.0.1:6379> ZRANK zset01 v4
(integer) 2
127.0.0.1:6379> ZSCORE zset01 v4
"90"
```



## zrevrank key value

逆序获取下标值

```redis
127.0.0.1:6379> ZRANGE zset01 0 -1
1) "v1"
2) "v3"
3) "v4"
4) "v5"
127.0.0.1:6379> ZREVRANK zset01 v5
(integer) 0
```



## zrevrange

逆序获取有序集合里面的元素

```redis
127.0.0.1:6379> ZREVRANGE zset01 0 -1
1) "v5"
2) "v4"
3) "v3"
4) "v1"
127.0.0.1:6379> ZRANGE zset01 0 -1
1) "v1"
2) "v3"
3) "v4"
4) "v5"
```



## zrevrangebyscore

逆序获取有序集合里面 规定的分数范围的元素

```redis
127.0.0.1:6379> ZREVRANGEBYSCORE zset01 60 90
(empty array)
127.0.0.1:6379> ZREVRANGEBYSCORE zset01 90 60
1) "v4"
2) "v3"
3) "v1"
```





