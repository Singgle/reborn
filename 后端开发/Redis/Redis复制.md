# Redis复制

尽管Redis的性能很强大，但是面对大量的请求需要处理，也会出现无法及时处理请求的情况。这个时候，我们可以使用复制来扩展Redis的性能。

通过创建主Redis服务器的数据副本，在主Redis服务器数据改变的同时同步数据副本，完成数据的同步。将读请求交给从服务器来处理，从而增强Redis的性能。



## Redis的复制选项

我们知道，当一个服务器连接主服务器的时候，并向主服务器发送SYNC来完成复制操作。如果主服务器没有正在执行BGSAVE或者刚刚没有执行BGSAVE命令，那么主服务就会触发一次BGSAVE命令。

因此，为了开启Redis的复制功能，主服务器必须需要配置dir选项和dbfilename选项。

开启从服务器所必须的配置项是slaveof。当Redis服务器的配置文件包含slave host port选项，Redis服务器就会连接指定的Redis主服务器。通过发送slave no one命令，可以让Redis服务器终止复制操作，同时不再接收主服务器的数据更新。同样，通过发送slave host port命令，可以让Redis服务器开始复制指定主服务器的数据。



## Redis复制的启动过程

当从服务器连接主服务器，主从服务器执行的所有操作：

| 步骤 | 主服务器操作                                                 | 从服务器操作                                                 |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1    | 等待命令                                                     | 连接(或重连接)主服务，发送SYNC命令                           |
| 2    | 接收SYNC命令后执行BGSAVE命令，并用缓冲区存储执行BGSAVE命令之后执行的所有写命令 | 根据配置选项决定使用当前的数据来响应客户端请求，还是直接向客户端发送请求错误 |
| 3    | BGSAVE执行完毕，向从服务器发送快照文件，在发送期间继续用缓冲区存储执行的写命令 | 接收主服务器发送的快照文件，抛弃之前旧的数据                 |
| 4    | 向从服务器发送自执行BGSAVE命令之后，缓冲区存储的所有写命令   | 解释主服务器发送的快照文件，并正常接收请求                   |
| 5    | 缓冲区存储的所有写命令发送完毕。从现在开始，每执行一个写命令，就向从服务器发送同样的命令 | 执行主服务器发送的所有写命令。从现在开始，接收并执行主服务器发送的每条写命令 |



当有多个从服务器尝试连接同一个服务器的时候，可能会出现下面两个情况之一：

| 从服务器连接主服务器的时机        | 主服务器执行的操作                                           |
| --------------------------------- | ------------------------------------------------------------ |
| 上表步骤3没有执行的时候           | 所有从服务器都会接收到相同的快照文件和写命令                 |
| 上表步骤3正在执行或者已经执行完成 | 主服务会先和较先连接的从服务器执行复制需要的5个步骤，然后与新连接的从服务器执行一次新的步骤1到步骤5 |



## 主从链

Redis的从服务器也可以拥有属于自己的从服务器，这样就可以构成一条主从链。

从服务器对从服务器进行复制在操作上从服务器对主服务器进行复制的唯一区别：如果从服务器X拥有从服务器Y，从服务器X在执行上表步骤4(解析主服务器发来的快照文件，并正常接收请求命令)时，会断开从服务器Y的连接，导致从服务器Y需要重新连接并同步。



## 检验硬盘写入

通过Redis持久化和主从服务器，我们可以将数据保存到不同的地方。

Redis写命令在主服务器执行之后，还需要等待从服务器接收命令并执行，这样数据才会切实在两个不同的机器存储下来。

为了验证主服务器是否已经将写命令传入给从服务器，我们可以在向主服务器写入真正的数据之后，再向主服务器写入一个唯一的虚拟值，然后通过查询从服务器是否存在这个虚拟值，我们就能知道写命令是否传入给从服务器。即便是这样，我们仍然无法知道这个数据是否存入到了硬盘。我们可以通过info命令查看aof_pending_bio_fsync属性的值是否为0，如果是的话，就代表缓冲区的数据已经全部落到硬盘中了。

