# Redis的setbit的实际应用

我们都知道Redis的五种数据类型，但是其实Redis还有其他的数据类型，其中一种就是setbit。

我们知道计算机使用0,1来存储数据，最小的存储单位是bit。

位操作是非常快的操作。

```
1 Byte(B) = 8bit
1 Kilo Byte(KB) = 1024 B
1 Mega Byte(MB) = 1024 KB
1 Giga Byte(GB) = 1024 MB
```



## setbit的一些常见命令

```
# 设置key所存储的字符串指定偏移量上的位
SETBIT key_name offset value

# 获取key所存储的字符串指定偏移量的位
GETBIT key_name offset
```



## setbit的实际应用

场景：有一亿个用户登录，需要统计连续7天登录的用户



**传统方案：**

使用传统的数据库，我们会用如下形式的表来记录用户的登录数据

```
userid  date  		flag
1		20211012	1
1		20211013	1
1		20211014	1
2		20211012	1
2		20211014	1
...
```

之后再使用group by、sum操作来统计连续7天登录的用户。

但是很显然，这么巨大的数据的运算是非常慢的。



**使用setbit：**

使用Redis的setbit来完成统计连续7天登录的用户，我们可以将一个key存储的值来统计所有用户在一天内的登录状态。

id为1的用户登录，我们可以设置offset为1的位值为1。这样存储1亿用户的登录状态，我们只需要约12M的内存就可以存储。

我们将每一天的用户登录数据进行**与运算**就可以求出连续登录的用户数据。

示例：使用setbit计算2天连续登录的用户

```redis
# 星期一登录的数据
setbit mon 1 1
setbit mon 2 1
setbit mon 3 1
# 星期二登录的数据
setbit tue 1 1
setbit tue 3 1
# 进行与操作
bitop and res mon tue
```

在这个实例中，这个key为res的结果就是我们想要获得的连续2天登录的用户数据。