

# RocketMQ

## RocketMQ基本概念

### Message

消息是指消息系统传输消息的物理载体，生产和消费的最小单位。每个消息必须属于一个Topic。

RocketMQ中的Message有一个唯一的标识MessageId，还可以携带业务标识key。

生产者生产消息的时候会自动生成一个MessageId，同时消息到达Broker的时候也会自动生成一个MessageId。



### Topic

主题表示一类消息的集合，是RocketMQ进行消息订阅的基本单位。



### Tag

为消息设置的标志，用于区分同一主题下不同类型的消息。



### Queue

存储消息的物理实体。一个主题下有多个Queue。一个Topic中的Queue只能被一个消费者组的一个消费者消费。(一个消费者组的一个消息者可以消费一个主题下的多个Queue)



### Producer Group

生产者组是同一类生产者的集合。



### Consumer Group

消费者组是同一类消费者的集合。消费者组使得在消息消费方面，实现负载均衡和容错的目标变得十分容易。

同一个消息者组的消费者必须订阅完全相同的主题。



## RocketMQ系统架构

### Name Server(名字服务)

Name Server是Broker和Topic的注册中心，提供Broker管理和路由信息管理。

- Broker管理：接收Broker集群的注册信息，提供心跳检测服务。
- 路由信息管理：消费者和生产者从Name Server中获取Broker信息和队列信息，从而完成消息的消费和投递。



#### 路由注册

Name Server保存Broker集群信息以及队列信息。

Name Server是无状态的，意思就是Name Server集群之间是不进行消息通信的。那么，Name Server是如何保存Broker信息的？

Broker会配置所有的Name Server的地址，启动的时候会遍历所有Name Server，然后将自己相关信息上报。



#### 路由剔除

Name Server提供心跳检测服务，在指定时间内没有接收到Broker的心跳包，就会将这个Broker从路由信息中剔除。



#### 路由发现

RocketMQ的路由发现采用的是pull模型。当NameServer的路由信息发生变化的时候，不会主动推送最新消息给客户端，而是客户端定时从Name Server中拉取最新的消息。



#### 客户端选择Name Server的策略

客户端在配置的时候配置了全部Name Server的地址，那么客户端到底连接的是哪个Name Server？

客户端会先生成一个随机数，然后跟Name Server的节点数取模，得到的值就是对应Name Server的索引。如果连接失败，客户端就会采用轮询策略。



### Broker

Broker负责完成消息的存储和转发。Broker接收存储生产者发送的消息，然后为消费者的拉取做准备。Broker同时也存储着消息相关的元数据，包括消费组消费进度、主题、队列等。	



### 消费者和生产者



## RocketMQ工作流程

RocketMQ收发消息的工作流程：

1. Name Server启动后，会监听端口，等待Broker、Producer、Consumer连接。
2. 启动Broker之后，Broker会与所有的Name Server建立并保持长连接，然后定时发送心跳包。
3. Producer发送消息时，会先与Name Server集群中的其中一台建立长连接，并拉取路由信息。然后根据算法策略选择发送的队列之后，与队列所在的Broker建立长连接并发送消息。Producer在初次拉取路由信息之后会保存到本地，之后定时从Name Server中拉取最新路由信息。
4. Consumer跟Producer类似。Consumer在消费消息的时候，会先与其中一台Name Server建立长连接并拉取路由信息。然后根据算法策略选择消费的队列，与队列所在的Broker建立连接之后开始消费消息。Consumer在拉取到路由信息之后，也要定时拉取路由信息。与Producer不同的是，Consumer还会向Broker发送心跳包，保证Broker的存活状态。



## RocketMQ的工作原理

### Queue选择算法

对于无序消息，常见的Queue选择算法有两种：

1. 轮询算法

   默认使用的是轮询算法。该算法可以保证每个Queue可以均匀的分配到消息。

   但是，使用该算法可能会存在一个问题，当某些Borker上的Queue投递延迟比较严重，可能会导致该队列产生较大的投递延迟，影响消息队列整体的投递性能。

2. 最小投递延迟算法

   该算法会统计每个队列的平均投递延迟，然后会将消息投递给投递延迟最小的队列。如果投递延迟相同，则使用轮询算法。使用该算法可以有效提升消息队列的消息投递性能。

   但是，使用该算法，可能会导致某个消费者的压力较大，降低消费者的消费能力，从而产生消息堆积。



### 消息存储

RocketMQ的消息存储在本地文件系统，这些相关的文件默认保证在用户的主目录下的store目录下。

下面是对store目录下的文件和目录的简单介绍：

- abort：Broker启动的时候，RocketMQ会创建这个文件。RocketMQ正常关闭之后，这个文件也会删除。如果RocketMQ关闭之后，这个文件还存在，说明RocketMQ未正常关闭。
- checkpoint：存放这commitlog、consumequeue、index文件的正常刷盘时间。
- commitlog：存放这commitlog文件。
- config：存放着RocketMQ的配置数据
- consumequeue：存放着RocketMQ的队列。
- index：存放着RocketMQ的消息索引文件
- lock：存放着RocketMQ运行时用到的全局资源锁。



#### commitlog文件

在commitlog目录下存放着许多commitlog(mappedFile)文件。mappedFile文件名由20个10进制数组成，表示着mappedFile文件中第一条消息的起始偏移量。每个mappedFile文件的大小小于等于1G。

mappedFile文件内容由一个个**消息单元**组成。每个消息单元包括消息总长度(MsgLen)、消息的物理位置(physicalOffset)、消息体内容(Body)、消息体长度(BodyLength)、消息主题(Topic)、消息主题长度(TopicLength)、消息生产者(BornHost)、消息发送时间戳(BornTimestamp)、消息所在的队列QueueId、消息在Queue中存储的偏移量(QueueOffset)等消息相关属性。



#### consumequeue

RocketMQ会为每个Topic在comsumequeue目录下创建同名目录，同时会为每个Topic的Queue创建一个目录，目录名为queueId。在这个queueId目录下会存放许多consumequeue文件。consumequeue文件是commitlog文件的索引，RocketMQ可以通过consumequeue文件找到对应的消息。

consumequeue文件名由20个10进制数字组成，表示当前文件第一个索引条目的起始偏移量。但是跟commitlog文件不同，每个consumequeue文件的大小都是固定的。因此后续的文件名都是固定的。

consumequeue文件内容的组成是一个个的索引条目。consumequeue索引条目的结构如下：

![consumequeue索引条目结构](img/image-20220205200941120.png)

每个conumequeue文件可以包含30万个索引条目。**每个索引条目包含三个消息重要属性：消息在commitlog文件的偏移量、消息长度、消息tag的hashcode。**这些属性加起来需要占用20个字节，所以每个consumequeue文件最大可以为30w*20字节。



#### 消息的写入和读取过程

##### 消息的写入

1. Broker会根据queueId，获取到该消息在consumequeue的消息偏移量
2. 然后queueId、consumequeue offset等消息属性封装成消息单元，并写入到commitlog文件
3. 同时形成索引条目并写入到对应的consumequeue文件中。



##### 消息的读取

1. 消费者会先获取到要消费消息所在queue的消费偏移量，然后计算出该消息的消息offset
2. 消费者会向Broker发送拉取消息请求，其中会包括要消费消息的queue、消息offset、消息tag。
3. Broker计算出在consumequeue中的queueOffset
3. 从该queueOffset处开始往后寻找第一个指定tag的索引条目
3. 解析该索引条目的前8个字节(即commitlog的offset)，即可定位到commitlog中的消息单元
4. 从commitlog中读取消息单元，并发送给消费者



#### RocketMQ是如何提升消息的读写性能？

RocketMQ无论是消息还是消息索引都是存放在硬盘上，这样不会影响RocketMQ的消息消费性能吗？

1. RocketMQ使用mmap零拷贝技术，使得对文件的操作转化成对内存地址的操作，极大提升了文件的读写性能
2. RocketMQ的consumequeue数据是顺序存储的，还引入了PageCache预读取机制，使得对consumequeue文件的读取近乎于直接从内存读取
3. RocketMQ中可能影响性能的是commitlog文件的读取，因为对commitlog文件的读取大多数是随机读取，而随机读取是很影响性能的

> PageCache，页缓存机制，用于加速对文件的读写操作。PageCache对文件进行顺序读取的性能近乎于对内存的读取。这是因为页缓存机制将一部分的内存用作PageCache。
>
> 写操作：OS会将数据写入到PageCache中，随后会将异步方式由pdflush(page dirty flush)内核线程将Cache中的数据写入到磁盘
>
> 读操作：读取数据先从PageCache读取，如果没有命中，则OS从硬盘中读取数据加载到内存，同时还会将相邻数据块的数据进行预读取



### indexFile

RocketMQ除了通过指定topic来消费消息之外，还可以通过key来消费指定key的消息。该查询是通过store目录下的index子目录下的indexFile来完成的快速查询。

indexFile的索引数据是包含key的消息被发送到Broker的时候写入的。如果发送的消息不包含key的话，则不会写入。



#### 索引条目结构

·每个Broker都会有多个indexFile文件。每个indexFile文件都以时间戳为名(该文件创建的时间戳)。

indexFile由indexHeader、slot、indexes索引数据组成。每个indexFile包含500w个slot槽位。每个slot槽位又可能挂载很多的index索引单元。

![indexFile文件结构](img/image-20220206154358228.png)

**indexHeader固定40个字节**，结构如下：

![indexHeader结构](img/image-20220206154456480.png)

- beginTimestamp：该indexFile第一条消息的存储时间
- endTimestamp：该indexFile最后一条消息的存储时间
- beginPhyoffset：该indexFile第一条消息在commitlog文件的offset
- endPhyoffset：该indexFile最后一条消息在commitlog文件的offset
- hasSlotCount：该indexFile文件填充了index的slot槽位数量
- indexCount：该indexFile文件包含的索引个数



slot和index的关系结构如下(**在实际存储的时候，indexes其实是存储在slot后面的**)

![slot和index的关系结构](img/image-20220206155107525.png)

**key的hash值 % 500w**就是消息索引单元存储的槽位，同时会将该slot槽位的值修改为当前索引单元的indexNo。当多个索引单元出现在同一个槽位的时候，slot的值存储的是最新索引单元的indexNo，而新加入的索引单元都会指向前一个索引单元。这样只要消息key对应的槽位，那么找到最新的索引单元，通过这个索引单元就能找到之前的所有索引单元。

> indexNo是indexFile中的流水号，从0开始递增。一个indexFile中的所有indexNo都是递增的。

**index索引单元默认是20个字节**，存放着下面这四个属性：

![image-20220206160112490](img/image-20220206160112490.png)

- keyHash：消息对应key的hashCode
- phyOffset：消息在commitlog中的offset
- timeDiff：该索引数据对应消息存储时间与当前indexFile创建时间的差值
- preIndexNo：前一个索引单元的indexNo



#### indexFile文件名的作用

indexFile文件名以该文件创建的时间戳命名的原因是：查询指定key的消息的时候，还需要传一个时间，表示查询不大于该时间的最新的消息。indexFile文件名以时间戳命名可以简化查询，提高查询效率。



#### 索引查询的过程

1. 传入key和时间戳开始查询
2. 根据时间戳定位到消息所在的indexFile文件
3. 计算传入的时间戳和indexFile创建的时间的差值、计算出key的hash值
4. 计算出消息所在的槽位值n(key的hash值 % 500w)
5. 根据slot槽位序号定位到slot在indexFile文件的位置(40 + (n - 1) * 4)
6. 找到slot槽位后读取slot值，即计算出当前slot槽位最新的indexNo值
7. 根据indexNo计算该index单元在indexFile的位置(40 + 500w * 4 + (m - 1) * 20)
8. 计算出前面计算出的时间差值  - 当前index单元的timeDiff，若<0则转至步骤9，否则转至步骤10
9. 读取该index单元的preIndexNo作为下一个要查找的index单元的indexNo，转至步骤7
10. 读取该index单元的phyoffset，定位到对应的消息并返回



### 消息的获取和消费

消费者获取消息的方式有两种：push模式和pull模式.

消息者组消费消息的方式也有两种：集群消费Clustering和广播消费Broadcasting。



#### 消息获取方式

##### pull模式

消费者主动从Broker中拉取消费。该模式的实时性不强，当Broker接收都新消息的时候，消费者不能及时获取到新消息。



##### push模式

当Broker获取到新消息的时候，会主动推送新消息给消费者。该方式的实时性较强。

该模式是典型的发布-订阅模式。消费者会向关联的queue注册监听器，当接收到新消息的时候，会触发回调方法从而将消息发送给消费者。这是通过长连接来实现，但是维护长连接需要消耗系统资源。s



#### 消息消费模式

##### 广播模式

在该模式下，相同消费组下的每个消费者都会消费同一个Topic下的全量消息。即Topic下的每个消息都会发送给消费组下的每个消费者。



##### 集群模式

在该模式下，相同消费组下的每个消费者会平均消费Topic下的消息。



##### 消费进度保存

广播模式下的消费进度由各消费者保存。这是因为广播模式下的每个消费者消费的是全量消息，每个消费者消费进度都不同。

集群模式下的消费进度保存在Broker中。因为消费组下的每个消费者平均消费Topic下的消费，每个消费只会被消费一次。消费进度会参与到消息的负载均衡中，所以消费进度是需要共享的。



#### Rebalance机制

Rebalance即再平衡，意思是将一个Topic的queue在同一个消费者组下的消费组之间重新分配的过程。

这是为了在Topic的queue数量改变、消费者组的消费者数量改变的时候，进行重新分配。

**Rebalance的限制：**

因为Topic下的queue只能分配给一个消费者组中的其中一个消费者。如果消费者组中的消费者的数量大于可以分配的queue数量，那么多余的消费者则分配不到queue。

**Rebalance的危害：**

- **消费暂停：**举个例子，一个Topic有5个queue，原先只有一个消费者消费。那么这5个queue就会分配给这个消费者消费。现在，我们增加一个消费者，那么就需要暂停部分queue的消费，分配给新增加的消费者。

- **消费重复：**consumer在消费新分配给自己的queue的时候，需要接着之前的消费进度进行消费。但是默认offset是异步提供的，这就会导致实际消费进度和Broker中的消费进度不一致。这样就会导致消费的重复消费。
- **消费突刺：**如果需要重复消费的消息过多，或者消费暂停的时间过长导致积压了过多的消费，就会导致消费者在Rebalance之后瞬间需要消费很多消息

**Rebalance过程：**

Broker中会维护多个Map集合，包括Topic的queue数量、消费者组中消费者的数量。如果发现它们发生变化，就会向消费者组的每个消费者发送Rebalance请求。

消费者在接收到Rebalance请求之后，才会采用queue分配算法计算出自己需要消费的queue



#### queue分配算法

##### 平均分配策略

![image-20220207191053759](img/image-20220207191053759.png)

利用avg = queueCount / consumerCount计算出每个消费者分配到的queue数量。如果存在多出的queue，那么按顺序分配这些多出来的queue。



##### 环形平均策略

![image-20220207194801145](img/image-20220207194801145.png)

环形平均策略就是根据消费者的顺序，逐个分配。这样就不需要事先计算每个消费者需要分配的queue数量。



##### 一致性hash策略

![image-20220207194520668](img/image-20220207194520668.png)

该算法会将consumer的hash值作为node节点存放在hash环上，然后将queue的hash值作为node节点放在hash环上。按照顺时针方向，距离queue最近的那个consumer就是该queue被分配到的consumer。

该算法存在的问题就是分配不均。



##### 同机房策略

![image-20220207195303810](img/image-20220207195303810.png)

该算法会根据queue的部署机房和consume的位置，过滤出和当前consumer相同的queue。然后按照平均分配策略或环形平均策略对同机房queue进行分配。如果没有同机房queue，则按照平均分配策略或者环形平均策略对所有queue进行分配。



##### queue分配算法的比较

平均分配算法和环形平均分配算法的分配效率较高，相比之下一致性hash策略的分配效率较低。

但是一致性hash策略可以有效减少因消费者组的扩容或缩容导致的rebalance需要重新分配的queue数量。因此，一致性hash策略比较适合适用于rebalance频繁出现的场景。



#### 至少一次原则

RocketMQ有一个原则就是，每条消息必须被**成功**消费一次。

conusmer消费完消息之后向消费进度记录器提供其消费消息的offset，并且offset被成功记录到消费进度记录器中，那么这条消息才叫成功消费。



### 订阅关系的一致性

订阅关系的一致性就是，同一个消费者组下的所有消费者所订阅的Topic和Tag以及对消息的消费逻辑必须一致。否则，消费消息的逻辑就会混乱，甚至导致消息丢失。



### 消费幂等

消费幂等的意思就是当消费者对某个消息进行重复消费的时候，重复消费的结果和消费一次的结果是相同的，并且对重复消息的消费不会对应用产生影响。

在互联网应用中，当出现网络不稳定的情况下，就容易出现消息的重复发送或者重复消费。如果对消息的重复消费会影响到业务，那么就需要对消息进行幂等处理。

**在消息队列中什么情况下可能出现消息被重复消费：**

-  **重复发送消息：**当消息发送到Broker并持久化完成时，Broker向生产者进行应答的时候，出现网络问题，导致生产者没有收到应答。那么就会导致生产者再次发送消息。这样Broker就会出现重复消息，就必然会导致重复消费。
- **重复消费消息：**当消息被消费完毕，消费者向Broker发送应答，因网络问题，导致Broker没有收到应答。那么Broker就会再次发送消费给消费者，这就会导致消息被重复消费。
- rebalance导致消息重复。



### 消息堆积和消息延迟

生产者生产消息快，而消费者消费消息慢，导致Broker中存在大量未消费的消息，这就是消息堆积。而消息堆积就会导致消息延迟。



#### 为什么会产生消息堆积？

Consumer若采取长轮询pull模型，那么可能导致消息堆积发生在两个阶段：消息拉取阶段和消息消费阶段。

**消息拉取阶段：**

在消息拉取阶段，消费者从Broker拉取消息到本地缓存队列，这个过程吞吐量很高，这个阶段一般不会导致消息堆积。

**消息消费阶段：**

在消息消费阶段，将本地缓存队列中的消息提交给消费线程对消息进行业务处理，并获得一个结果。这就是消息的消费过程。消费者的消费能力取决于**消息业务耗时**和**消息并发度**。如果消息业务耗时过长，那么整体消息吞吐率就不会高，那么本地消息缓存队列很快就会达到上限，那么消费者就会停止从Broker拉取消息。



#### 消息业务耗时

消息业务耗时主要取决于两个因素：代码计算逻辑耗时、代码IO耗时。

通常情况下，如果代码没有复杂的递归和循环，那么代码计算逻辑耗时相对于代码IO耗时可以忽略。所以代码IO还是才是消息业务耗时的关键。



#### 消息并发度

一般来说，消息的并发度由单节点线程数和节点数来决定。

不过优先调整单节点线程数，若单机线程数达到上限才通过横向扩展来提高消费并发度。



#### 单节点线程数计算

单机线程数不能盲目调大。过大的单机线程数反而会带来大量的线程切换开销。

理想情况下单节点的最优线程计算模型为：C * (T1 + T2) / T1

C：CPU内核数

T1：CPU内部逻辑计算耗时

T2：外部IO操作耗时

**推导公式：**

> 最优线程数 = C * (T1 + T2) /T1 = C + C * (T2 / T1)

根据上面推到出来的式子，我们很容易就可以理解为什么单节点理论最优线程数是上面的计算模型。

假设CPU内核数为1，CPU内部逻辑计算耗时为1ms，外部IO操作耗时为10ms，那么通过上面的计算模型可以得到理论最优线程数是11。

在一瞬间有很多个请求进来，单核CPU会先分配一个线程执行代码。代码逻辑计算耗时较短，CPU就会空闲下来，但是外部IO耗时较长，此时这个线程就会阻塞。这样操作系统可以切换到其他线程处理请求。这样，当我们依次切换了10个线程来利用空闲CPU执行代码的逻辑计算。那么理论耗时就是10ms，而在这个时间段最先分配到任务的线程的外部IO操作可以完成，就会存在空闲的线程。

当然，上面的计算模型计算出来的只是理论值。真实的最优线程数应该通过上面的计算模型算出理论值后，在这个值左右进行代码压测，从而找出最优值。



#### 如何避免消息堆积和消息延迟？

要避免消息堆积和消息延迟的问题，就要在前期设计阶段对整个业务逻辑进行评估。其中最重要的就是评估消息的业务耗时，以及消息消费的并发度。



##### 评估消息的业务耗时

通过压测得到消息的业务耗时，对业务耗时较长的代码进行分析：

- 消息消费逻辑的计算逻辑是否可以优化，是否可以减少必要的递归和循环
- 消息消费逻辑中的IO操作是否一定需要
- 消息消费逻辑中的复杂逻辑是否可以异步处理



##### 设置消息的消费并发度

- 通过压测获得单节点最优线程数
- 根据对整体流程峰值计算出需要的节点数



### 消息的清理

在RocketMQ中，消息被消费过后并不会立即删除。

消息是存储在commitlog文件中，而且消息是不定长的，所以不可能以消息为单位进行删除。在RocketMQ中，消息的删除是以commitlog文件为单位进行删除的。

commitlog文件存在过期时间，默认为3天，超过这个时间，commitlog就会过期。除了用户手动进行清理外，在以下情况下会进行自动清理，不管消息是否被消费：

- 文件过期，且到达清理时间点(默认为凌晨4点)，自动清理过期文件
- 文件过期，且磁盘空间占用率超过过期清理警戒线(默认为75%)，就会立即删除过期文件
- 磁盘空间占用率达到清理警戒线(默认为85%)，就会按预先指定的规则删除commitlog文件(无论是否过期)，默认先删除最早的文件。
- 磁盘空间占用率达到系统危险警戒线(默认为90%)，Broker就会拒绝消息写入



## RocketMQ应用

### 普通消息

Producer对于消息的方式有不同的方式。

**同步发送：**

同步发送消息是指，Producer在发送消息之后，要接收到Broker的ACK之后才发下一条消息。该方式的消息可靠性最高，但是消息发送效率最低。

**异步发送：**

异步发送消息是指，Producer在发送消息之后，不必等到接收Broker的ACK就可以发送下一条消息。该方式的消息可靠性可以得到保证，消息发送效率也可以。

**单向发送：**

单向发送是指，Producer仅发送消息，不用等待处理Broker的ACK。Broker在接收到消息之后也不会发送ACK。该方式的消息可靠性较差，消息发送效率最高。



#### 消息发送代码

##### 异步发送普通消息

```java
/**
 * 普通消息，异步生产者
 */
public class AsyncProducer {

    public static void main(String[] args) throws Exception {
        DefaultMQProducer defaultMQProducer = new DefaultMQProducer("g2");
        defaultMQProducer.setNamesrvAddr("NAMESRVIP:9876");
        defaultMQProducer.setRetryTimesWhenSendAsyncFailed(0);
        defaultMQProducer.start();
        // 发送消息
        for (int i = 0; i < 500; i++) {
            String str = "Hi," + i;
            Message message = new Message("someTopic", "someTag", str.getBytes());
            defaultMQProducer.send(message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    System.out.println(sendResult);
                }

                @Override
                public void onException(Throwable throwable) {

                }
            });
        }

        Thread.sleep(3000);
        defaultMQProducer.shutdown();
    }
}
```



##### 同步发送普通消息

```java
/**
 * 普通消息：同步生产者
 */
public class SyncProducer {

    public static void main(String[] args) throws Exception {
        DefaultMQProducer defaultMQProducer = new DefaultMQProducer("g1");
        defaultMQProducer.setNamesrvAddr("NAMESRVIP:9876");
        defaultMQProducer.setRetryTimesWhenSendFailed(3);
        defaultMQProducer.setSendMsgTimeout(5000);
        defaultMQProducer.start();
        // 发送消息
        for (int i = 0; i < 500; i++) {
            String str = "Hi," + i;
            Message message = new Message("someTopic", "someTag", str.getBytes());
            SendResult sendResult = defaultMQProducer.send(message);
            System.out.println(sendResult);
        }
        // 关闭生产者
        defaultMQProducer.shutdown();
    }
}
```



##### 单向发送普通消息

```java
/**
 * 普通消息：单向消息，不需要确认
 */
public class OneWayProducer {

    public static void main(String[] args) throws Exception {
        DefaultMQProducer defaultMQProducer = new DefaultMQProducer("g3");
        defaultMQProducer.setNamesrvAddr("NAMESRVIP:9876");
        defaultMQProducer.start();
        // 发送消息
        for (int i = 0; i < 500; i++) {
            String str = "Hi," + i;
            Message message = new Message("oneway", "onetag", str.getBytes());
            defaultMQProducer.sendOneway(message);
        }
        defaultMQProducer.shutdown();
        System.out.println("producer shutdown");
    }
}
```



##### 消费消息

```java
public class SomeConsumer {

    public static void main(String[] args) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("cg");
        consumer.setNamesrvAddr("NAMESRVIP:9876");
        // 指定从第一条消息开始消费
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        // 指定topic和tag
        consumer.subscribe("someTopic", "*");
        // 设置广播消费
        consumer.setMessageModel(MessageModel.BROADCASTING);
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for (MessageExt messageExt : list) {
                    System.out.println(messageExt);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
        System.out.println("消费者启动成功");
    }
}
```



### 顺序消息

**什么是顺序消息？**

顺序消息就是严格按照消息的发送顺序进行消息。

**为什么需要顺序消息？**

以订单为例，订单会有多个状态(提交订单、支付成功、发货中、发货成功和发货失败)。

假设在消息队列中同时存在提交订单和支付成功的消息。我们需要先完成对提交订单信息的处理，再完成对支付成功信息的处理，不然逻辑就会出现问题。

**顺序消息分为全局有序和分区有序**

全局有序：Topic中只有一个队列，那么整个Topic中的消息都是按顺序消费的。

分区有序：Topic中有多个队列，但是相同key的消息都发送到同一个key。这样不同队列的消息消费是无序的，但是同一个队列中的消息消费是有序的。

> 消息在发送时，如何实现Queue的选择？
>
> 在定义Producer的时候，我们可以定义消息队列选择器，这个消息队列选择器是我们通过实现了MessageQueueSelector接口定义的。



#### 生产顺序消息的代码

```java
/**
 * 顺序消息生产者
 */
public class OrderedProducer {

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("pg");
        producer.setNamesrvAddr("NAMESRVIP:9876");
        producer.start();
        // 生产消息
        for (int i = 0; i < 100; i++) {
            byte[] msg = ("Hi，" + i).getBytes();
            Message message = new Message("orderTopic", null, msg);
            SendResult sendResult = producer.send(message, new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> list, Message message, Object o) {
                    int id = (int) o;
                    // 获取消息分发的队列
                    int index = id % list.size();
                    return list.get(index);
                }
            }, i);
            System.out.println(sendResult);
        }
        producer.shutdown();
        System.out.println("生产者生产顺序消息完毕");
    }
}
```



### 延迟消息

**什么是延迟消息？**

当消息被写入到Broker之前，在指定的时长之后才可被消费处理的消息。

利用RocketMQ的延迟消息可以实现**定时任务**的功能，而无需使用延时器。

**延迟等级**

延时消息的延迟时长不支持随意时长，需要通过延迟等级来控制。

如果需要自定义延迟等级，我们可以在Broker加载的配置增加配置。

**为什么RocketMQ不支持延迟时长的设置？**

这是为了避免消息排序的导致大量性能的消耗。

假设RocketMQ支持随意延迟时长的设置，那么当Broker每接受到一个延迟消息，都需要对所有的延迟消息进行排序，会消耗大量的系统资源。

而采用延迟等级的概念，那么不同延迟等级的消息必然是存放在不同的queueId下的consumequeue中。那么，RocketMQ就可以保证，后面进来的延迟消息一定是最后的，那么直接将延迟消息写入到对应consumequeue文件的最后就可以了，那么就可以节省排序操作。



#### 延迟消息实现原理

Broker在将接收到的消息写入到commitlog文件中，需要先判断消息是否是延迟消息。如果是延迟消息，那么将延迟消息的Topic修改为`SCHEDULE_TOPIC_XXXX`，queueId修改为`delayLevel - 1`，然后再将原来的消息的topic和queueId写入到消息的**properties**中，以便消息到期时，将消息投递到对应的主题和队列中。

消息在修改完之后会写入到commitlog文件中，然后再写入到consumequeue文件中。在写入的时候，如果发现消息是延迟等级，那么消息索引单元原来存放消息tag的hashcode会改为存放消息的再次投递时间(即消息的投递时间+延迟等级对应时间)。

在Broker中存在一个延迟消息服务，该服务会根据延迟队列的消费偏移从队列中获取消息。其执行逻辑如下：

1. 根据延迟队列的消费偏移从队列中获取消息对应的消息索引单元
2. 拿消息索引单元中的再次投递时间和当前时间比较，若当前时间大于再次投递时间，则进行消息的投递(这就是普通消息的投递，可以被消费者消费)
3. 继续调度下一条消息

> Broker默认每隔10s就会将延迟队列的消费偏移持久化



#### 延迟消息生产代码

消息设置一下延迟等级就是生产延迟消息了

```java
/**
 * 延迟消息生产者
 */
public class DelayProducer {

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("DelayProducer");
        producer.setNamesrvAddr("NAMESRVIP:9876");
        producer.start();
        for (int i = 0; i < 100; i++) {
            byte[] bytes = ("Hi，" + i).getBytes();
            Message message = new Message("delayTopic", "delayTag", bytes);
            // 设置延迟等级
            message.setDelayTimeLevel(3);
            producer.send(message);
        }
        producer.shutdown();
        System.out.println("延迟消息生产完成");
    }
}
```



### 事务消息

事务消息是RocketMQ提供的功能，可以保证事务操作和消息发送一起成功，要么一起失败。

**为什么需要事务消息？**

不知道没关系，用一个具体场景举例：

![img](img/1419561-20200330075824993-1468007004.jpg)

在一个支付流程里面，支付成功之后，系统需要完成更新支付订单、向RocketMQ发送支付成功的消息。手续费系统会从消息队列中拉取消息，然后计算手续费并保存到数据库中。

计算手续费的操作可以异步执行，所以这里采用MQ完成支付和计算手续费的解耦。

上面的支付流程分为了更新支付订单和发送支付成功消息两个操作。在实际业务中，可能会出现失败的情况。

无论上面两个操作哪个在前，哪个在后，都可能出现一个成功，一个失败的情况，这样就会导致系统数据的不一致行。

可能有的人会说，消息发送失败可以重试啊！的确，我们是可以通过重试来保证消息发送成功，但是多次的重试会降低系统的响应速度，影响系统的数据吞吐量，所以不建议这么处理。

**使用事务消息解决数据不一致问题：**

我们可以通过事务消息解决这个问题：

使用事务消息流程如下：

![img](img/1419561-20200330075825968-521099781.jpg)

我们会先发送一个半(half)消息给MQ，通知MQ开启一个事务。注意，这里的半消息并不是指信息不完整，而是指这个在事务提交之前，对消费者是不可见的，消费不会消费这条消息。

发送半消息成功后，我们可以执行更新支付订单操作。

如果更新成功，那么就向MQ发送确认消息。如果更新失败，那么就向MQ发送回滚消息。这样就解决了数据不一致的问题。

**事务回查机制：**

实际上，上面的流程还是存在问题。如果，我们发送的确认或回滚消息因网络问题丢失怎么办？

因此，RocketMQ提供了事务回查机制，我们需要注册一个回调接口，支持RocketMQ回查事务执行的状态。

如果，RocketMQ没有收到确认消息或回滚消息，那么会执行回调接口，根据获得结果来决定是确认消息还是回滚消息。

RocketMQ事务消息完整流程如下：

![img](img/1419561-20200330075826122-1719090786.jpg)



#### 代码

```java
package com.xgc.tx;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.exception.RemotingException;

/**
 * 事务生产者
 */
public class TXProducer {


    public static void main(String[] args) throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        TransactionMQProducer producer = new TransactionMQProducer("txProducer");
        producer.setNamesrvAddr("NAMESRVIP:9876");
        producer.setTransactionListener(new TransactionListener() {
            @Override
            public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                if (msg.getTags().equals("TAGA")) {
                    return LocalTransactionState.COMMIT_MESSAGE;
                } else if (msg.getTags().equals("TAGB")) {
                    return LocalTransactionState.UNKNOW;
                } else if (msg.getTags().equals("TAGC")) {
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }
                return LocalTransactionState.ROLLBACK_MESSAGE;
            }

            @Override
            public LocalTransactionState checkLocalTransaction(MessageExt msg) {
                return LocalTransactionState.COMMIT_MESSAGE;
            }
        });
        producer.start();

        String[] tags = {"TAGA", "TAGB", "TAGC"};
        for (int i = 0; i < tags.length; i++) {
            Message message = new Message("somTopic", tags[i],  ("hello," + i).getBytes());
            producer.sendMessageInTransaction(message, null);
        }
        System.out.println("消息发送完毕");
    }
}
```

上面的代码发送的事务消息有三种TAG。TAGA消息则提交成功，TAGB消息需要回查，TAGC消息则回滚。

那么消费者会先获取到TAGA的消息，经过一段时间才会获得TAGB消息，而TAGC消息则不会被消费。



### 批量消息

RocketMQ支持生产者批量发送消息、消费者批量消费消息，以提高消息吞吐率。

**生产者批量发送消息：**不过需要注意一下几点：

1. 批量发送的消息要有相同topic
2. 批量发送的消息要有相同的刷盘策略
3. 批量发送的消息不能是延迟消息和事务消息

**批量发送的消息有大小限制，默认不能超过4MB**，如果超过可以：

1. 将消息进行拆分成不超出4MB
2. producer和broker修改单次批量发送消息最大值

**发送者发送消息大小：**

生产者通过send()发送Message，不是将Message序列化后发送到网络，而是生成一个字符串发送出去。这个字符串组成结构：topic、body、消息日志log(占20字节)和消息属性properties。

![image-20220218084707598](img/image-20220218084707598.png)

**消费者批量消费消息：**

消费者默认可以消费1条消息，可以通过consumeMessageBatchMaxSize属性来修改。不过这个值不能超过32，因为默认消费者一次拉取的消息最大值32条，通过pullBatchSize属性可以指定。

**是不是拉取和消费的消息越多越好？**

不是。分析如下：

1. pullBatchSize越大，那么consumer拉取一次消息的时间会变长，那么网络传输中出现问题的可能性变大。当拉取过程出现问题，这批数据需要重新拉取
2. consumeMessageBatchMaxSize越大，消息的并发消费能力越低。因为这批消息必须是相同的消费逻辑，具有相同的消费结果，而且这些数据是通过单线程来执行的。另外，在消息处理过程中只要有一条消息出现问题，那么这批消息需要全部重新进行处理。



#### 批量推送消息代码

生产者生产代码：

```java
public class BatchProducer {

    public static void main(String[] args) throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("batchPG");
        producer.setNamesrvAddr("NAMESRVIP:9876");
        // 默认生产者能够批量发送的消息大小不超过4MB，可以设置
		// producer.setMaxMessageSize(1024 * 1024 * 8);
        producer.start();

        List<Message> msgList = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            Message msg = new Message("batchTopic", null, ("batch" + i).getBytes());
            msgList.add(msg);
        }
        // 消息分割器，避免消息列表消息大小出现限制
        MessageSplitter splitter = new MessageSplitter(msgList);
        if (splitter.hasNext()) {
            List<Message> messageList = splitter.next();
            producer.send(messageList);
        }
        System.out.println("批量消息发送完成");
    }
}
```

消息分割器代码：

```java
/**
 * 将消息列表进行切割，每个列表消息大小不超过4MB
 */
public class MessageSplitter implements Iterator<List<Message>> {

    private final int SIZE_LIMIT = 1024 * 1024 * 4;
    private final List<Message> messageList;
    private int currIdx;

    public MessageSplitter(List<Message> messageList) {
        this.messageList = messageList;
    }

    @Override
    public boolean hasNext() {
        return currIdx < messageList.size();
    }

    @Override
    public List<Message> next() {
        int nextIndex = currIdx;
        int totalSize = 0;
        for (; nextIndex < messageList.size(); nextIndex++) {
            Message message = messageList.get(nextIndex);
            int tempSize = message.getTopic().length() + message.getBody().length;
            Map<String, String> properties = message.getProperties();
            for (Map.Entry<String, String> entry : properties.entrySet()) {
                tempSize = tempSize + entry.getKey().length() + entry.getValue().length();
            }
            tempSize += 20;
            if (tempSize > SIZE_LIMIT) {
                if (nextIndex - currIdx == 0) {
                    nextIndex++;
                }
                break;
            }
            if (tempSize + totalSize > SIZE_LIMIT) {
                break;
            } else {
                totalSize += tempSize;
            }
        }
        List<Message> subList = messageList.subList(currIdx, nextIndex);
        currIdx = nextIndex;
        return subList;
    }

}
```



#### 批量消费消息代码

```java
public class BatchConsumer {

    public static void main(String[] args) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("batchCG");
        consumer.setNamesrvAddr("NAMESRVIP:9876");
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        consumer.subscribe("batchTopic", "*");
        // 设置每次可以消费的消息数量
        consumer.setConsumeMessageBatchMaxSize(10);
        // 每次拉取的消息数量，默认为32
        // consumer.setPullBatchSize(50);
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                System.out.println("size:" + list.size());
                for (MessageExt messageExt : list) {
                    System.out.println(messageExt);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
        System.out.println("消费者启动成功");
    }
}
```



### 消息过滤

消费者在进行消息订阅的时候，可以指定需要订阅的消息的topic，也可以对Topic中的消息根据指定消息过滤，完成更低细粒度的消息获取。

消息过滤分为两种：tag过滤和SQL过滤。

**Tag过滤：**通过消费者的`subscribe`方法指定要订阅消息的tag。如果要指定多个tag，那么tag之间使用或运算符(||)连接。

**SQL过滤：**根据消息的用户属性来完成消息过滤。但只有PULL模式的消费者能使用。默认，Broker没有开启SQL过滤的功能，需要在broker的配置文件添加属性`enablePropertyFilter`开启功能。



#### TAG过滤代码

生产者代码：

```java
public class FilterByTagProducer {

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("filterTagProducer");
        producer.setNamesrvAddr("NAMESRVIP:9876");
        producer.start();

        String[] tags = {"TAGA", "TAGB", "TAGC"};
        for (int i = 0; i < tags.length; i++) {
            Message msg = new Message("filterTopic", tags[i], "hello".getBytes());
            producer.send(msg);
        }
        System.out.println("生产者生产消息完成");
    }
}
```

消费者代码：

```java
public class FilterByTagConsumer {

    public static void main(String[] args) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("filterTagConsumer");
        consumer.setNamesrvAddr("NAMESRVIP:9876");
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        consumer.subscribe("filterTopic", "TAGA || TAGB");
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println(msg);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
    }
}
```



#### SQL过滤代码

生产者代码：

```java
public class FilterBySQLProducer {

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("filterSQLPg");
        producer.setNamesrvAddr("NAMESRVIP:9876");
        producer.start();

        for (int i = 0; i < 10; i++) {
            Message msg = new Message("filterTopic", ("hello," + i).getBytes());
            msg.putUserProperty("age", "" + i);
            producer.send(msg);
        }
    }
}
```

消费者代码：

```java
public class FilterBySQLConsumer {

    public static void main(String[] args) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer();
        consumer.setNamespace("NAMESRVIP:9876");
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        consumer.subscribe("filterTopic", MessageSelector.bySql("age between 2 and 6"));
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println(msg);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
        System.out.println("消费者启动完成");
    }
}
```



### 消息发送重试机制

Producer对发送失败的消息重新投递的机制，就叫做消息发送重试机制。

对于消息发送重试需要注意的是：

1. 只有普通消息有发送重试机制，顺序消息是没有的
2. 消息发送重试机制可能导致消息重复，需要消费者保证消费的幂等性。
3. 消息发送重试机制有三种：同步发送失败策略、异步发送失败策略、刷盘失败策略



#### 同步发送失败策略

对于普通消息，生产者默认采用轮询机制选择需要投递的队列。当投递失败的时候，默认会投递2次，重试的时候是不会选择上次失败的Broker。当超出重试次数的时候会抛出异常。

同时Broker有失败隔离功能，是Producer尽量选择没有投递失败的Broker作为投递的Broker。



#### 异步发送失败策略

对于异步发送失败，重试的时候是不会选择其他Broker。所以，异步发送无法保证消息不丢失。



#### 刷盘失败策略

对于刷盘失败或slave不可用，默认是不会将消息尝试发送到其他Broker。不过，对于重要消息可以在Broker的配置文件设置`retryAnotherBrokerWhenNoStoreOK`属性为true来开启。



### 消息消费重试机制

#### 顺序消息消费重试机制

对于顺序消息，为了保证消息的顺序性，会一直不断阻塞重试。所以对于顺序消息，最好要监控消息队列的情况。



#### 无序消息消费重试机制

对于无序消息(普通消息，延迟消息，事务消息)，消费者消费失败的时候可以返回消费失败状态来完成重试。不过只有在集群模式下才有消费重试机制，广播消费没有消费重试机制。

默认每条消息最多会重试16次，但是每次重试的时间间隔是不同的，会随着重试次数的增加而变长。

**消费重试队列：**

对于消费失败的消息，Broker会为每个消费者组创建一个特殊的队列来存放这些消费失败的消息。

这个重试队列是属于主题名为%RETRY%consumerGroup@consumerGroup下的队列。

Broker对重试消息的处理是通过延迟机制处理的。先将重试的队列投递到延迟队列，时间到了再投递到重试队列中。

**死信队列：**

对于重试次数达到最大值，而且还没有消费成功的消息，会投递到死信队列(Dead-eitter Queue, DLQ)，里面的消息称为死信消息(Dead-Letter Message，DLM)。

**死信队列的特性：**

1. 死信队列的消息不会被消费
2. 死信队列的消息存在有效期，默认3天删除
3. 死信队列归属Topic名为%DLQ%consumerGroup@consumerGroup，同样是为每个消费组创建的。

**为什么需要死信队列？**

当一条消息进入了死信队列，就代表系统某处出现了问题，比如代码出现了bug。

这时候就需要开发人员进行排查，找出系统中存在的问题，并将死信队列中的消息进行处理。
