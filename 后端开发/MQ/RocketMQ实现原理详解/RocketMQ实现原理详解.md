> 本文源码版本：
>
> ```xml
> <groupId>org.apache.rocketmq</groupId>
> <artifactId>rocketmq-all</artifactId>
> <version>4.9.3-SNAPSHOT</version>
> ```



# RocketMQ的路由中心：NameServer

NameServer是RocketMQ的路由中心，存储了Topic的路由信息。

借助NameServer，生产者可以获取到消息可以发送到哪个Broker，消费者可以获取到需要消费的消息在哪个Broker，从而完成消息的生产和消费。

下面会根据两个方面来介绍NameServer：

1. NameServer的架构
2. NameServer的动态路由发现和剔除



## NameServer的架构

### 为什么需要NameServer？

**消息中间件的设计思路是基于发布/订阅模型。**消息生产者发送某一主题的消息到消息服务器，消息服务器进行该消息的持久化存储，消息消费者订阅感兴趣的主题，消息服务器根据**订阅信息(路由信息)**将消息推送给消息消费者(PUSH模型)或消息消费者主动从消息服务器拉取消息(PULL模型)，从而完成消费者与生产者的解耦。

为了避免消息服务器单点故障导致系统不可用，我们往往会部署多台消息服务器来承担消息的存储。这样就会产生一些问题：生产者怎么知道要将消息发送到哪一台消息服务器？当其中一台消息服务器宕机时，生产者如何在不重启的情况下感知到？

NameServer就是为了解决上面的问题设计的。



### RocketMQ物理部署图

RocketMQ的物理部署图如下：

![RocketMQ物理部署图](img/epub_23768928_38.jpeg)





### NameServer的运行机制(概要)

Broker消息服务器在启动的时候会向所有NameServer注册。消息生产者(Producer)在发送消息前会从NameServer拉取消息服务器地址列表，然后根据负载算法从列表中选择一台消息服务器进行消息发送。

NameServer会与每个Broker消息服务器保持长连接，每隔30s检查Broker是否存活，如果Broker宕机则从路由注册表中删除该消息服务器。但是路由变化并不会马上通知消息生产者，这是为了降低NameServer设计的复杂度，在消息发送端提供容错机制来保证消息发送的高可用性。

NameServer本身的高可用性是通过部署多台NameServer来实现，但是NameServer之间并不会进行通信，也就是说在同一时刻NameServer服务器的数据并不会完全相同，但是这并不会对消息发送产生影响。



## NameServer启动过程

本节将从源码角度来窥探NameServer启动过程：

### Step1：解析配置文件，构建NamesrvConfig和NettyServerConfig对象

```java
final NamesrvConfig namesrvConfig = new NamesrvConfig();
final NettyServerConfig nettyServerConfig = new NettyServerConfig();
nettyServerConfig.setListenPort(9876);
if (commandLine.hasOption('c')) {
    String file = commandLine.getOptionValue('c');
    if (file != null) {
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        properties = new Properties();
        properties.load(in);
        MixAll.properties2Object(properties, namesrvConfig);
        MixAll.properties2Object(properties, nettyServerConfig);

        namesrvConfig.setConfigStorePath(file);

        System.out.printf("load config properties file OK, %s%n", file);
        in.close();
    }
}

if (commandLine.hasOption('p')) {
    InternalLogger console = InternalLoggerFactory.getLogger(LoggerName.NAMESRV_CONSOLE_NAME);
    MixAll.printObjectProperties(console, namesrvConfig);
    MixAll.printObjectProperties(console, nettyServerConfig);
    System.exit(0);
}

MixAll.properties2Object(ServerUtil.commandLine2Properties(commandLine), namesrvConfig);
```

从上面代码可以看到，Namesrv启动会先创建NamesrvConfig和NettyServerConfig对象，然后将加载的参数填充到这两个对象中。参数的来源有两种：

1. 通过`-c configPath`指定的配置文件的文件
2. 通过`--configKey configValue`指定的命令行参数，例如`--listenPort 9876`

---

`namesrvConfig`配置对象属性：

```java
private String rocketmqHome = System.getProperty(MixAll.ROCKETMQ_HOME_PROPERTY, System.getenv(MixAll.ROCKETMQ_HOME_ENV));
private String kvConfigPath = System.getProperty("user.home") + File.separator + "namesrv" + File.separator + "kvConfig.json";
private String configStorePath = System.getProperty("user.home") + File.separator + "namesrv" + File.separator + "namesrv.properties";
private String productEnvName = "center";
private boolean clusterTest = false;
private boolean orderMessageEnable = false;
```

- rocketmqHome：RocketMQ主目录，可以通过`-Drocketmq.home.dir=path`或者环境变量`ROCKETMQ_HOME`来设置
- kvConfigPath：NameServer存储KV配置属性的持久化路径
- configStorePath：nameServer默认配置文件路径，默认不生效。通过`-c`选项来指定
- orderMessageEnable：是否支持顺序消息，默认不支持

---

`nettyServerConfig`配置对象属性：

```java
private int listenPort = 8888;
private int serverWorkerThreads = 8;
private int serverCallbackExecutorThreads = 0;
private int serverSelectorThreads = 3;
private int serverOnewaySemaphoreValue = 256;
private int serverAsyncSemaphoreValue = 64;
private int serverChannelMaxIdleTimeSeconds = 120;
private int serverSocketSndBufSize = NettySystemConfig.socketSndbufSize;
private int serverSocketRcvBufSize = NettySystemConfig.socketRcvbufSize;
private boolean serverPooledByteBufAllocatorEnable = true;
private boolean useEpollNativeSelector = false;
```

- listenPort：namesrv监听端口，默认为9876
- serverWorkerThreads：Netty业务线程池线程个数
- serverCallbackExecutorThreads：Netty公共任务线程池线程个数。Netty网络设计会根据业务类型创建不同的线程池，比如消息发送、消息消费、心跳检测等。如果该业务类型(RequestCode)没有注册线程池，那么提交到公共线程池执行。

- serverSelectorThreads：IO线程池线程个数。NameServer和Broker处理请求、返回响应的线程个数。
- serverOnewaySemaphoreValue：发送单向消息最大并发度(Broker端参数)
- serverAsyncSemaphoreValue：异步消息发送最大并发度(Broker端参数)
- serverChannelMaxIdleTimeSeconds：网络连接最大空闲时间，默认120s。如果超出时间，则关闭连接。
- serverSocketSndBufSize：网络发送缓存区大小，默认64k
- serverSocketRcvBufSize：网络接收缓存区大小，默认64k
- serverPooledByteBufAllocatorEnable：是否开启缓存，建议开启
- useEpollNativeSelector：是否采用Epoll IO模型，Linux环境下建议开启

> 小技巧：在启动NameServer时，可以先使用./mqnamesrv -c configPath -p 打印当前加载的配置属性



### Step2：解析配置文件，创建NamesrvController对象并初始化

```java
this.kvConfigManager.load();

this.remotingServer = new NettyRemotingServer(this.nettyServerConfig, this.brokerHousekeepingService);

this.remotingExecutor =
    Executors.newFixedThreadPool(nettyServerConfig.getServerWorkerThreads(), new ThreadFactoryImpl("RemotingExecutorThread_"));

this.registerProcessor();

this.scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
    @Override
    public void run() {
        NamesrvController.this.routeInfoManager.scanNotActiveBroker();
    }
}, 5, 10, TimeUnit.SECONDS);

this.scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
    @Override
    public void run() {
        NamesrvController.this.kvConfigManager.printAllPeriodically();
    }
}, 1, 10, TimeUnit.MINUTES);
```

加载KV配置，创建NettyServer网络处理对象，并开启两个定时任务。在RocketMQ中，这类定时任务统称为心跳检测，这两个定时任务执行逻辑为：

1. 每隔10s检查是否存在没有存活的Broker，存在则将其从路由表中移除
2. 每隔10分钟打印KV配置



### Step3：注册JVM钩子函数，并启动Namesrv服务

```java
Runtime.getRuntime().addShutdownHook(new ShutdownHookThread(log, new Callable<Void>() {
    @Override
    public Void call() throws Exception {
        controller.shutdown();
        return null;
    }
}));

controller.start();
```

这里展示了一种编程技巧，如果我们在代码中有创建线程池，一种优雅停机的方式就是注册一个JVM钩子函数，这样可以在JVM进程关闭之前先关闭线程池，及时释放资源。



## NameServer路由注册、故障剔除

NameServer的主要功能是为生产者和消费者提供Topic的路由信息，为此NameServer需要存储路由的基础信息，还要管理Broker节点，包括路由注册、路由剔除等功能。



### 路由元信息

```java
private final HashMap<String/* topic */, List<QueueData>> topicQueueTable;
private final HashMap<String/* brokerName */, BrokerData> brokerAddrTable;
private final HashMap<String/* clusterName */, Set<String/* brokerName */>> clusterAddrTable;
private final HashMap<String/* brokerAddr */, BrokerLiveInfo> brokerLiveTable;
private final HashMap<String/* brokerAddr */, List<String>/* Filter Server */> filterServerTable;
```

- topicQueueTable：Topic消息队列路由信息，消息发送的时候根据路由表进行负载均衡
- brokerAddrTable：Broker基础信息，包括Broker归属集群，brokerName以及主备Broker地址
- clusterAddrTable：Broker集群信息，存储集群中所有的brokerName
- brokerLiveTable：Broker状态信息，NameServer接收到心跳包的时候就会更新该信息
- filterServerTable：Broker上的FilterServer列表，用于类模式消息过滤

---

QueueData属性

```java
private String brokerName; 
private int readQueueNums; // 读队列数量
private int writeQueueNums; // 写队列数量
private int perm; // 读写权限
private int topicSysFlag; // topic同步标记
```

---

BrokerData属性

```java
private String cluster; // Broker归属集群名字
private String brokerName;
private HashMap<Long/* brokerId */, String/* broker address */> brokerAddrs;
```

---

BrokerLiveInfo属性

```java
private long lastUpdateTimestamp; // 最后一次更新的时间
private DataVersion dataVersion;
private Channel channel;
private String haServerAddr;
```



### 路由注册

RocketMQ路由注册主要是通过Broker和NameServer的心跳检测功能来实现的。Broker在启动的时候会向集群的所有NameServer发送心跳包，每隔30s发送一次。NameServer在接收到心跳包后，会更新brokerLiveTable缓存的BrokerLiveInfo里面的lastUpdateTimeStamp属性。

NameServer每隔10s就会检查brokerLiveTable，如果发现Broker超过120s没有发送心跳包，那么就将该Broker从路由表中移除并关闭Socket连接。

#### 1.Broker发送心跳包

`Broker端心跳包发送(BrokerController#start)`

```java
this.scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
    @Override
    public void run() {
        try {
            BrokerController.this.registerBrokerAll(true, false, brokerConfig.isForceRegister());
        } catch (Throwable e) {
            log.error("registerBrokerAll Exception", e);
        }
    }
}, 1000 * 10, Math.max(10000, Math.min(brokerConfig.getRegisterNameServerPeriod(), 60000)), TimeUnit.MILLISECONDS);
```

---

`BrokerOuterAPI#registerBrokerAll`

```java
List<String> nameServerAddressList = this.remotingClient.getNameServerAddressList();
if (nameServerAddressList != null && nameServerAddressList.size() > 0) {

    final RegisterBrokerRequestHeader requestHeader = new RegisterBrokerRequestHeader();
    requestHeader.setBrokerAddr(brokerAddr);
    requestHeader.setBrokerId(brokerId);
    requestHeader.setBrokerName(brokerName);
    requestHeader.setClusterName(clusterName);
    requestHeader.setHaServerAddr(haServerAddr);
    requestHeader.setCompressed(compressed);

    RegisterBrokerBody requestBody = new RegisterBrokerBody();
    requestBody.setTopicConfigSerializeWrapper(topicConfigWrapper);
    requestBody.setFilterServerList(filterServerList);
    final byte[] body = requestBody.encode(compressed);
    final int bodyCrc32 = UtilAll.crc32(body);
    requestHeader.setBodyCrc32(bodyCrc32);
    final CountDownLatch countDownLatch = new CountDownLatch(nameServerAddressList.size());
    for (final String namesrvAddr : nameServerAddressList) {
        brokerOuterExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    RegisterBrokerResult result = registerBroker(namesrvAddr, oneway, timeoutMills, requestHeader, body);
                    if (result != null) {
                        registerBrokerResultList.add(result);
                    }

                    log.info("register broker[{}]to name server {} OK", brokerId, namesrvAddr);
                } catch (Exception e) {
                    log.warn("registerBroker Exception, {}", namesrvAddr, e);
                } finally {
                    countDownLatch.countDown();
                }
            }
        });
    }

    try {
        countDownLatch.await(timeoutMills, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
    }
}
```

该方法就是封装请求包头。遍历NameServer地址列表，然后Broker依次向NameServer发送心跳包。

封装请求包头：

- brokerAddr：broker地址
- brokerId：0表示Master，大于0表示Slave
- brokerName：broker名称
- clusterName：集群名称
- hasServerAddr：master地址，初始请求时该值为空，slave向Nameserver注册后返回
- requestBody：
  - filterServerList：消息过滤服务器列表
  - topicConfigWrapper：主题配置，topicConfigWrapper封装的是topicConfigManager里面的topicConfigTable，存储的是Broker启动时默认的一些topic。

---

`BrokerOuterAPI#registerBroker(网络发送代码)`

```java
RemotingCommand request = RemotingCommand.createRequestCommand(RequestCode.REGISTER_BROKER, requestHeader);
request.setBody(body);

if (oneway) {
    try {
        this.remotingClient.invokeOneway(namesrvAddr, request, timeoutMills);
    } catch (RemotingTooMuchRequestException e) {
        // Ignore
    }
    return null;
}

RemotingCommand response = this.remotingClient.invokeSync(namesrvAddr, request, timeoutMills);
```



#### 2.NameServer处理心跳包

NameServer处理心跳包的逻辑其实就是更新路由表和Broker存活信息。

`org.apache.rocketmq.namesrv.processor.DefaultRequestProcessor`网络处理器解析请求类型，如果请求类型是`RequestCode.REGISTER_BROKER`，那么将请求分发到`RouteInfoManager#registerBroker`执行。



##### Step1：更新clusterAddrTable

`RouteInfoManager#registerBroker clusterAddrTable维护`

```java
 this.lock.writeLock().lockInterruptibly();

Set<String> brokerNames = this.clusterAddrTable.get(clusterName);
if (null == brokerNames) {
    brokerNames = new HashSet<String>();
    this.clusterAddrTable.put(clusterName, brokerNames);
}
brokerNames.add(brokerName);
```

路由注册需要加锁，防止并发修改RouteInfoManager的路由信息。

首先判断broker归属集群是否存在，不存在则创建，然后将brokerName加入到Broker集群路由信息中。



##### Step2：更新brokerAddrTable

`RouteInfoManager#registerBroker brokerAddrTable维护`

```java
boolean registerFirst = false;

BrokerData brokerData = this.brokerAddrTable.get(brokerName);
if (null == brokerData) {
    registerFirst = true;
    brokerData = new BrokerData(clusterName, brokerName, new HashMap<Long, String>());
    this.brokerAddrTable.put(brokerName, brokerData);
}
Map<Long, String> brokerAddrsMap = brokerData.getBrokerAddrs();
//Switch slave to master: first remove <1, IP:PORT> in namesrv, then add <0, IP:PORT>
//The same IP:PORT must only have one record in brokerAddrTable
Iterator<Entry<Long, String>> it = brokerAddrsMap.entrySet().iterator();
while (it.hasNext()) {
    Entry<Long, String> item = it.next();
    if (null != brokerAddr && brokerAddr.equals(item.getValue()) && brokerId != item.getKey()) {
        it.remove();
    }
}

String oldAddr = brokerData.getBrokerAddrs().put(brokerId, brokerAddr);
registerFirst = registerFirst || (null == oldAddr);
```

首先判断brokerAddrTable是否包含该brokerName的broker基本信息，不存在则创建brokerData，将registerFirst设置为true，表示该broker是第一次注册。

接着判断该broker是否进行主备切换，是则移除掉该broker地址信息。

最后，更新broker的地址信息。



##### Step3：更新topicQueueTable

`RouteInfoManager#registerBroker topicQueueTable维护`

```java
if (null != topicConfigWrapper
    && MixAll.MASTER_ID == brokerId) {
    if (this.isBrokerTopicConfigChanged(brokerAddr, topicConfigWrapper.getDataVersion())
        || registerFirst) {
        ConcurrentMap<String, TopicConfig> tcTable =
            topicConfigWrapper.getTopicConfigTable();
        if (tcTable != null) {
            for (Map.Entry<String, TopicConfig> entry : tcTable.entrySet()) {
                this.createAndUpdateQueueData(brokerName, entry.getValue());
            }
        }
    }
}
```

如果Broker为MASTER，并且Broker Topic配置信息没有修改或者是首次注册，那么创建或更新topic路由信息。

这个过程其实是为默认主题注册路由信息，其中包含MixAll.DEFAULT_TOPIC的路由信息。当消息生产者发送消息的时候，如果该主题没有创建并且BrokerConfig的autoCreateTopicEnable为true时，将返回MixAll.DEFAULT_TOPIC的路由消息。

---

`RouteInfoManager#createAndUpdateQueueData`

```java
private void createAndUpdateQueueData(final String brokerName, final TopicConfig topicConfig) {
    QueueData queueData = new QueueData();
    queueData.setBrokerName(brokerName);
    queueData.setWriteQueueNums(topicConfig.getWriteQueueNums());
    queueData.setReadQueueNums(topicConfig.getReadQueueNums());
    queueData.setPerm(topicConfig.getPerm());
    queueData.setTopicSysFlag(topicConfig.getTopicSysFlag());

    List<QueueData> queueDataList = this.topicQueueTable.get(topicConfig.getTopicName());
    if (null == queueDataList) {
        queueDataList = new LinkedList<QueueData>();
        queueDataList.add(queueData);
        this.topicQueueTable.put(topicConfig.getTopicName(), queueDataList);
        log.info("new topic registered, {} {}", topicConfig.getTopicName(), queueData);
    } else {
        boolean addNewOne = true;

        Iterator<QueueData> it = queueDataList.iterator();
        while (it.hasNext()) {
            QueueData qd = it.next();
            if (qd.getBrokerName().equals(brokerName)) {
                if (qd.equals(queueData)) {
                    addNewOne = false;
                } else {
                    log.info("topic changed, {} OLD: {} NEW: {}", topicConfig.getTopicName(), qd,
                             queueData);
                    it.remove();
                }
            }
        }

        if (addNewOne) {
            queueDataList.add(queueData);
        }
    }
}
```

根据topicConfig构建topicQueueData，然后加入到topicQueueTable



##### Step4：更新brokerLiveTable

`RouteInfoManager#registerBroker brokerLiveTable维护`

```java
BrokerLiveInfo prevBrokerLiveInfo = this.brokerLiveTable.put(brokerAddr,
	new BrokerLiveInfo(
    	System.currentTimeMillis(),
        topicConfigWrapper.getDataVersion(),
        channel,
        haServerAddr));
if (null == prevBrokerLiveInfo) {
    log.info("new broker registered, {} HAServer: {}", brokerAddr, haServerAddr);
}
```

更新brokerLiveInfo(存活Broker消息表)，BrokerLiveInfo是执行路由删除的重要依据。



##### Step5：更新filterServerTable

`RouteInfoManager#registerBroker filterServerTable维护`

```java
if (filterServerList != null) {
    if (filterServerList.isEmpty()) {
        this.filterServerTable.remove(brokerAddr);
    } else {
        this.filterServerTable.put(brokerAddr, filterServerList);
    }
}

if (MixAll.MASTER_ID != brokerId) {
    String masterAddr = brokerData.getBrokerAddrs().get(MixAll.MASTER_ID);
    if (masterAddr != null) {
        BrokerLiveInfo brokerLiveInfo = this.brokerLiveTable.get(masterAddr);
        if (brokerLiveInfo != null) {
            result.setHaServerAddr(brokerLiveInfo.getHaServerAddr());
            result.setMasterAddr(masterAddr);
        }
    }
}
```

注册Broker的过滤器Server地址列表，一个Broker上会关联多个FilterServer消息过滤服务器。如果Broker为从节点，则需要查找该Broker的Master节点信息，并更新对应的masterAddr属性。



##### 设计亮点

NameServer会与Broker保持长连接，每接受一个Broker发来的心跳包，都会更新BrokerLiveInfo以及相关路由信息(topicQueueTable、brokerAddrTable、clusterAddrTable、filterServerTable)。

同时，NameServer使用读写锁。这样，生产者在发送消息的时候可以并发读取路由数据。在同一时刻，可以保证NameServer只会处理一个心跳包。



### 路由删除

触发路由删除的时机有两个：

1. NameServer定时检查brokerLiveTable，检查broker最后一次发送心跳包的时间与当前时间之差是否大于120s，如果大于则删除该Broker路由信息。
2. Broker正常被关闭的时候，会执行unregisterBroker指令。

其实不管是那种方式触发的路由删除，执行路由删除逻辑的都是一样的方法。

本文以第一种触发路由删除的方式进行代码追踪：

`RouteInfoManager#scanNotActiveBroker` 

```java
public void scanNotActiveBroker() {
    Iterator<Entry<String, BrokerLiveInfo>> it = this.brokerLiveTable.entrySet().iterator();
    while (it.hasNext()) {
        Entry<String, BrokerLiveInfo> next = it.next();
        long last = next.getValue().getLastUpdateTimestamp();
        if ((last + BROKER_CHANNEL_EXPIRED_TIME) < System.currentTimeMillis()) {
            RemotingUtil.closeChannel(next.getValue().getChannel());
            it.remove();
            log.warn("The broker channel expired, {} {}ms", next.getKey(), BROKER_CHANNEL_EXPIRED_TIME);
            this.onChannelDestroy(next.getKey(), next.getValue().getChannel());
        }
    }
}
```

遍历brokerLiveTable表，检查BrokerLiveInfo的lastUpdateTimestamp上次收到心跳包的时间与当前系统时间之差是否大于120s。若大于，则删除与Broker相关的路由信息，并断开Scoket连接。



#### Step1：申请写锁，将brokerAddress从brokeLiveTable，filterServerTable移除

`RouteInfoManager#onChannelDestroy`

```java
this.lock.writeLock().lockInterruptibly();
this.brokerLiveTable.remove(brokerAddrFound);
this.filterServerTable.remove(brokerAddrFound);
```



#### Step2：维护brokerAddrTable

`RouteInfoManager#onChannelDestroy`

```java
String brokerNameFound = null;
boolean removeBrokerName = false;
Iterator<Entry<String, BrokerData>> itBrokerAddrTable =
    this.brokerAddrTable.entrySet().iterator();
while (itBrokerAddrTable.hasNext() && (null == brokerNameFound)) {
    BrokerData brokerData = itBrokerAddrTable.next().getValue();

    Iterator<Entry<Long, String>> it = brokerData.getBrokerAddrs().entrySet().iterator();
    while (it.hasNext()) {
        Entry<Long, String> entry = it.next();
        Long brokerId = entry.getKey();
        String brokerAddr = entry.getValue();
        if (brokerAddr.equals(brokerAddrFound)) {
            brokerNameFound = brokerData.getBrokerName();
            it.remove();
            log.info("remove brokerAddr[{}, {}] from brokerAddrTable, because channel destroyed",
                     brokerId, brokerAddr);
            break;
        }
    }

    if (brokerData.getBrokerAddrs().isEmpty()) {
        removeBrokerName = true;
        itBrokerAddrTable.remove();
        log.info("remove brokerName[{}] from brokerAddrTable, because channel destroyed",
                 brokerData.getBrokerName());
    }
}
```

遍历brokerAddrTable，从BrokerData的brokerAddrs中找到具体的Broker，从BrokerData中移除。如果移除后，BrokerData不包含其他Broker，那么brokerAddrTable移除该brokerName对应的条目。



#### Step3：维护clusterAddrTable

`RouteInfoManager#onChannelDestroy`

```java
if (brokerNameFound != null && removeBrokerName) {
    Iterator<Entry<String, Set<String>>> it = this.clusterAddrTable.entrySet().iterator();
    while (it.hasNext()) {
        Entry<String, Set<String>> entry = it.next();
        String clusterName = entry.getKey();
        Set<String> brokerNames = entry.getValue();
        boolean removed = brokerNames.remove(brokerNameFound);
        if (removed) {
            log.info("remove brokerName[{}], clusterName[{}] from clusterAddrTable, because channel destroyed",
                     brokerNameFound, clusterName);

            if (brokerNames.isEmpty()) {
                log.info("remove the clusterName[{}] from clusterAddrTable, because channel destroyed and no broker in this cluster",
                         clusterName);
                it.remove();
            }

            break;
        }
    }
}
```

遍历clusterAddrTable，brokerName集合移除brokerName。移除后若集合没有没有其他brokerName，则移除该集群。



#### Step4：维护topicQueueTable

`RouteInfoManager#onChannelDestroy`

```java
if (removeBrokerName) {
    Iterator<Entry<String, List<QueueData>>> itTopicQueueTable =
        this.topicQueueTable.entrySet().iterator();
    while (itTopicQueueTable.hasNext()) {
        Entry<String, List<QueueData>> entry = itTopicQueueTable.next();
        String topic = entry.getKey();
        List<QueueData> queueDataList = entry.getValue();

        Iterator<QueueData> itQueueData = queueDataList.iterator();
        while (itQueueData.hasNext()) {
            QueueData queueData = itQueueData.next();
            if (queueData.getBrokerName().equals(brokerNameFound)) {
                itQueueData.remove();
                log.info("remove topic[{} {}], from topicQueueTable, because channel destroyed",
                         topic, queueData);
            }
        }

        if (queueDataList.isEmpty()) {
            itTopicQueueTable.remove();
            log.info("remove topic[{}] all queue, from topicQueueTable, because channel destroyed",
                     topic);
        }
    }
}
```



#### Step5：移除写锁，完成路由删除

`RouteInfoManager#onChannelDestroy`

```java
this.lock.writeLock().unlock();
```



### 路由发现

RocketMQ路由发现是非实时的，需要客户端主动拉取主题的路由信息。

根据主题拉取路由消息的RequestCode是`GET_ROUTEINFO_BY_TOPIC`，NameServer返回的数据实体是`TopicRouteData`。

`TopcicRouteData`的属性如下：

```java
private String orderTopicConf;
private List<QueueData> queueDatas;
private List<BrokerData> brokerDatas;
private HashMap<String/* brokerAddr */, List<String>/* Filter Server */> filterServerTable;
```

- orderTopicConf：顺序消息配置内容，来自于kvConfig
- queueDatas：topic队列元数据
- brokerDatas：topic分布的broker元数据
- filterServerTable：broker上的过滤服务器地址列表

下面分析路由发现代码：

`DefaultRequestProcessor#getRouteInfoByTopic`

```java
public RemotingCommand getRouteInfoByTopic(ChannelHandlerContext ctx,
    RemotingCommand request) throws RemotingCommandException {
    final RemotingCommand response = RemotingCommand.createResponseCommand(null);
    final GetRouteInfoRequestHeader requestHeader =
        (GetRouteInfoRequestHeader) request.decodeCommandCustomHeader(GetRouteInfoRequestHeader.class);

    TopicRouteData topicRouteData = this.namesrvController.getRouteInfoManager().pickupTopicRouteData(requestHeader.getTopic());

    if (topicRouteData != null) {
        if (this.namesrvController.getNamesrvConfig().isOrderMessageEnable()) {
            String orderTopicConf =
                this.namesrvController.getKvConfigManager().getKVConfig(NamesrvUtil.NAMESPACE_ORDER_TOPIC_CONFIG,
                    requestHeader.getTopic());
            topicRouteData.setOrderTopicConf(orderTopicConf);
        }

        byte[] content = topicRouteData.encode();
        response.setBody(content);
        response.setCode(ResponseCode.SUCCESS);
        response.setRemark(null);
        return response;
    }

    response.setCode(ResponseCode.TOPIC_NOT_EXIST);
    response.setRemark("No topic route info in name server for the topic: " + requestHeader.getTopic()
        + FAQUrl.suggestTodo(FAQUrl.APPLY_TOPIC_URL));
    return response;
}
```

**Step1：**调用RouteInfoManager#pickupTopicRouteData方法，从路由表topicQueueTable、brokerAddrTable、filterServerTable中分别填充到TopicRouteData中的queueDatas、brokerDatas、filterServerTable。

**Step2：**如果找到主题对应的路由消息并且该主题为顺序消息，则从NameServer KVConfig中获取关于顺序消息相关的配置填充路由消息。

如果找不到路由消息，则响应的CODE使用TOPIC_NOT_EXISTS，表示没有找到对应的路由。



# RocketMQ消息发送

