---
title: 类图
tags:
  - 类图
categories:
  - 软件工程
date: 2021-06-13 16:52:02
---

## 什么是类图？

UML类图是显示类、接口以及它们之间的静态结构和关系。



## 类之间的关系

![img](README/807775-20160723144134247-1743796764.png)



### 关联关系(Association)

关联关系是一个**“HAS”关系**。

一个对象作为另一个对象的属性，这就是关联关系。

关联关系可以是单向的也可以是双向的。双向的符号是没有方向标的，只是一条直线。

**例：**

单向：

![img](README/807775-20160723144237560-523170503.png)

双向：

![img](README/807775-20160723144249232-258531063.png)

自关联：

![img](README/807775-20160723144303419-630408989.png)



多重性关联关系：

![img](README/807775-20160723144350529-855486099.png)

**例：**

![img](README/807775-20160723144434013-1500135858.png)



### 依赖关系(Dependency)

依赖是**“USE”关系**。

一个类A使用到了另一个类B，这就是依赖关系。

具体表现形式有：

- 一个类作为另一个类的方法的**形式参数**
- 一个类作为另一个类的方法的**局部变量**
- 一个类作为另一个类的方法的**返回值**
- 一个类的方法调用了另一个类的**静态变量**



![img](README/807775-20160723144633185-87772206.png)



### 聚合关系(Aggregation)

整体与部分的关系，即成员变量是整体对象的一部分，但是成员对象能够脱离整体单独存在。



![img](README/807775-20160723144510279-1844358604.png)

聚合关系有一个特点就是**可替换**。



### 组合关系(Composition)

整体与部分的关系，即成员变量是整体对象的一部分，但是部分对象不能脱离整体存在。当整体对象销毁的时候，部分对象也会销毁。

![img](README/807775-20160723144538529-541997244.png)



### 泛化关系(Generalization)

泛化关系就是继承关系，即父类和子类的关系。

![img](README/807775-20160723144659935-1934295664.png)



### 实现关系(Realization)

具体类和接口的关系，就是实现关系。

![img](README/807775-20160723144730763-1113801133.png)





## 参考资料

[类图 - 吴小凯 - 博客园 (cnblogs.com)](https://www.cnblogs.com/ubuntu1/p/9140055.html)
