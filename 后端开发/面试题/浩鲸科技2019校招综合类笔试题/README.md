# 浩鲸科技2019校招综合类笔试题

1. Linux 显示系统主机名的命令是？

   uname -r

   who am i

   uname -n

   whoami

   答案：C

   解析：

   uname

   -a或--all 　显示全部的信息。

   -m或--machine 　显示电脑类型。

   -n或-nodename 　显示在网络上的主机名称。

   -r或--release 　显示操作系统的发行编号。

   -s或--sysname 　显示操作系统名称。

   -v 　显示操作系统的版本。

   --help 　显示帮助。

   --version 　显示版本信息。

   who 可以查询当前登录在系统上的登录用户的信息

   who am i 等同于 who -m，只打印执行该命令的登录用户的信息

   whoami 可以查询当前有效用户的名字

   

2. shell中，将command1的输出作为command2的输入，应使用哪个指令？

   command1 > command2

   command2 > command1

   command2 |command1

   command1 | command2

   答案：D

   

3. 如何在文件中查找显示所有以 `*` 打头的行？

   `find \*file`

   `wc -l *`

   `grep -n * file`

   `grep \* file`

   答案：D

   解析：

   Linux系统中grep命令是一种强大的文本搜索工具，它能使用正则表达搜索文本，并把匹配的行打印出来。

   grep 的全称是 **Global Regular Expression Print**， 表示全局正则表达式版本，它的使用权限是所有用户。

   命令格式：

   ```bash
   grep [option] pattern file
   ```

   ---

   find:查找文件或目录

   

4. *.tar.gz文件解压成文件，用下列哪个指令？ 

   gzip –d

   tar –xzf

   gunzip

   tar –xvf

   答案：B

   解析：

   **tar**

   -c: 建立压缩档案

   -x：解压

   -t：查看内容

   -r：向压缩归档文件末尾追加文件

   -u：更新原压缩包中的文件

   这五个是独立的命令，压缩解压都要用到其中一个，可以和别的命令连用但只能用其中一个。

   下面的参数是根据需要在压缩或解压档案时可选的。

   -z：有gzip属性的

   -j：有bz2属性的

   -Z：有compress属性的

   -v：显示所有过程

   -O：将文件解开到标准输出

   下面的参数-f是必须的

   -f: 指定档案名字，切记，这个参数是最后一个参数，后面只能接档案名。

   ---

   gzip是个使用广泛的压缩程序，文件经它压缩过后，其名称后面会多处“.gz”扩展名。

   ```
   -d或--decompress或----uncompress：解开压缩文件；
   ```

   ---

   gunzip是个使用广泛的解压缩程序，它用于解开被gzip压缩过的文件，这些压缩文件预设最后的扩展名为".gz"。事实上gunzip就是gzip的硬连接，因此不论是压缩或解压缩，都可通过gzip指令单独完成。

   

5. Linux有三个查看文件的命令，若希望在查看文件内容过程中可以用光标上下移动来查看文件内容，应使用命令。

   cat

   more

   less

   menu

   答案：C

   解析：

   cat 一次性将文件内容全部输出

   more 可以分页查看

   less 使用光标向上或向下移动一行

   

6. 下哪个环境变量表示当前路径。

   PATH

   PWD

   HOME

   ROOT

   答案：B

   解析：

   pwd 命令，是 Print Working Directory （打印工作目录）的缩写，功能是显示用户当前所处的工作目录。

   

7. 文件exer1的访问权限为rw-r--r--，现要增加所有用户的执行权限和同组用户的写权限，下列命令正确的是。

   chmod a+x, g+w exer1

   chmod 765 exer1

   chmod o+x exer1

   chmod g+w exer1

   答案：A

   解析：

   chmod 命令用来修改文件权限

   1. 用数字来设定权限

      r : 4、w : 2、x : 1

      4 = 100

      2 = 010

      1 = 001

      实际上是按二进制取1的位来设置的权限

      chmod 777 test.txt

      7 = 111, 给test拥有者、所属群组、其他人所有权限

   2. 用符号设定权限

      \# chmod [ugoa] [+-=] [rwx] dirname/filename

      u：拥有者
      
      g：所属群组
      
      o：其他人
      
      a：所有人

      +：添加权限
      
      -：移除权限
      
      =：设定权限
      
      为 test.txt 文件的所有用户添加读权限。
      
      chmod a+r test.txt

   

8. 下列四项中表示域名的是：

   www.cctv.com

   hk@zj.school.com

   zjwww@china.com

   202.96.68.1234

   答案：A

   

9. 电子邮件地址格式为： username@hostname, 其中 hostname 为______。

   用户地址名

   某国家名

   某公司名

   ISP某台主机的域名

   答案：D

   解析：

   邮箱地址：用户名@域名

   

10. 在B类网络中，可以分配的主机地址是多少? 

    1022

    4094

    32766

    65534

    答案：D

    解析：

    ![](./img/20201019120201.png)

    总主机地址：2^16 = 64*1024 = 65536

    其中，主机号全0和全1的地址去掉。

    可分配的主机地址：65536 - 2 = 65534

    

11. 局域网的协议结构一般不包括。

    网络层

    物理层

    数据链路层

    介质访问控制层

    答案：A

    解析：

    局域网的体系结构遵循IEEE 802.2制定的标准，在该体系结构中，只定义了相当于ISO/OSI七层参考模型的最低两层，即物理层和数据链路层。其中，将数据链路层又分为两个子层，一个是与物理层密切相关的介质访问子层(MAC)，另一个是对上层提供统一服务的逻辑链路控制子层(LLC)，介质访问子层和物理层一起构成了如IEEE 802.3、802.4、802.5等标准，逻辑链路控制子层的标准是IEEE 802.2。

    

12. 将十进制数93转换为二进制数为

    1110111

    1110101

    1010111

    1011101

    答案：D

    

13. 小于（  ）的TCP/UDP 端口号已保留与现有服务一一对应，此数字以上的端口号可自由分配。

    256

    512

    1024

    2048

    答案：C

    

14. 执行下列二进制数算术加法运算10101010+00101010，其结果是_______。

    11010100

    11010010

    10101010

    00101010

    答案：A

    

15. OSI七层模型中，路由器位于哪一层（  ）。

    传输层

    网络层

    物理层

    数据链路层

    答案：B

    

16. 因特网中完成域名地址和IP 地址转换的系统是（ ）。

    POP

    DNS

    SLIP

    Usenet

    答案：B

    

17. DELETE语句用来删除表中的数据,一次可以删除(   )。

    一行

    不能删除

    一行和多行

    只能多行

    答案：C

    

18. （   ）不是用来查询、添加、修改和删除数据库中数据的语句。

    SELECT

    INSERT

    UPDATE

    DROP

    答案：D

    

19. 数据库文件中主数据文件扩展名和次数据库文件扩展名分别为(   )。

    .mdf    .ldf

    .ldf    .mdf

    .mdf    .ndf

    .ndf    .mdf

    答案：C

    

20. 使用SQL命令将教师表teacher中工资salary字段的值增加500，应该使用的命令是（    ）。

    Replace salary with salary+500

    Update teacher salary with salary+500

    Update set salary with salary+500

    Update teacher set salary=salary+500

    答案：D

    

21. 在Oracle中，可用于提取日期时间类型特定部分（如年、月、日、时、分、秒）的函数有（     ）。

    DATEPART

    EXTRACT

    TO_CHAR

    TRUNC

    答案：B、C

    解析：

    语法：

    参考资料：[oracle中extract()函数----用于截取年、月、日、时、分、秒](https://www.cnblogs.com/xqzt/p/4477239.html)

    ```sql
    extract (    
    
            { year | month | day | hour | minute | second }    
    
            | { timezone_hour | timezone_minute }    
    
            | { timezone_region | timezone_abbr }    
    
    from { date_value | interval_value } )
    ```

    实例：	

    ```sql
    SQL> select  extract (year from sysdate) year, extract (month from sysdate) month, extract (day from sysdate) day from  dual;
    
          YEAR      MONTH        DAY
    ---------- ---------- ----------
          2015          5          4
    ```

    参考资料：[Oracle to_char函数的使用方法](https://www.cnblogs.com/aipan/p/7941917.html)

     to_char函数的功能是将数值型或者日期型转化为字符型

    ```sql
    to_char(timestamp 'now','HH12')
    ```

    上面讲当前时间转化成字符串，并获取12进制的小时数

    

22. 在Oracle中，下面用于限制分组函数的返回值的子句是（    ）。

    WHERE

    HAVING

    ORDER BY

    无法限定分组函数的返回值

    答案：B

    

23. 在Oracle数据库的逻辑结构中有以下组件：A、表空间 B、数据块 C、区 D、段，这些组件从大到小依次是（    ）。

    A→B→C→D

    A→D→C→B

    A→C→B→D

    D→A→C→B

    答案：B

    解析：

    一个数据库从逻辑上说是由一个或多个表空间所组成。每一个表空间是由段(segment)组成，一个段是由一组区(extent)所组成，一个区是由一组连续的数据库块(database block)组成，而一个数据库块对应硬盘上的一个或多个物理块。

    

24. 表（TEACHER）包含以下列： 

    ```sql
    ID NUMBER(7) PK
    SALARY NUMBER(7,2)
    SUBJECT_ID NUMBER(7) NOT NULL
    ```

    判断以下两个SQL语句：
    （1）SELECT COUNT（DISTINCT SUBJECT_ID） FROM teacher ;

    （2）SELECT COUNT（SUBJECT_ID） FROM teacher;

    语句1将返回老师的总数

    语句2将返回老师的总数

    两句都返回老师的总数

    以上说话都不对

    答案：B

    

25. 用以下哪个子句来限制分组统计结果信息的显示（  ）。

    FROM

    WHERE

    SELECT

    HAVING

    答案：D

    

26. 有T1表数据如下：
    id1  amount1
    1   10
    2   20
    3   30
    4   40
    5   50
    6   60
     有T2表数据如下：
    id2  amount2
    1   100
    2   200
    5   500

    执行  `update T1 set T1.amount1=(select T2.amount2 from T2 where T1.id1=T2.id2);`  `commit;`
    执行之后查看 `select * from t1 where id1=3;`结果是哪个（  ）

    0
    
    30
    

null
    
其他选项都正确
    
答案：C
    
解析：
    
首先要知道的是这是一个关联子查询，子查询中的T1.id1指的是主查询中的T1。
    
执行步骤：子查询查询一行，主查询就更新一行。
    
    最终T1表的结果如下：
    
    ```sql
    SELECT * FROM T1;
    id1		amount1
    1		100
    2		200
    3		NULL
    4		NULL
5		500
    6		NULL
```
    
    
    
27. 向一个有59个元素的顺序表中插入一个新元素并保持原来顺序不变，平均要移动几个元素（） 

    1

    29

    29.5

    58

    答案：C

    解析：

    (0+1+...+59)/60 = 59(1+59)/2/60 = 59/2 = 29.5

    

28. 十一”黄金周期间，某超市推出如下优惠方案：购物不超过100元不享受优惠；购物超过100元（含100元）但不超过300元，享受9折优惠；购物超过300元（含300元）享受8折优惠。一位顾客在超市的购物实付款252元，请问他在该超市购买了多少价值的商品? 

    280

    315

    252

    280或315

    答案：D

    

29. 4个人在对一部电视剧主演的年龄进行猜测，实际上只有一个人说对了，请问谁说对了？（ ）
    张：她不会超过20岁；
    王：她不超过25岁；
    李：她绝对在30岁以上；
    赵：她的岁数在35岁以下。

    

    张说得对

    她的年龄在35岁以上

    她的岁数在30～35岁之间

    赵说得对

    答案：B

    

30. 有A、B、C、D四个数，它们分别有以下关系：A、B之和大于C、D之和，A、D之和大于B、C之和，B、D之和大于A、C之和。请问，你可以从这些条件中知道这四个数中那个数最小吗？

    A

    B

    C

    D

    答案：C

    

31. 一个人花8块钱买了一只鸡，9块钱卖掉了，然后他觉得不划算，花10块钱又买回来了，11块卖给另外一个人。问他赚了多少？

    1

    2

    3

    答案：B

    

32. 有口井7米深，有个蜗牛从井底往上爬，白天爬3米，晚上往下坠2米，问蜗牛几天能从井里爬出来?

    4

    5

    6

    7

    答案：B

    

33. 圆形的周长扩大至原来的2倍，它的面积比原来增大：

    1倍

    2倍

    3倍

    4倍

    答案：C

    

34. 32名学生需要到河对岸去野营，只有一条船，每次最多载4人（其中需1人划船），往返一次需5分钟，如果9时整开始渡河，9时17分时，至少有（ ）人还在等待渡河。

    15

    17

    19

    22

    答案：C

    

35. 若在有序表的关键字序列为（b,c,d,e,f,g,q,r,s,t），则在二分查找b的过程中，先后进行比较的关键字为

    f c b

    f d b

    g c b

    g d b

    答案：A

    

36. 设有以下定义和语句char str［20］="Program"，*p；p=str；
    则以下叙述中正确的是

    *p与str［0］的值相等

    str与p的类型完全相同

    str数组长度和p所指向的字符串长度相等

    数组str中存放的内容和指针变量p中存放的内容相同

    答案：A

    

37. 设一棵二叉树的中序遍历序列为BDCA，后序遍历序列为DBAC，则这棵二叉树的前序遍历序列为

    BADC

    BCDA

    CDAB

    CBDA
    答案：D

    

38. 有以下程序       （　　　）

    ```c
    struct NODE {int num; struct NODE *next; }
    void main() {
    struct NODE *p,*q,*r;
    p=(struct NODE *)malloc(sizeof(struct NODE));
    q=(struct NODE *)malloc(sizeof(struct NODE));
    r=(struct NODE *)malloc(sizeof(struct NODE));
    p->num=10;q->num=20;r->num=30;
    p->next=q;q->next=r;
    printf("%d\n",p->num+q->next->num); }
```

    程序运行后的输出结果是______。
    
    10
    
    20
    
    30
    
    40
    
    答案：D


​    

39. 阅读c++代码输出

    ```c++
    class base1{
        private: int a,b;
        public:
        base1 ( int i ) : b(i+1),a(b){}
        base1():b(0),a(b){}
        int get_a(){return a;}
        int get_b(){return b;}
    };
    int main()
    {
        base1 obj1(11);
        cout<<obj1.get_a()<<endl<<obj1.get_b()<<endl;
        return 0;
    }
    ```

    12 12

    随机数 12

    随机数 随机数

    12 随机数

    答案：B

    

40. char * const p 意味着：

    p是一个常量指针

    p指向的内容是一个常量

    p是一个常量指针，它指向的内容也是一个常量

    不符合C++语法

    答案：A 

    

41. 某大型公司的HR系统中(基于Oracle)，已建有数据库表部门表(dept)和雇员表(emp)。结构如下： 
    部门表 dept

    > dept_id        部门ID       number
    > dept_name      部门名称     varchar2（200）

    雇员表  emp

    > emp_id         雇员ID       number
    > emp_name       姓名         varchar2（200）
    > dept_id        部门ID       number
    > salary         薪水         number
    > birth_date     出生年月     date
    > entry_date     入职年月(当天)  date
    > sex            性别（男/女）  varchar2（200）

    1、请在雇员表中增加一条记录('100002','张华',002,8000,to_date('1996-09-10','YYYY-MM-DD'),sysdate,'男')

    2、查出部门名称为“人力资源部”的所有女性职员的姓名和薪水，按薪水由高到低排序

    3、请将部门名称为“人力资源部”的所有雇员薪水上浮10%

    答案：

    ```sql
    1、
    insert into emp(emp_id,emp_name,dept_id,salary,birth_date,entry_date,sex)
    values('100002','张华',002,8000,to_date('1996-09-10','YYYY-MM-DD'),sysdate,'男')
    2、
    select emp_name,salary 
    from emp 
    where sex='女' 
    and dept_id in (select dept_id 
                    from dept 
                    where dept_name ='人力资源部')
    order by salary desc
    3、
    update emp set salary=salary*1.1 
    where dept_id in (select dept_id  from dept where dept_name ='人力资源部')
    ```

    

42. 描述：

    1. 26个小写字母，依次压入队列q
    2. 遍历队列q，取出队列第1个字母加到队列末尾，依次循环，直到第10次操作，这时取到的第1个字母弹出队列，计数器清0
    3. 重复步骤2 10次

    问题：

    1. 请用python编写一个函数，满足以上描述，python队列相关的说明参考[https://www.cnblogs.com/CongZhang/p/5274486.html](https://www.cnblogs.com/CongZhang/p/5274486.html)
    2. 给出函数执行结果，即10次操作依次弹出的字母是什么

    答案：

    ```python
    # 1. 函数
    import queue
    q=queue.Queue()
    for i in range(26):
        q.put(chr(i+97))
    d=[]
    def huan(q,popidx):
        while q.empty()==False:
            for i in range(popidx-1):
                q.put(q.get())
            d.append(q.get())
    huan(q,10)
    print(d[0:10])
    # 2. 函数执行结果：['j', 't', 'd', 'o', 'z', 'l', 'x', 'k', 'y', 'n']
    ```

    