---
title: GET请求和POST请求的区别？
tags:
  - GET请求
  - POST请求
  - 面试题
categories:
  - 面试题
  - 计算机网络面试题
date: 2021-06-10 16:11:43
---

1. GET把参数包含在URL中，POST把参数放在请求体中。这样，POST能够发送的数据更大。(URL有长度限制)
2. GET请求比POST请求快
   - POST请求包含更多的请求头(但可忽略不计)
   - **最重要的是：**POST在真正发送数据的时候，会先将请求头发送给服务器进行确认，然后才发送数据
3. GET将数据缓存起来，而POST不会
4. POST请求一般用于修改和写入操作，而GET请求用来获取资源操作。
5. POST请求更加安全(参数不放在URL、数据不缓存)
6. 对参数的数据类型，POST能够发送更多的数据类型(GET只能发送ASCII字符)



---

**POST请求的过程：**

1. 在TCP的第三次握手，浏览器会进行确认并发送POST请求头。
2. 服务器返回100 Continue响应
3. 浏览器发送数据
4. 服务器返回200 OK响应

---

**GET请求的过程：**

1. 在TCP第三次握手，浏览器就会进行确认并发送GET请求头和数据
2. 服务器返回200 OK响应

---

这样GET的总耗是POST的2/3左右。
