# 嬴彻科技2020社招-软件开发

> 来自牛客网，可以自行在牛客网上面测试自己写的代码是否存在问题，能够通过上面的测试用例。



1. [编程题]String literal validation

   > 时间限制：C/C++ 1秒，其他语言2秒空间限制：C/C++ 256M，其他语言512M

   Check if the given string is a valid string literal.

   **输入描述:**

   > A double quoted string

   **输出描述:**

   > true or false

   **示例1**

   输入

   > "abc"

   输出

   > true

   **示例2**

   输入

   > "abc

   输出

   > false

   ```java
   import java.util.*;
   
   public class Main {
       public static void main(String[] args) {
           Scanner scanner = new Scanner(System.in);
           String input = scanner.nextLine();
           // System.out.println("true");
           // or
           // System.out.println("false");
       }
   }
   ```

   

   答案：

   ```java
   import java.util.*;
    
   /**
   满足字符串格式:
   1.字符串前后包含 "
   2.字符串内不能包含 ",如果存在必须进行转义
   3.长度大于>=2
   4. /" // //" 转义判断
   * 其实 /g /p这种不是转义字符的是不能出现在字符串中的，但是本题解没有进行判断(但也能通过测试用例)
   */
   public class Main {
       public static void main(String[] args) {
           Scanner scanner = new Scanner(System.in);
           String input = scanner.nextLine();
           char[] inputChar = input.toCharArray();
           if(inputChar.length<2 || inputChar[0]!='"' || inputChar[inputChar.length-1]!='"') {
               System.out.println("false");
               return;
           }
           
           //标志是否存在 `/`
           int flag = 0;
           //循环判断输入字符，判断字符串是否符合格式(不判断第一个和倒数第一个字符)
           for(int i = 1; i<inputChar.length-1; i++) {
               //如果字符是`/`
               if(inputChar[i]=='\\') {
                   if(flag == 0) {
                       flag = 1;
                   } else {
                       //如果标志位为1，表示该字符被转义了，同时重置标志位
                       flag = 0;
                   }
               } else if (inputChar[i] == '"') {
                   //判断该字符是 "的时候，有无被转义
                    if(flag == 0) {
                       System.out.println("false");
                       return;
                   }
                   flag = 0;
               } else {
                   //当该字符是其他字符的时候
                   flag = 0;
               }
           }
           //此时flag=0，表示倒数第二个字符是`/`
           if(flag == 1) {
               System.out.println("false");
               return;
           }
           System.out.println("true");
            
           // System.out.println("true");
           // or
           // System.out.println("false");
       }
   }
   ```

   

   

2. Convert the department list to preorder traversal of the name of department tree

   **示例1**

   输入

   > [["d1", "d0", "IT"], ["d2", "d0", "RD"], ["d0", "", "The Company"], ["d3", "d0", "HR"]]

   输出

   > ["The Company","HR","IT","RD"]

   **说明**

   > On each level of the department tree, departments are listed in alphabetical order of the name

   ```java
   import java.util.*;
   
   public class Solution {
       /**
        * Convert the department list to preorder traversal of the department tree
        * @param departments string字符串二维数组 [["id1", "parentId", "name"]...], there's only one root department with empty parent id.
        * @return string字符串一维数组
        */
       public String[] listToTree (String[][] departments) {
           // write code here
       }
   }
   ```

   答案：

   题目介绍本身是不能理解题意的，要结合代码中的注解来看。

   之后，我们会发现我们要做的事情如下：

   1. 将给定二维数组转换成树结构
   2. 先序遍历生成的树(但是子节点要按照字典序排序)

   ```java
   import java.util.*;
    
    
   public class Solution {
        
        
       /**
        * Convert the department list to preorder traversal of the department tree
        * @param departments string字符串二维数组 [["id1", "parentId", "name"]...], there's only one root department with empty parent id.
        * @return string字符串一维数组
        */
       public String[] listToTree (String[][] departments) {
           // write code here
           //冒泡排序 每一层按照字典序进行
           for (int i = 0; i < departments.length; i++) {
               for (int j = 0; j < departments.length-1; j++) {
                   if (departments[j][2].compareTo(departments[j+1][2]) > 0) {
                       String[] tmp = departments[j+1];
                       departments[j+1] = departments[j];
                       departments[j] = tmp;
                   }
               }
           }
   
           /*将给定二维数组转换成树*/
   
           //map id Node
           Map<String, Node> map = new HashMap<>();
           //超级结点，这是因为根节点的父节点为"",所以可以通过key ""来获取到根节点
           map.put("", new Node("", ""));
           //map中添加结点
           for (int i=0; i<departments.length; i++) {
               Node node = new Node(departments[i][0], departments[i][2]);
               map.put(node.id, node);
           }
   
           //添加子节点
           for (int i=0; i<departments.length; i++) {
               //获取当前结点的父节点
               Node parent = map.get(departments[i][1]);
               //然后将当前节点添加到父结点的子结点列表中
               parent.sons.add(map.get(departments[i][0]));
           }
   
           //结果集，即先序遍历树访问到的结点名称列表
           List<String> res = new ArrayList<>();
   
           Node root = map.get("").sons.get(0);
           //先序遍历
           preOrder(root, res);
   
           //将上面得到的结果集转换成一维数组
           String[] result = new String[departments.length];
           for (int i = 0; i< res.size(); i++) {
               result[i] = res.get(i);
           }
           return result;
   
       }
       
       /**
       * 结点结构
       */
       class Node {
           String id;
           List<Node> sons;
           String name;
   
           Node(String id, String name) {
               this.id = id;
               this.sons = new ArrayList<>();
               this.name = name;
           }
       }
        
      /**
      * 先序遍历
      * @param root 根节点
      * @param res 存储遍历到的结点名称的列表
      */
       public static void preOrder(Node root, List<String> res) {
           res.add(root.name);
           if (root.sons.isEmpty()) {
               return;
           }
           for (Node node: root.sons) {
               preOrder(node, res);
           }
       }
   }
   ```

   