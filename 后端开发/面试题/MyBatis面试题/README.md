---
title: MyBatis面试题
tags:
  - MyBaits
  - 面试题
categories:
  - 面试题
  - MyBatis面试题
date: 2021-06-05 17:20:59
---



## 什么是MyBatis?

1. MyBatis是持久层框架，对JDBC进行封装，开发时只需要关注SQL本身，不需要花费精力去处理加载驱动、创建连接、创建statement对象等繁琐的过程。省去了几乎所有的JDBC代码，不需要手动设置参数以及处理结果集。
2. MyBatis可以通过XML或注解进行配置，能够将POJO映射为数据库的记录。
3. MyBatis可以通过XML或注解的方式对需要执行的statement进行配置，生成最终执行的SQL语句。MyBatis执行SQL之后，自动将结果集映射为Java对象并返回。



## MyBatis的优点

1. 基于SQL编程，非常灵活，不会对应用程序和数据库的现有设计造成任何影响。
2. 与JDBC相比，减少了大量冗余的代码。不需要手动创建和销毁连接。
3. 能够很好的与各种数据库兼容。
4. 与Spring能够很好的集成。



## MyBatis的缺点

1. 当SQL的编写工作量大的时候，对开发人员的SQL功底有一定要求。
2. SQL语句依赖于数据库，导致MyBatis数据库移植性差，不能随便切换数据库。



## MyBatis框架使用场合

1. MyBatis是一个DAO层解决方案
2. 对性能要求高、需求变化较多的项目，如互联网项目



## MyBatis中#{}和${}的区别

`#{}`是预编译处理，`${}`是字符串替换。

MyBatis在处理`#{}`的时候，会将SQL中的`#{}`替换成占位符`?`，通过调用`PreparedStatement`的`setXxx`方法来替换占位符。

MyBatis在处理`${}`的时候，就是直接将`${}`替换成变量的值。

使用`#{}`可以有效的避免SQL注入，提高系统的安全性。



## 当实体类的属性名和表中的字段名不相同，怎么办？

1. 通过在查询的SQL语句中定义字段名的别名，使得别名和属性名一致

   ```xml
   <select id="selectorder" parameterType="int" resultType="com.xgc.entity.Order">
       select order_id id, order_no orderno, order_price price
       from orders
       where order_id = #{id}
   </select>
   ```

2. 通过resultMap标签映射属性名和字段名的关系

   ```xml
   <resultMap id="orderResultMap" type="com.xgc.entity.Order">
       <!-- 用id标签来映射主键字段 -->
       <id property="id" column="order_id" />
       <!-- 用result标签来映射非主键字段 -->
       <result property="orderno" column="order_no" />
       <result property="price" column="order_price" />
   </resultMap>
   ```



## 通常一个XML映射文件都会有一个DAO接口与之对应。请问这个DAO接口的工作原理是什么？DAO接口里的方法能重载吗？

**DAO接口的工作原理**：MyBatis在运行时会使用JDK动态代理为DAO接口生成一个代理对象，代理对象会拦截接口方法，进而执行XML中配置的对应的SQL语句，然后将SQL执行结果封装返回。

**DAO接口中的方法不能重载。**这是因为MyBatis使用**接口全限定名+方法名**的保存和查找策略。



## MyBatis是如何进行分页的？分页插件的原理是什么？

MyBatis使用`RowBounds`对象进行分页，它是针对结果集ResultSet进行的内存分页，而不是物理分页。我们可以在SQL中直接书写分页参数来实现物理分页，也可以通过分页插件来实现物理分页。

分页插件通过MyBatis提供的插件接口，实现自定义插件，在插件的拦截方法中拦截需要执行的SQL语句，重写SQL，根据dialect方言，添加对应的物理分页语句和物理分页参数。



## MyBatis如何将SQL执行结果封装成目标对象并返回？都有哪一些映射形式？

映射形式有两种：

1. 通过`<resultMap>`标签来定义表的列名和对象属性名之间的映射关系。
2. 在SQL语句中为列名起别名，使得别名和对象属性名一致。

在拥有了列名和属性名的映射关系下，MyBatis通过反射创建对象并为对象属性赋值，最终将对象返回。



## 如何获取自动生成的(主)键值？

insert方法会返回一个值，这个值就是插入的行数。

MyBatis可以通过useGeneratedKeys、keyProperty属性来将生成的键值设置到传入的参数对象中。

```xml
<insert id="insertUser" useGeneratedKeys="true" keyProperty="id">
    insert into user (username, age) values (#{username}, #{age})
</insert>
```

```java
//在这条语句执行之后，MyBatis会将insert返回的自增键值id 设置到我们传入的对象user的id属性中
userMapper.insertUser(user);
```



## MyBatis动态SQL有什么用？执行原理是什么？有哪些动态SQL？

MyBatis动态SQL可以在XML映射文件中，以标签的形式编写动态SQL。

执行原理：根据表达式的值 进行逻辑判断，完成动态拼接SQL的功能。

动态SQL标签(9种)：

- if、choose、when、otherwise
- where、trim、set、foreach
- bind



## xml映射文件中，除了常见的select、update、delete、insert标签之外，还有哪些标签？

- resultMap、parameterMap
- sql、include
- selectKey
- 9个动态SQL标签



## MyBatis的xml映射文件中，不同的xml映射文件，id可不可以重复？

不同的xml映射文件，如果设置了namespace属性，那么id可以重复。否则，不可以重复。

因为MyBatis使用 namespace+id 的存储查找策略。



## 为什么说MyBatis是半orm映射工具？它和全自动的有什么区别？

MyBatis是半orm映射工具，它需要在xml或注解中编写SQL语句。Hibernate是全自动orm映射工具，可以自动生成SQL语句，不需要手动编写。



## 一对一、一对多的关联查询?

**一对一的关联查询：**

学生和课室是一对一的关系。当我们需要查询学生的同时，也将对应学生的课室信息也查询出来时，可以使用关联查询。如下，我们输入学生id就可以获取对应学生和课室信息。

```xml
<mapper namespace="com.xgc.mapper.StudentMapper">
    <!-- 一对一关联查询 association -->
    <resultMap id="studentClassroomsResultMap" type="com.xgc.entity.Student">
        <id property="id" column="id" />
        <result property="name" column="name" />
        
        <association property="classroom" javaType="com.xgc.entity.Classroom">
            <id property="id" column="cid" />
            <result property="name" column="cname" />
        </association>
    </resultMap>
    
    <select id="selectStudentClassroom" parameterType="int" 
            resultMap="studentClassroomsResultMap">
        select s.id, s.name, c.id cid, c.name cname from student s, classroom c
        where s.classroomId = c.id
        and s.id = #{id}
    </select>
</mapper>
```



**一对多的关联查询：**

课室和学生是一对多的关系。一个课室可以有多个学生。如下：我们输入课室id，可以将课室和课室内的学生信息全部获取出来。

```xml
<mapper namespace="com.xgc.mapper.ClassroomMapper">
    <resultMap id="classroomStudentResultMap" type="com.xgc.entity.Classroom">
        <id property="id" column="id" />
        <result property="name" column="name" />
        
        <collection property="students" ofType="com.xgc.entity.Student">
            <id property="id" column="sid" />
            <result property="name" column="sname" />
        </collection>
    </resultMap>
    
    <select id="selectClassroomStudent" parameterType="int" 
            resultMap="classroomStudentResultMap">
        select c.id, c.name, s.id sid, s.name sname from classroom c, student s
        where c.id = s.classroomId
        and c.id = #{id}
    </select>
</mapper>
```



## MyBatis实现一对一有几种方式？具体怎么操作？

MyBatis实现一对一有两种方式：嵌套查询和连接查询。

关系：丈夫和妻子(一对一关系)

实现效果：通过丈夫的id查询丈夫和妻子的信息。

**连接查询：**

```xml
<mapper namespace="com.xgc.mapper.Husband">
    <resultMap id="husbandWifeResultMap" type="com.xgc.entity.Husband">
        <id property="id" column="id" />
        <result property="name" column="name" />
        
        <association property="wife" javaType="com.xgc.entity.Wife">
            <id property="id" column="wid" />
            <result property="name" column="wname" />
        </association>
    </resultMap>
    
    <select id="selectHusbandWife" parameterType="int" 
            resultMap="husbandWifeResultMap">
        select h.id, h.name, w.id wid, w.name wname from husband h, wife w
        where h.id = w.id
        and h.id = #{id}
    </select>
</mapper>
```



**嵌套查询：**

```xml
<mapper namespace="com.xgc.mapper.HusbandMapper">
    <resultMap id="husbandWifeResultMap" type="com.xgc.entity.Husband">
        <id property="id" column="id" />
        <result property="name" column="name" />
        
        <!-- column属性指定的是 selectWife 查询语句的参数 -->
        <!--
			如果selectWife是另一个Mapper文件指定的，select属性这么取值：
			com.xgc.mapper.WifeMapper.selectWife
		-->
        <association property="wife"
                     javaType="com.xgc.entity.Wife"
                     select="selectWife"
                     column="id">
        </association>
    </resultMap>
    
    <select id="selectHusband" parameterType="int" 
            resultMap="husbandWifeResultMap">
        select * from husband
        where id = #{id}
    </select>
    
    <select id="selectWife" parameterType="int" 
            resultType="com.xgc.entity.Wife">
        select * from wife
        where id = #{id}
    </select>
</mapper>
```



## MyBatis实现一对多有几种方式？具体如何操作？

MyBatis实现一对多有两种方式：嵌套查询和连接查询。

关系：课室和学生(一对多)

**连接查询：**

```xml
<mapper namespace="com.xgc.mapper.ClassroomMapper">
    <resultMap id="classroomStudentResultMap" type="com.xgc.entity.Classroom">
        <id property="id" column="id" />
        <result property="name" column="name" />
        
        <collection property="students" ofType="com.xgc.entity.Student">
            <id property="id" column="sid" />
            <result property="name" column="sname" />
        </collection>
    </resultMap>
    
    <select id="selectClassroomStudent" parameterType="int" 
            resultMap="classroomStudentResultMap">
        select c.id, c.name, s.id sid, s.name sname from classroom c, student s
        where c.id = s.classroomId
        and c.id = #{id}
    </select>
</mapper>
```



**嵌套查询：**

```xml
<mapper namespace="com.xgc.mapper.ClassroomMapper">
    <resultMap id="classroomStudentResultMap" type="com.xgc.entity.Classroom">
        <id property="id" column="id" />
        <result property="name" column="name" />

        <collection property="students"
                    ofType="com.xgc.entity.Student"
                    select="selectStudents"
                    column="id">
        </collection>
    </resultMap>

    <select id="selectClassroomStudent" parameterType="int"
            resultMap="classroomStudentResultMap">
        select * from classroom
        where id = #{id}
    </select>

    <select id="selectStudents" parameterType="int"
            resultType="com.xgc.entity.Student">
        select * from student
        where classroomId = #{id};
    </select>
</mapper>
```



## MyBatis是否支持延迟加载？实现原理是什么？

MyBatis仅支持association关联对象和collection关联集合对象的延迟加载。

**实现原理：**通过cglib为目标对象创建代理对象。当目标方法被调用时，会进入拦截器方法。如：`a.getB().getName()`，当发现`a.getB()`的值为null时，执行事先存储的查询关联B对象的SQL语句，查询到结果后，调用`a.setB()`方法将对象B赋值给a的B属性。接着，执行`a.getB().getName()`方法。这就是MyBatis的延迟加载。



## 介绍一下MyBatis的一级缓存和二级缓存？

**一级缓存：**借助`PerpetualCache`实现，能够将查询结果缓存起来，作用范围是session。当session flush或close的时候，session内的缓存全部清空。默认开启一级缓存。

**二级缓存：**跟一级缓存的实现机制类似，都是借助于`PerpetualCache`，作用范围是mapper，并且可以自定义缓存源。MyBatis要求二级缓存返回的对象必须可以序列化，而且mapper文件要添加`<cache />`。默认不开启二级缓存。

**缓存数据更新机制：**当某个作用范围内进行了C|U|D操作，那么该作用范围内的所有缓存都会被清空。



## 什么是MyBatis的接口绑定，有哪些实现方式？

**MyBatis的接口绑定：**在程序中创建一个接口，并使得该接口的方法和对应SQL进行绑定。那么，当调用接口方法的时候，就可以执行对应的SQL语句。

实现方式有注解和xml文件两种方式：

- 注解。在接口的方法上标注`@Select`，`@Update`等注解，并且注解包含对应的SQL语句。
- xml文件。在mapper映射文件中编写SQL语句，通过namespace + id的方式对应mapper接口中的方法。



## 使用MyBatis的mapper接口调用有哪些要求？

1. xml映射文件的namespace属性要指定绑定的接口
2. xml映射文件的id要与mapper接口的方法名对应
3. mapper接口方法的入参和出参类型要与xml映射文件中一致。



## Mapper编写有几种方式？

1. 接口实现类继承`SQLSessionDaoSupport`，此方法需要编写mapper接口、mapper接口的实现类和mapper.xml文件。
2. 使用`org.myabtis.spring.mapper.MapperFactoryBean`，此方法需要在`SqlMapConfig.xml`中配置`mapper.xml`的位置，还需定义`mapper`接口。
3. 使用`mapper`扫描器，需要编写`mapper.xml`、mapper接口，配置Mapper扫描器，使用扫描器从Spring容器中获取mapper的实现对象。

