---
title: 泛型类和泛型方法
tags:
  - 泛型
categories:
  - JavaSE
date: 2021-05-29 17:49:38
---

## 不使用泛型的代码

### Generic

```java
public class Generic {
    
    public void show(String s) {
        System.out.println(s);
    }
    
    public void show(Integer i) {
        System.out.println(i);
    }
    
    public void show(Boolean b) {
        System.out.println(b);
    }
}
```



### GenericDemo

```java
public class GenericDemo {
    
    public static void main(String[] args) {
        Generic g = new Generic();
        g.show("abc");
        g.show(123);
        g.show(true);
    }
}
```

如上，我们可以看到，没有使用泛型的代码，方法在处理逻辑相同但参数类型不同的情况下，不能复用。

我们可以使用泛型来解决这个问题。



## 使用泛型类

### Generic

```java
public class Generic<T> {
    
    public void show(T t) {
        System.out.println(t);
    }
}
```



### GenericDemo

```java
public class GenericDemo {
    
    public static void main(String[] args) {
        Generic<String> g1 = new Generic<>();
        g.show("abc");
        Generic<Integer> g2 = new Generic<>();
        g2.show(123);
        Generic<Boolean> g3 = new Generic<>();
        g3.show(true);
    }
}
```



## 使用泛型方法

### Generic

```java
public class Generic {
    
    public <T> void show(T t) {
        System.out.println(t);
    }
}
```



### GenericDemo

```java
public class GenericDemo {
    
    public static void main(String[] args) {
        Generic g = new Generic();
        g.show("abc");
        g.show(123);
        g.show(true);
    }
}
```



## 小结

我们可以看到没有使用泛型的代码，方法在处理逻辑相同但参数类型不同的情况下，不能复用。

但是我们可以通过泛型类和泛型方法来解决这个问题。

泛型类和泛型方法的区别在于：

- 泛型类：类型通过创建对象时传入的类型决定
- 泛型方法：类型通过方法中传入的形参类型决定