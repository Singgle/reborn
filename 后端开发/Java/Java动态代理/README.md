---
title: Java动态代理
tags:
  - Java动态代理
categories:
  - JavaSE
date: 2021-06-15 12:50:02
---



> 代理模式分为静态代理和动态代理。
>
> 当代理类的class文件在程序运行之前就已经存在，这就是静态代理。
>
> 动态代理的代理对象在程序运行期间通过反射动态生成。



## 静态代理

```java
public interface Human {
    
    void sing();
}
```

```java
public class SuperStar implements Human {
    
    @Override
    public void sing() {
        System.out.println("SuperStar is singing");
    }
}
```

```java
@Data
public class StarAgent implements Human {
    
    private Human human;
    
    @Override
    public void sing() {
        System.out.println("经纪人进行准备工作");
        human.sing();
        System.out.println("经纪人进行收尾工作");
    }
}
```

```java
public class Client {
    
    public static void main(String[] args) {
        StarAgent starAgent = new StarAgent();
        starAgent.setHuman(new SuperStar());
        starAgent.sing();
    }
}
```

运行结果如下：

```
经纪人进行准备工作
SuperStar is singing
经纪人进行收尾工作
```



## Java动态代理

### Human

```java
public interface Human {

    void sing();
}
```



### SuperStar

```java
public class SuperStar implements Human{

    @Override
    public void sing() {
        System.out.println("SuperStar is singing");
    }
}
```



### ProxyFactory

```java
/**
 * 代理工厂
 */
public class ProxyFactory {

    public static Object getProxyInstance(Object obj) {
        MyInvocationHandler handler = new MyInvocationHandler();
        handler.bind(obj);

        /*
        * Proxy.newProxyInstance方法参数说明
        * 第一个参数：用哪个类加载器加载代理对象
        * 第二个参数：代理类需要实现的接口
        * 第三个参数：InvocationHandler对象。当代理类对象的方法执行时，调用InvocationHandler对象的invoke方法执行。
         */
        return Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                handler
                );
    }
}
```



### MyInvocationHandler

```java
public class MyInvocationHandler implements InvocationHandler {

    //被代理对象
    private Object obj;

    //绑定被代理对象
    public void bind(Object obj) {
        this.obj = obj;
    }

    /**
     *
     * @param proxy 代理对象
     * @param method 代理对象调用的方法
     * @param args 调用方法的参数
     * @return 方法返回值
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("经纪人进行准备工作");
        /*
        * method.invoke方法介绍
        * 第一个参数：被代理对象
        * 第二个参数：传入到被代理对象同名方法的参数列表
        * 返回值：被代理对象调用方法的返回值
         */
        Object result = method.invoke(obj, args);
        System.out.println("经纪人进行收尾工作");
        return result;
    }
}
```



### Client

```java
public class Client {

    public static void main(String[] args) {
        Human starAgent = (Human) ProxyFactory.getProxyInstance(new SuperStar());
        starAgent.sing();
    }
}
```

执行结果如下：

```
经纪人进行准备工作
SuperStar is singing
经纪人进行收尾工作
```


