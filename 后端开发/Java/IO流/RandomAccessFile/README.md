# RandomAccessFile

RandomAcessFile可以实现对文章的读取和写入，同时还支持随机写入。



## RandomAccessFile构造方法

RandomAccessFile存在两个构造方法

```java
public RandomAccessFile(File file, String mode) throws FileNotFoundException
```

```java
public RandomAccessFile(String name, String mode) throws FileNotFoundException
```

两个构造方法的mode参数指定的是RandomAccessFile的访问模式，有四种。

1. **r**：只读模式
2. **rw**：可读写模式
3. **rws**：可读写模式，但对**文件的内容**或**元数据**的每个更新都要同步写入基础存储设备
4. **rwd**：可读写模式，但对**文件的内容**的每个更新都要同步写入基础存储设备



## RandomAccessFile方法

RandomAccessFile对象维护了一个记录指针，用于记录当前读写的位置。

RandomAccessFile类拥有两个方法用来操作记录指针

```java
//返回文件记录指针的当前位置
long getFilePointer();

//将文件记录指针移动到pos位置
void seek(long pos)
```

