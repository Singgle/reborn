# 阻塞队列

## 什么是阻塞队列

- 当阻塞队列空的时候，从队列中获取元素的操作会被阻塞
- 当阻塞队列满的时候，从队列中存元素的操作会被阻塞



## 为什么需要阻塞队列？有什么好处？

在多线程开发中，线程在某些情况下会被挂起(即阻塞)，但在某些情况下会被唤醒。



使用JUC提供的BlockingQueue：我们不再关心什么时候需要阻塞线程，什么时候需要唤醒线程。BlockingQueue会自动管理这一切。



## JUC包中有哪些类别的阻塞队列

- ArrayBlockingQueue：由数组结构组成的有界阻塞队列
- LinkedBlockingQueue：由链表结构组成的有界阻塞队列
- PriorityBlockingQueue：支持优先级排序的无界阻塞队列
- DelayQueue：使用优先级队列实现的延迟无界阻塞队列
- SynchronousQueue：不存储元素的阻塞队列，即单个元素的阻塞队列
- LinkedTansferQueue：由链表结构组成的无界阻塞队列
- LinkedBlockingDeque：由链表结构组成的双向阻塞队列



## BlockingQueue的核心方法

核心方法如下：

| 方法类型 | 抛出异常  | 特殊值   | 阻塞   | 超时               |
| -------- | --------- | -------- | ------ | ------------------ |
| 插入     | add(e)    | offer(e) | put(e) | offer(e,time,unit) |
| 移除     | remove()  | poll()   | take() | poll(time,unit)    |
| 检查     | element() | peak()   | 无     | 无                 |

说明如下：

| 方法     | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| 抛出异常 | 当阻塞队列满的时候，再往队列add元素，会抛出IllegalStateException:Queue full<br />当阻塞队列空的时候，从队列remove元素，会抛出NoSuchElementException |
| 特殊值   | 往队列offer元素，成功返回true，否则返回false<br />往队列poll元素，成功返回移除元素，否则返回null |
| 阻塞     | 当阻塞队列满的时候，再往队列put元素，会一直阻塞到队列不满为止<br />当阻塞队列空的时候，再往队列take元素，会一直阻塞到队列不空为止 |
| 超时     | 当阻塞队列满的时候，从队列offer元素，会阻塞一定时间，超出退出<br />当阻塞队列空的时候，从队列poll元素，会阻塞一定时间，超时退出 |

