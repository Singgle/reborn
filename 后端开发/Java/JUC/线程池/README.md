# 线程池

## 为什么需要线程池？

线程池的工作是统一管理线程，控制并发量。



## 线程池的好处？

1. 线程复用。线程复用，可以降低资源的消耗。这是因为这可以减少线程创建和销毁的开销。
2. 提高响应速度。当任务到来的时候可以立即执行。
3. 提供线程的可管理性。线程的分配、调优和监控由线程池统一管理。



## Java中的线程池

Java提供了线程池的创建方法。

在Java中，线程池的核心接口是ExecutorService。



### 常见的线程池

1. newFixedThreadPool。固定长度的线程池。
2. newSingleThreadExecutor。只有一个线程的线程池。
3. newCachedThreadPool。缓存线程池，线程数为`Integer.MAX_VALUE`



### 线程池底层实现

上面三种线程池的底层实现如下：

```java
public static ExecutorService newFixedThreadPool(int nThreads) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>());
}

public static ExecutorService newSingleThreadExecutor() {
    return new FinalizableDelegatedExecutorService
        (new ThreadPoolExecutor(1, 1,
                                0L, TimeUnit.MILLISECONDS,
                                new LinkedBlockingQueue<Runnable>()));
}

public static ExecutorService newCachedThreadPool() {
    return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                  60L, TimeUnit.SECONDS,
                                  new SynchronousQueue<Runnable>());
}
```

可以看到都是通过ThreadPoolExecutor()来实现的。



### ThreadPoolExecutor

```java
//该方法就是上面线程池实现的方法，通过不同的参数创建不同策略的线程池
public ThreadPoolExecutor(int corePoolSize,
                          int maximumPoolSize,
                          long keepAliveTime,
                          TimeUnit unit,
                          BlockingQueue<Runnable> workQueue) {
    this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
         Executors.defaultThreadFactory(), defaultHandler);
}

//线程池实现的方法，一共7个参数
public ThreadPoolExecutor(int corePoolSize,
                          int maximumPoolSize,
                          long keepAliveTime,
                          TimeUnit unit,
                          BlockingQueue<Runnable> workQueue,
                          ThreadFactory threadFactory,
                          RejectedExecutionHandler handler) {
```



#### 7大参数

1. corePoolSize，核心线程数。线程池中在空闲下能够拥有的最大线程数。
2. maxinumPoolSize，最大线程数。线程池中能够拥有的最大线程数。
3. keepAliveTime，最大存活时间。超出核心线程数的线程能够存活的最大时间。超过这段时间的多余线程被销毁。
4. unit，最大存活时间的时间单位。
5. workQueue，任务队列。当核心线程都在执行任务时，新的任务存放在任务队列。
6. threadFactory，创建线程的工厂。一般使用默认的线程工厂即可。
7. handler，拒绝策略。当线程都繁忙并且任务队列都满的时候，新的任务的拒绝策略。



#### 拒绝策略(4种)

它们都实现了RejectedExecutionHandler接口

1. AbortPolicy(默认)。直接抛出RejectedExecutionException。
2. CallerRunsPolicy。该策略既不抛出异常，也不丢弃任务，而是将任务返回给调用它的线程来执行。
3. DiscardOldestPolicy。抛弃任务队列中等待时间最久的任务，然后尝试提交当前任务到任务队列中。
4. DiscardPolicy。直接抛弃当前提交的任务。如果允许任务丢失，这是最好的策略。



### 线程池运行策略

1. 创建线程池之后，线程池等待任务的提交
2. 当调用execute()方法添加一个任务的时候，线程池进行如下判断
   - 1. 如果当前运行的线程小于corePoolSize，立即创建线程执行这个任务
   - 2. 如果当前运行的线程大于或等于corePoolSize，则将这个任务放在任务队列
   - 3. 如果任务队列满了，而运行的线程数小于maxinumPoolSize，则立即创建线程执行这个任务
   - 4. 如果任务队列满了，运行的线程数为最大线程数，那么使用对应的拒绝策略处理这个任务
3. 当一个线程完成任务后，它会从任务队列中取下一个任务来执行
4. 当一个线程空闲了最大存活时间，线程池会判断
   - 如果当前线程数小于或等于corePoolSize，不做操作
   - 如果当前线程数大于corePoolSize，则这个线程会被销毁。所以，线程池的所有任务都执行完成后，最后线程池中只存在corePoolSize个线程。



### 如何选择使用哪种线程池？

虽然JUC中提供各种类型的线程池供我们直接使用，但是在开发中不允许使用Java提供的线程池。

当我们需要线程池的时候，我们需要使用ThreadPoolExecutor方法自定义线程。这是为了能够开发者更加明确线程池的运行规则，避免资源耗尽的风险。



### 如何合理配置线程池的线程数？

这需要根据任务类型来决定。

1. 若任务类型为CPU密集型(即任务需要大量的计算，没有阻塞)

   这种类型需要尽可能少的线程数。

   需要CPU核数+1个线程

2. 若任务类型IO密集型(即任务会出现大量阻塞)

   IO密集型的任务有两种公式配置线程数

   - 1. CPU核数*2个线程

   - 2. 一些大厂会使用的公式：

        CPU核数/(1-阻塞系数)

        阻塞系数在0.8-0.9左右

     比如8核CPU配置的线程数为：8/(1-0.9)=80。即需要配置80个线程数

系统的CPU核数可以通过下面代码获得

```java
Runtime.getRuntime().availableProcessors();
```
