# 乐观锁CAS

## 什么是CAS？

CAS(Compare And Swap)，比较再交换。

JDK1.5增加了JUC包，使用CAS实现不同于synchronized的同步机制。JDK5之前使用synchronized来实现同步，这是一种独占锁，也是悲观锁。



## CAS算法理解

CAS是一种无锁算法，CAS有三个操作数：内存值V，预期值A，要更新的值B。

当且仅当**预期值A**和**内存值V**相等的时候，才将内存值修改为**要更新的值B**。



## CAS在JUC的应用和实现原理

**CAS在JUC中通过自旋和Unsafe类实现。**



剖析如下：

以`AtomicInteger`为例

```java
AtomicInteger atomicInteger = new AtomicInteger();
atomicInteger.getAndIncrement();
```

`AtomicInteger`的`getAndIncrement`方法是一个自增的原子操作。源码如下：

```java
/**
* AtomicInteger类的getAndIncrement方法，实现原子自增
* 利用Unsafe类的getAndAddInt来实现原子自增
*/
public final int getAndIncrement() {
    return unsafe.getAndAddInt(this, valueOffset, 1);
}
```

继续追踪，unsafer.getAndAddInt的源码如下：

注意这一段代码是从rt.jar中反编译出来的

```java
/**
* Unsafe类的getAndInt方法，完成获取然后增加变量指定值var4
* @param var1 当前对象
* @param var2 变量对应的地址
* @param var4 变量需要增加的值
*/
public final int getAndAddInt(Object var1, long var2, int var4) {
    // AtomicIntger对应的值
    int var5;
    do {
        //从内存中获取指定地址var2的变量值
        var5 = this.getIntVolatile(var1, var2);
        
        //this.compareAndSwapInt CAS操作
        //如果var2指定地址的值 和 预期值var5 相同，那么将内存值替换成 var5 + var4
    } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));

    return var5;
}
```

可以看到上面`Unsafe`的`getAndAddInt`使用do-while语句来循环比较内存值和预期值是否相等。同时CAS实现的核心类就是Unsafe类。这就是为什么前面说：**CAS在JUC中通过自旋和Unsafe类实现。**



## CAS系统原语

在上面**CAS在JUC的应用和实现原理**一节中，我们看到AtomicInteger自增操作是通过Unsafe类的`compareAndSwapInt`方法来实现的。

现在我们来看看这个方法的定义

```java
public final native boolean 
    compareAndSwapInt(Object var1, long var2, int var4, int var5);
```

可以看到这个方法是一个本地方法。这个方法是就是核心的CAS操作，**这个操作是原子性的**。



接下来谈谈这个方法是如何实现原子性的？

CAS是一种系统原语。所谓原语由数条指令组成，用来实现某些功能的一段过程。这个过程是不可中断，即**这个操作是原子性的**。

因此，JUC的原子类的原子操作是通过底层的系统原语来实现的。



## CAS的缺点

CAS的缺点：

1. 当并发大的时候，就会存在自旋时间过长的情况，就会导致CPU资源浪费
2. 只能保证一个共享变量的原子操作
3. 存在ABA问题



## ABA问题

假设现在存在两个线程对同一个共享变量进行操作。

现存在下面这种情况：

1. 线程A和线程B都将变量读取到自己的本地内存中。
2. 线程A先将变量的值M修改为N，然后再将变量值N修改M
3. 线程B发现变量值未发生改变，于是CAS操作成功

上面这种情况就是ABA问题。实际上变量值发生了变动的过程，但是另一个线程却认为它没有发生改变，CAS操作成功。

**尽管线程B的CAS操作成功，但是不代表这个过程没有问题。**



## 如何解决ABA问题？

通过为变量增加一个版本号。

每次操作都会将变量的版本号 + 1。这样就可以解决ABA问题。











