# synchronized和Lock有什么区别？

1. 构成层面

   synchronized是JVM层面的锁，通过monitor实现锁

   Lock是api层面的锁

2. 使用方法

   synchronized不需要手动释放锁，当同步代码执行完成或出现异常时，它会让系统自动释放锁。

   Lock需要手动释放锁，不然会出现死锁现象

3. 是否可中断

   synchronized不可被中断，要么代码执行成功或抛出异常

   ReentrantLock可以被中断

   - `tryLock()`方法来设置超时时间
   - 使用`lockInterruptibly()`来获得锁，使用`interrupt()`方法来中断

4. 是否公平锁

   synchronized是非公平锁

   ReentrantLock可以通过指定构造参数来构造公平锁或非公平锁

5. 绑定多个Condition

   synchronized不能绑定Condition，要么随机唤醒一个线程，要么唤醒全部线程

   ReentrantLock可以分段唤醒线程

