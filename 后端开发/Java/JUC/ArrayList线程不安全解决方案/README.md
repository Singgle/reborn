# ArrayList线程不安全解决方案

ArrayList是线程不安全的，解决方案如下：

1. 不使用ArrayList类，使用Vector类(线程安全)
2. 使用`Collections.synchronizedList()`方法将ArrayList对象封装成线程安全的对象
3. 不是用ArrayList类，使用`CopyOnWriteArrayList`类



第一种方案，不使用，因为Vector使用synchronized来实现同步，在并发情况下，效率太低，**过时**

第二种方案，可以使用

第三种方案，推荐使用，这是一个写时复制的容器。



## 为什么推荐使用CopyOnWriteArrayList？

CopyOnWrite是一种写时复制的容器。通俗的理解是当我们需要往一个容器里添加元素时，先复制出一个新的容器，然后再往新的容器里添加元素，之后再将指针指向新的容器。

这样的好处是：可以对容器进行并发的读，而不需要加锁。因为当前的容器不需要添加任何的元素。所以，CopyOnWrite是一种读写分离的容器，这可以减少读操作不必要的阻塞。



## CopyOnWriteArrayList的读操作源码剖析

下面这段是`CopyOnWriteArrayList`的`add`方法源码

```java
public boolean add(E e) {
    //使用ReentrantLock来加锁
    final ReentrantLock lock = this.lock;
    lock.lock();
    try {
        Object[] elements = getArray();
        int len = elements.length;
        //复制出一个新的数组
        Object[] newElements = Arrays.copyOf(elements, len + 1);
        //添加元素
        newElements[len] = e;
        //将指针指向新的数组
        setArray(newElements);
        return true;
    } finally {
        lock.unlock();
    }
}
```



## HashSet和HashMap线程不安全的解决方案

HashSet和HashMap都是线程不安全的，都可以通过`Collections.synchronizedxxx`方法进行封装，实现同步。



### HashSet

读写分离的Set为`CopyOnWriteArraySet`。

查看`CopyOnWriteArraySet`类的无参构造器源码：

```java
public CopyOnWriteArraySet() {
    al = new CopyOnWriteArrayList<E>();
}
```

可以看到`CopyOnWriteArraySet`底层是使用`CopyOnWriteArrayList`来实现的。



### HashMap

Map对应JUC包下面的并发类是`ConcurrentHashMap`，它使用锁分段技术来提高并发能力。







