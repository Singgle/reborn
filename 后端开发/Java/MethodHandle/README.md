---
title: MethodHandle
tags:
  - JavaSE
categories:
  - JavaSE
date: 2021-07-05 20:59:51
---



## JDK何时引入？

MethodHandle是JDK7引入的API，该包提供了一种新的动态调用方法的 机制。



## 什么是MethodHandle？

MethodHandle是对底层方法、构造函数、字段或类似低级操作的类型化，直接可执行的引用，具有参数或返回值的可选转换。

简单点来说，MethodHandle就是一个方法句柄，能够通过这个方法句柄调用对应方法。



## 使用示例

先演示String类的replace方法的使用，如下：

```java
public class CommonTest {

    public static void main(String[] args) {
        String str = "abc";
        String handleStr = str.replace("a", "c");
        System.out.println(handleStr);
    }
}
```

执行结果：

```
cbc
```

在这里，我们演示使用MethodHandle方式来调用String类的replace方法，将abc字符串内的所有a字符替换成c字符。

```java
public class MethodHandleTest {
    public static void main(String[] args) {
        // 借助MethodHandles创建 方法查找 对象
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        // 创建 MethodType 方法类型 对象,该对象代表方法的返回类型和参数类型
        // 第一个参数是返回类型，后面的参数就是方法的参数类型
        MethodType methodType = 
            MethodType.methodType(String.class, char.class, char.class);
        try {
            // 通过MethodHandles.Lookup对象提供的方法查找方法句柄
            // 第一个参数是在String类中查找
            // 第二个参数表示要查找replace方法
            MethodHandle methodHandle = 
                lookup.findVirtual(String.class, "replace", methodType);
            // 通过方法句柄调用对应方法，并获得方法返回值
            String handleStr = (String) methodHandle.invoke("abc", 'a', 'c');
            System.out.println(handleStr);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
```

执行结果：

```java
cbc
```



## 使用步骤总结

总结一下使用MethodHandle的步骤：

1. 创建查找(MethodHandles.Lookup)
2. 创建方法类型(MethodType)
3. 查找方法句柄(MethodHandle)
4. 调用方法句柄



### 创建查找

通过MethodHandles提供的API，我们可以创建具有不同访问权限的查找对象。

创建提供对公共方法的访问的查找：

```java
MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
```

创建提供对任何访问权限的方法的查找：

```java
MethodHandles.Lookup lookup = MethodHandles.lookup();
```



### 创建方法类型

一个方法类型(MethodType)由返回类型和适当数量的参数类型组成。

使用如下：

```java
MethodType methodType =
    MethodType.methodType(String.class, char.class, char.class);
```

上面代码将String类型指定为返回类型，两个char类型指定为参数类型。

如果方法无返回值，或者无参数。那么在`MethodType.methodType()`方法传入对应参数为`void.class`



### 查找方法句柄

在创建完查找对象和方法类型对象之后，我们可以将方法类型对象传入查找对象的对应方法中，从而查找并获取对应的方法句柄。

1. 实例方法

   使用`MethodHandle.Lookup`对象的`findVirtual()`方法，可以为对象方法创建MethodHandle对象。

   ```java
   MethodType methodType = 
       MethodType.methodType(String.class, char.class, char.class);
   MethodHandle methodHandle = lookup.findVirtual(String.class, "replace", methodType);
   ```
   
2. 静态方法

   使用`findStatic()`方法可以查找静态方法的MethodHandle对象。

   ```java
   MethodType methodType = 
       MethodType.methodType(List.class, Object[].class);
   MethodHandle methodHandle = lookup.findStatic(Arrays.class, "asList", methodType);
   ```

3. 构造函数

   使用`findConstructor()`方法来访问构造函数。

   下面代码创建一个Integer类的构造函数的方法句柄，接收一个String为参数。

   ```java
   MethodType mt = MethodType.methodType(void.class, String.class);
   MethodHandle methodHandle = lookup.findConstructor(Integer.class, mt);
   ```

4. 字段

   利用MethodHandle，还可以访问字段。

   ```java
   public class Book {
   
       String id;
       String title;
   
       public Book(String id, String title) {
           this.id = id;
           this.title = title;
       }
   
   }
   ```
   
   ```java
   MethodHandle methodHandle = lookup.findGetter(Book.class, "title", String.class);
   String title = (String) methodHandle.invoke(new Book("1", "我与地坛"));
   ```
   
5. 私有方法

   利用`java.lang.reflect`的API，我们可以为私有方法创建方法句柄。

   为上面的Book类添加一个私有方法：

   ```java
   private String formatBook() {
       return id + ">" + title;
   }
   ```

   使用方法如下：

   ```java
   Method formatBookMethod = Book.class.getDeclaredMethod("formatBook");
   formatBookMethod.setAccessible(true);
   MethodHandle methodHandle = lookup.unreflect(formatBookMethod);
   String str = (String) methodHandle.invoke(new Book("1", "我与地坛"));
   ```



### 调用方法句柄

创建了方法句柄，即拥有了对应方法的引用。我们通过方法句柄就可以调用对应的方法。

MethodHandle提供了三种执行方法句柄的方法：`invoke()`、`invokeWithArguments()`、`invokeExact()`。

1. `invokeExact()`方法

   invokeExact()方法在调用时要求严格的类型匹配。

   ```java
   MethodHandle methodHandle = lookup.findVirtual(String.class, "replace", methodType);
   Object handleStr = methodHandle.invokeExact("abc", 'a', 'c');
   System.out.println(handleStr);
   ```

   上述代码执行后会报错：

   ```java
   java.lang.invoke.WrongMethodTypeException: expected (String,char,char)String but found (String,char,char)Object
   	at java.lang.invoke.Invokers.newWrongMethodTypeException(Invokers.java:298)
   	at java.lang.invoke.Invokers.checkExactType(Invokers.java:309)
   	at com.xgc.InvokeMethodHandle.main(InvokeMethodHandle.java:14)
   ```

   这是因为，不将`methodHandle.invokeExact("abc", 'a', 'c')`强制转型成String的话，在调用的时候该方法会以为返回值时Object类型，而不是String类型，所以会抛出异常。

2. `invoke()`方法

   跟`invokeExact()`方法不同，invoke方法允许更加松散的调用方式。它会尝试在调用的时候进行返回值和参数类型转换工作。

   转换工作是通过MethodHandle类的asType()方法来完成的。asType()方法是将当前方法句柄适配到新的HandleType上面，并产生一个新的方法句柄。

3. `invokeWithArguments()`方法

   `invokeWithArguments()`方法除了对参数和返回类型进行强制转换、装箱和拆箱外，它还允许传入一个包含了多个参数的数组或List对象。

