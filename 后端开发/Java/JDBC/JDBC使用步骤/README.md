---
title: JDBC使用步骤
tags:
  - JDBC
categories:
  - JavaSE
  - JDBC
date: 2021-05-29 21:25:39
---

## JDBC使用步骤(文字说明)

使用JDBC访问数据的步骤分为6步：

1. 加载驱动
2. 创建数据库连接
3. 创建Statement对象
4. 通过Statement对象执行SQL并获得结果集
5. 处理结果集
6. 关闭数据库资源



## JDBC代码示例

```java
public class JdbcTest {

    //解释
    // jdbc:数据库类型://ip地址:端口/数据库名称
    private static final String url = "jdbc:mysql://localhost:3306/notes";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            //STEP 1： 加载驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            //STEP 2： 创建连接
            conn = DriverManager.getConnection(url, "root", "123456");
            //STEP 3： 创建Statement对象
            stmt = conn.createStatement();
            //STEP 4： 通过Statement对象执行SQL并获得结果集
            String sql = "select username, age from user";
            ResultSet rs = stmt.executeQuery(sql);
            //STEP 5： 处理结果集
            while(rs.next()) {
                String username = rs.getString("username");
                int age = rs.getInt("age");
                System.out.println("username: " + username + " age: " + age);
            }
            //STEP 6： 关闭数据库资源
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
```


