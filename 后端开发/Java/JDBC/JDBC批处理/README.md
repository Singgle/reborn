---
title: JDBC批处理
tags:
  - JDBC
categories:
  - JavaSE
  - JDBC
date: 2021-05-29 22:31:52
---

JDBC支持批处理，可以减少通信的消耗，提高性能。

通过JDBC的批处理，我们不再像之前一样，每执行一条SQL语句就要提交一次。我们只需要在设置完需要提交的数据后，通过`PreparedStatement`对象的`executeBatch`方法同时将多条SQL同时提交，进而减少了通信的次数，从而提高了性能。

```java
Connection conn = DriverManager.getConnection(url, username, pass);
//关闭自动提交
conn.setAutoCommit(false);
//创建PreparedStatment对象
String sql = "insert into user values (?,?)";
PreparedStatement pstmt = conn.prepareStatement(sql);

//设置参数
pstmt.setString(1, "user1");
pstmt.setInt(2, 17);
pstmt.addBatch();

pstmt.setString(1, "user2");
pstmt.setInt(2, 17);
pstmt.addBatch();

pstmt.setString(1, "user3");
pstmt.setInt(2, 17);
pstmt.addBatch();

//提交上面3条语句
int[] counts = pstmt.executeBatch();
conn.commit();
```


