# tomcat配置文件server.xml详解

## 前言

server.xml是tomcat最重要的配置文件，**server.xml的每一个元素都对应了tomcat中的一个组件**。通过对xml文件中元素的配置，可以实现对tomcat中各个组件的控制。



## server.xml配置文件

server.xml配置文件位于$TOMCAT_HOME/conf目录下，内容如下。后文会结合该实例进行讲解

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Server port="8005" shutdown="SHUTDOWN">
  <Listener className="org.apache.catalina.startup.VersionLoggerListener" />
  <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
  <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
  <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
  <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />

  <GlobalNamingResources>
    <Resource name="UserDatabase" auth="Container"
              type="org.apache.catalina.UserDatabase"
              description="User database that can be updated and saved"
              factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
              pathname="conf/tomcat-users.xml" />
  </GlobalNamingResources>

  <Service name="Catalina">
    <Connector port="8081" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
      
    <Connector protocol="AJP/1.3"
               address="::1"
               port="8009"
               redirectPort="8443" />

    <Engine name="Catalina" defaultHost="localhost">
      <Realm className="org.apache.catalina.realm.LockOutRealm">
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>

      <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="localhost_access_log" suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>
    </Engine>
  </Service>
</Server>
```



## server.xml整体结构

```xml
<Server>
	<Service>
    	<Connector />
        
        <Engine>
        	<Host>
            	//<Context></Context>
            </Host>
        </Engine>
    </Service>
</Server>
```



## 核心组件

下面介绍各个核心组件的作用、特点以及配置方式等。



### Server

Server元素是顶层元素，代表整个tomcat容器，因此它只能有一个。**一个Server元素中可以存在一个或多个Service元素。**

```xml
<Server port="8005" shutdown="SHUTDOWN">
```

在上面的实例中，我们可以看到上面这一行。shutdown属性表示关闭Server的指令，port属性表示Server接收shutdown指令的端口号，设为 -1 可以禁掉该端口。

**Server的主要任务**：提供一个让客户端访问到Service集合的接口，同时维护它所包含的所有的Service的生命周期，包括如何初始化、如何结束服务、如何找到客户端想要访问的Service。



### Service

Service的作用就是：将Connector和Engine封装起来，对外提供服务。一个Service可以包含多个Connector，但只能存在一个Engine。



### Connector

Connector的主要功能是：接收连接请求，并将请求封装成Request和Response。然后，分配线程让Engine来处理这个请求，并将产生的Request和Response对象传给Engine。

通过配置Connector，可以控制请求Service的协议及端口号。

在上面的实例中，我们可以看到下面两个Connector：

```xml
<Connector port="8081" protocol="HTTP/1.1" 
           connectionTimeout="20000" redirectPort="8443" />
      
<Connector protocol="AJP/1.3" address="::1" port="8009" redirectPort="8443" />
```

1. 通过配置第1个Connector，客户端可以通过8080端口使用http协议访问tomcat。其中，protocol属性规定了请求的协议，port规定了请求的端口号，connectionTimeout表示连接的超时时间，redirectPort表示当强制使用https而请求是http时，重定向到端口为8443的Connector。

2. 通过配置第2个Connector，客户端可以通过8009端口号使用AJP协议访问tomcat。AJP协议负责和其他的HTTP服务建立连接。当tomcat与其他HTTP服务器集成时，需要使用到这个连接器。address属性表示： 如果Tomcat所在的主机有多个IP，这个值声明了用于监听请求的IP地址。这里的`::1`代表的IPv6的本地环回地址。

   之所以tomcat要与其他服务器集成，是因为tomcat可以用作Servet/JSP容器，但是对静态资源的处理速度比较慢，不如Apache和IIS等HTTP服务器。因此常常将tomcat与Apache等集成，前者做Servlet容器，后者处理静态资源，而AJP协议便负责tomcat和Apache的连接。



### Engine

Engine组件在Service组件中有且仅有一个。Engine是Service组件中的请求处理组件。Engine组件从一个或多个Connector中接收请求并处理，并将完成的响应返回给Connector，最终传递给客户端。

在上面例子中，我们可以看到下面语句：

```xml
<Engine name="Catalina" defaultHost="localhost">
```

其中，name属性表示此引擎的逻辑名称，用于日志和错误信息，在整个Server中唯一。defaultHost属性指定了默认的host名称，当发往本地的请求指定的host名称不存在时，使用defaultHost指定的host进行处理。因此，defaultHost的值，必须与Engine中的一个Host组件的name属性值匹配。



### Host

Host是Engine的子容器。Engine组件中可以包含一个或多个Host组件，每个Host组件代表Engine中的一个虚拟主机。Host组件至少有一个，且其中一个的name必须与defaultHost属性相匹配。

Host虚拟主机的作用是运行多个Web应用(一个Context代表一个Web应用)，并负责安装、展开、启动和结束每个Web应用。

Host组件代表的虚拟主机，对应了服务器中的一个网络名实体(如：`www.test.com`，或IP地址`116.25.25.25`)。为了使用户可以通过网络连接tomcat服务器，这个名字应该在DNS服务器上注册。

客户端通常使用主机名来表示它们希望连接的服务器。该主机名也会包含在HTTP请求头中。tomcat从HTTP头中提取出主机名，寻找名称匹配的主机。如果没有匹配，请求将发送到默认主机。

**Host的配置**

```xml
<Host name="localhost"  appBase="webapps" unpackWARs="true" autoDeploy="true">
```

name属性指定虚拟主机的主机名。

unpackWARs指定了是否将代表Web应用的WAR文件解压。如果为true，通过解压后的文件结构运行该Web应用。如果为false，直接使用WAR文件运行Web应用。

Host的autoDeploy和appBase属性，与Host内Web应用的自动部署有关。此外，本例中没有出现的xmlBase和deployOnStartup属性，也与Web应用的自动部署有关。在下一节(Context)中介绍。



### Context

#### Context的作用

Context元素代表在特定虚拟主机上运行的一个Web应用。每个Web应用对应WAR文件或WAR文件解压后的目录。

Context是Host的子容器，每个Host中可以定义多个Contex元素。

在上面的实例中，我们可以看到server.xml配置文件中并没有出现Context元素的配置。这是因为，tomcat开启了自动部署，Web应用没有在server.xml中配置静态部署，而是由tomcat通过特定的规则自动部署。



#### Web应用自动部署

**Host的配置**

要开启Web应用的自动部署，需要配置所在的虚拟主机。配置的方式就是前面提到的Host元素的deployOnStartup和autoDeploy属性。如果deployOnStartup和autoDeploy设置为true，则tomcat开启自动部署：当检测到新的Web应用或Web应用的更新时，会触发应用的部署(或重新部署)。二者的主要区别在于，deployOnStartup为true时，Tomcat在启动时检查Web应用，且检测到的所有Web应用视作新应用；autoDeploy为true时，Tomcat在运行时定期检查新的Web应用或Web应用的更新。除此之外，二者的处理相似。

通过配置deployOnStartup和autoDeploy可以开启虚拟主机自动部署Web应用；实际上，自动部署依赖于检查是否有新的或更改过的Web应用，而Host元素的appBase和xmlBase设置了检查Web应用更新的目录。

其中，appBase属性指定Web应用所在的目录，默认值是webapps，这是一个相对路径，代表Tomcat根目录下webapps文件夹。

xmlBase属性指定Web应用的XML配置文件所在的目录，默认值为conf/<engine_name>/<host_name>，例如第一部分的例子中，主机localhost的xmlBase的默认值是$TOMCAT_HOME/conf/Catalina/localhost。

**检查Web应用更新**

一个Web应用可能包括以下文件：XML配置文件，WAR包，以及一个应用目录(该目录包含Web应用的文件结构)；其中XML配置文件位于xmlBase指定的目录，WAR包和应用目录位于appBase指定的目录。

Tomcat按照如下的顺序进行扫描，来检查应用更新：

A、扫描虚拟主机指定的xmlBase下的XML配置文件

B、扫描虚拟主机指定的appBase下的WAR文件

C、扫描虚拟主机指定的appBase下的应用目录

**`<Context>`元素的配置**

Context元素最重要的属性是docBase和path，此外reloadable属性也比较常用。

docBase指定了该Web应用使用的WAR包路径，或应用目录。需要注意的是，**在自动部署场景下，docBase不在appBase目录中，才需要指定；如果docBase指定的WAR包或应用目录就在appBase中，则不需要指定**，因为Tomcat会自动扫描appBase中的WAR包和应用目录，指定了反而会造成问题。

path指定了访问该Web应用的上下文路径，当请求到来时，Tomcat根据Web应用的 path属性与URI的匹配程度来选择Web应用处理相应请求。例如，Web应用app1的path属性是”/app1”，Web应用app2的path属性是”/app2”，那么请求/app1/index.html会交由app1来处理；而请求/app2/index.html会交由app2来处理。如果一个Context元素的path属性为””，那么这个Context是虚拟主机的默认Web应用；当请求的uri与所有的path都不匹配时，使用该默认Web应用来处理。

但是，需要注意的是，**在自动部署场景下，不能指定path属性，path属性由配置文件的文件名、WAR文件的文件名或应用目录的名称自动推导出来**。如扫描Web应用时，发现了xmlBase目录下的app1.xml，或appBase目录下的app1.WAR或app1应用目录，则该Web应用的path属性是”app1”。如果名称不是app1而是ROOT，则该Web应用是虚拟主机默认的Web应用，此时path属性推导为””。

reloadable属性指示tomcat是否在运行时监控在WEB-INF/classes和WEB-INF/lib目录下class文件的改动。如果值为true，那么当class文件改动时，会触发Web应用的重新加载。在开发环境下，reloadable设置为true便于调试；但是在生产环境中设置为true会给服务器带来性能压力，因此reloadable参数的默认值为false。

下面来看自动部署时，xmlBase下的XML配置文件app1.xml的例子：

```
<Context docBase="D:\app1.war" reloadable="true"/>
```

在该例子中，docBase位于Host的appBase目录之外；path属性没有指定，而是根据app1.xml自动推导为”app1”；由于是在开发环境下，因此reloadable设置为true，便于开发调试。



## 参考资料

[详解Tomcat 配置文件server.xml](https://www.cnblogs.com/kismetv/p/7228274.html)

