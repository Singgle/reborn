# tomcat安装

## apt-get方式安装

> 本教程是在Ubuntu 18.04.4下安装



1. 安装tomcat

   ```bash
   sudo apt-get install tomcat9
   ```

2. 查看tomcat9安装到了哪里

   ```bash
   xgc@xgc-ubuntu:~$ whereis tomcat9
   tomcat9: /etc/tomcat9 /usr/libexec/tomcat9 /usr/share/tomcat9
   ```

   我们可以看到tomcat9的配置文件在`/etc/tomcat9`、启动文件在`/usr/share/tomcat9`

   那么我们webapps目录到哪里了：(在`/var/lib/tomcat9`)

   ```bash
   xgc@xgc-ubuntu:$ cd /var/lib/tomcat9/
   xgc@xgc-ubuntu:/var/lib/tomcat9$ ls
   conf  lib  logs  policy  webapps  work
   ```

3. tomcat命令

   tomcat安装之后默认是启动的

   ```bash
   sudo service tomcat9 start	#启动
   sudo service tomcat9 stop	#停止
   sudo service tomcat9 status #查看tomcat状态
   ```

   > 注：用apt-get安装tomcat之后，`/usr/share/tomcat9/bin`下面的脚本都不能用了，只能使用上面的方式来启动或停止tomcat。



## 压缩包解压安装

1. 下载tomcat压缩包

   ```bash
   wget https://mirrors.bfsu.edu.cn/apache/tomcat/tomcat-9/v9.0.39/bin/apache-tomcat-9.0.39.tar.gz
   ```

2. 复制压缩包到想要安装的目录

   ```bash
   sudo cp apache-tomcat-9.0.39.tar.gz /usr/local
   ```

3. 解压压缩包

   ```bash
   tar -zxvf /usr/local/apache-tomcat-9.0.39.tar.gz 
   ```

4. 启动tomcat

   进入到tomcat目录下的bin目录

   执行下面命令启动tomcat

   ```bash
   ./startup.sh 
   ```

5. 查看是否启动成功

   在浏览器输入http://localhost:8080/，出现下面页面，表示tomcat启动成功

   ![](img/20201015215701.png)

   