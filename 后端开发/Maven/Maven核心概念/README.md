# Maven核心概念

Maven 能够实现自动化构建是和它的内部原理分不开的，这里我们从 Maven 的九个核心概念入手， 看看 Maven 是如何实现自动化构建的

1. POM
2. 约定的目录结构
3. 坐标
4. 依赖管理 
5. 仓库管理
6. 生命周期
7. 插件和目标
8. 继承
9. 聚合



## POM

POM：Project Object Model 项目对象模型

pom.xml对应Maven工程是核心配置文件，与构建有关的一切配置都在这个文件中进行配置



## 约定的目录结构

```
Hello
|---src
|---|---main
|---|---|---java
|---|---|---resources
|---|---test
|---|---|---java
|---|---|---resources
|---pom.xml
```



## 坐标

Maven使用下面三个向量来唯一定位一个Maven工程

1. groupid：公司或组织域名倒序+项目名

   ```java
   <groupId>com.xgc.blog</groupId>
   ```

2. artifactid：模块名

   ```xml
   <artifactId>Login</artifactId>
   ```

3. version：版本

   ```xml
   <version>1.0.0</version>
   ```



Maven工程的坐标与仓库中路径的对应关系

```xml
<groupId>org.springframework</groupId>
<artifactId>spring-core</artifactId>
<version>4.0.0.RELEASE</version>
```

```
org/springframework/spring-core/4.0.0.RELEASE/spring-core-4.0.0.REALEASE.jar
```



## 依赖

### 依赖的范围

1. compile

   对主程序有效

   对测试程序有效

   参与打包

2. test

   对主程序无效

   对测试程序有效

   不参与打包

3. provided

   对主程序有效

   对测试程序有效

   不参与打包

   不参与部署



### 依赖的传递性

我们导入一个依赖的同时，该依赖的依赖也会被导入进来

> 只有scope为compile的依赖才会被导入进来



### 依赖的排除

如果我们在当前工程中引入了一个依赖 A，而 A 又依赖了 B，那么 Maven 会自动将 A 依赖的 B 引入当前工程，但是个别情况下 B 有可能是一个不稳定版，或对当前工程有不良影响。这时我们可以在引入 A 的时候将 B 排除。

```xml
<dependency>
     <groupId>com.xgc.blog</groupId>
     <artifactId>Login</artifactId>
     <version>0.0.1-SNAPSHOT</version>
     <type>jar</type>
     <scope>compile</scope>
     <exclusions>
         <exclusion>
             <groupId>commons-logging</groupId>
             <artifactId>commons-logging</artifactId>
         </exclusion>
     </exclusions>
</dependency>
```

通过这种方式，我们可以将Login依赖下面依赖的commons-logging依赖排除掉



### 依赖的原则

1. 路径最短者优先

   ![](img/image-20201028014859657.png)

2. 路径相同时先声明者优先

   ![](img/image-20201028014933200.png)

   这里“声明”的先后顺序指的是 dependency 标签配置的先后顺序。



### 统一管理所依赖 jar 包的版本

对同一个框架的一组 jar 包最好使用相同的版本。为了方便升级框架，可以将 jar 包的版本信息统一提取出来

1. 统一声明版本号

   ```xml
   <properties>
        <spring.version>4.1.1.RELEASE</spring.version>
   </properties>
   ```

2. 引用前面声明的版本号

   ```xml
   <dependencies>
       <dependency>
           <groupId>org.springframework</groupId>
           <artifactId>spring-core</artifactId>
           <version>${spring.version}</version>
       </dependency>
   </dependencies>
   ```

   





## 仓库

仓库的分类

1. 本地仓库：为当前本机电脑上的所有Maven工程服务。
2. 远程仓库
   - 私服：架设在当前局域网环境下，为当前局域网范围内的所有Maven工程服务。
   - 中央仓库：架设在Internet上，为全世界所有Maven工程服务。
   - 中央仓库镜像：架设在各个大洲，为中央仓库分担流量。减轻中央仓库的压力，同时更快的响应用户请求。



## 生命周期

Maven有三套相互独立的生命周期，分别是：

1. Clean Lifecycle：在进行真正的构建之前进行一些清理工作。
2. Default Lifecycle：构建的核心部分，编译，测试，打包，安装，部署等等。
3. Site Lifecycle：生成项目报告，站点，发布站点。 

它们是相互独立的，我们可以仅仅调用 clean 来清理工作目录，仅仅调用 site 来生成站点。当然也可以直接运行 mvn clean install site 运行所有这三套生命周期。

每套生命周期都由一组阶段(Phase)组成，我们平时在命令行输入的命令总会对应于一个特定的阶段。比如，运行 mvn clean，这个 clean 是 Clean 生命周期的一个阶段。



### Clean 生命周期

Clean 生命周期一共包含了三个阶段：

1. pre-clean 执行一些需要在 clean 之前完成的工作
2. clean 移除所有上一次构建生成的文件
3. post-clean 执行一些需要在 clean 之后立刻完成的工作



### Default 生命周期

Default 生命周期是 Maven 生命周期中最重要的一个，绝大部分工作都发生在这个生命周期中。这里，只解释一些比较重要和常用的阶段：

validate

generate-sources

process-sources

generate-resources

process-resources 复制并处理资源文件，至目标目录，准备打包。

compile 编译项目的源代码。

process-classes

generate-test-sources

process-test-sources

generate-test-resources

process-test-resources 复制并处理资源文件，至目标测试目录。

test-compile 编译测试源代码。

process-test-classes

test 使用合适的单元测试框架运行测试。这些测试代码不会被打包或部署。

prepare-package

package 接受编译好的代码，打包成可发布的格式，如 JAR。

pre-integration-test

integration-test

post-integration-test

verify

install 将包安装至本地仓库，以让其它项目依赖。

deploy 将最终的包复制到远程的仓库，以让其它开发人员与项目共享或部署到服务器上运行。



### Site 生命周期

1. pre-site 执行一些需要在生成站点文档之前完成的工作
2. site 生成项目的站点文档
3. post-site 执行一些需要在生成站点文档之后完成的工作，并且为部署做准备
4. site-deploy 将生成的站点文档部署到特定的服务器上

这里经常用到的是 site 阶段和 site-deploy 阶段，用以生成和发布 Maven 站点，这可是 Maven 相当强大的功能，Manager 比较喜欢，文档及统计数据自动生成，很好看。



## 继承

由于非 compile 范围的依赖信息是不能在“依赖链”中传递的，所以当有工程需要这个依赖只能单独配置。以junit为例，这是一个单元测试类，只在test范围有效。

这样，每个模块需要用到它的时候，都需要单独配置。这样，当我们需要将所有模块的junit版本升级，就需要到各个模块中修改。通过继承机制，我们可以将这样的依赖信息统一提取到父工程模块中进行统一管理。

操作如下：

1. 创建父项目

   创建父工程和创建一般的 Maven 工程操作一致，唯一需要注意的是：打包方式处要设置为 pom。

2. 父工程使用dependencyManagement标签统一管理

   ```xml
   <dependencyManagement>
       <dependencies>
           <dependency>
               <groupId>junit</groupId>
               <artifactId>junit</artifactId>
               <version>4.9</version>
               <scope>test</scope>
           </dependency>
       </dependencies>
   </dependencyManagement>
   ```

3. 在各个子工程中引用父工程

   ```xml
   <parent>
       <groupId>com.xgc</groupId>
       <artifactId>framework</artifactId>
       <version>0.0.1-SNAPSHOT</version>
       
       <!-- 以当前文件为基准查找父工程中pom.xml文件的相对路径 -->
   	<relativePath>../framework/pom.xml</relativePath>
   </parent>
   ```

4. 删除子工程中的重复信息

   此时如果子工程的 groupId 和 version 和父工程重复，那么子工程的 groupId 和 version 则可以删除。

5. 子工程导入被父工程管理的依赖

   ```xml
   <dependency>
       <groupId>junit</groupId>
       <artifactId>junit</artifactId>
   </dependency>
   ```

   此时，依赖的版本号可以省略，默认使用父工程的依赖版本。当我们在父工程修改被管理的依赖版本，此时所有子工程都会更新依赖版本。



## 聚合

将多个工程拆分为模块后，需要手动逐个安装到仓库后依赖才能够生效。修改源码后也需要逐个手动进 行 clean 操作。而使用了聚合之后就可以批量进行 Maven 工程的安装、清理工作。

操作如下：

在总的聚合工程中使用 modules/module 标签组合，指定模块工程的相对路径即可

```xml
<modules>
    <module>../Hello</module>
    <module>../HelloFriend</module>
    <module>../MakeFriends</module>
</modules>
```

