# 使用@RequiredArgsConstructor(onConstructor = @__(@Autowired))替代@Autowired注解

在我们写Controller或者Service层的时候，需要注入很多的mapper接口或者service接口，这时候就会写很多的@AutoWired注解，代码看起来就会很乱。



Lombok提供了一个注解：

`@RequiredArgsConstructor(onConstructor = @__(@Autowired))`

这个注解标注在类上，可以替代@Autowired注解。要注意的是，注入的mapper或service需要使用final修饰。

```java
private final UserService userService;
```

