# RowBounds分页原理

Mybatis通过传递RowBounds对象，可以进行数据库数据的分页操作。但是，该分页操作是对ResultSet结果集进行分页，也就是人们常说的逻辑分页，而不是物理分页。

RowBounds对象源码如下：

```java
public class RowBounds {

  public static final int NO_ROW_OFFSET = 0;
  public static final int NO_ROW_LIMIT = Integer.MAX_VALUE;
  public static final RowBounds DEFAULT = new RowBounds();

  private int offset;
  private int limit;

  public RowBounds() {
    this.offset = NO_ROW_OFFSET;
    this.limit = NO_ROW_LIMIT;
  }

  public RowBounds(int offset, int limit) {
    this.offset = offset;
    this.limit = limit;
  }

  public int getOffset() {
    return offset;
  }

  public int getLimit() {
    return limit;
  }

}
```

对数据库数据进行分页，依靠offset和limit两个参数，表示从第几条开始，取多少条数据。


下面来看看Mybatis是如何进行分页的：

`org.apache.ibatis.executor.resultset.DefaultResultSetHandler` 类的`handleRowValuesForSimpleResultMap()`方法源码：

```java
  private void handleRowValuesForSimpleResultMap(ResultSetWrapper rsw, ResultMap resultMap, ResultHandler<?> resultHandler, RowBounds rowBounds, ResultMapping parentMapping)
      throws SQLException {
    DefaultResultContext<Object> resultContext = new DefaultResultContext<Object>();
    // 跳到offset位置，准备读取
    skipRows(rsw.getResultSet(), rowBounds);
    // 读取limit条数据
    while (shouldProcessMoreRows(resultContext, rowBounds) && rsw.getResultSet().next()) {
      ResultMap discriminatedResultMap = resolveDiscriminatedResultMap(rsw.getResultSet(), resultMap, null);
      Object rowValue = getRowValue(rsw, discriminatedResultMap);
      storeObject(resultHandler, resultContext, rowValue, parentMapping, rsw.getResultSet());
    }
  }
  
    private void skipRows(ResultSet rs, RowBounds rowBounds) throws SQLException {
    if (rs.getType() != ResultSet.TYPE_FORWARD_ONLY) {
      if (rowBounds.getOffset() != RowBounds.NO_ROW_OFFSET) {
        // 直接定位
        rs.absolute(rowBounds.getOffset());
      }
    } else {
      // 只能逐条滚动到指定位置
      for (int i = 0; i < rowBounds.getOffset(); i++) {
        rs.next();
      }
    }
  }
```

说明，Mybatis的分页是对结果集进行的分页。



Mybatis的RowBounds只适用于数据量小的场景。在数据量大的场景中，可以借助第三方Mybatis分页插件来实现物理分页。



# 参考文章 

[Mybatis3.3.x技术内幕（十三）：Mybatis之RowBounds分页原理](https://my.oschina.net/zudajun/blog/671446)

