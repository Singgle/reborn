---
title: 如何执行批量处理？
tags:
  - MyBatis
categories:
  - MyBatis
date: 2021-05-30 12:24:37
---

`MyBatis`有三种批量插入数据的方式。

1. 循环插入。
2. foreach标签。
3. 批处理。



## 循环插入

### userMapper.xml

```xml
<mapper namespace="com.xgc.entity.User">
    <insert id="insertUser" parameterType="User">
        insert into user (username, age)
        values (#{username}, #{age})
	</insert>
</mapper>
```



### UserMapper

```java
public interface UserMapper {
    void insertUser(User user);
}
```



### UserService

```java
public class UserService {
    
    @Autowired
    private UserMapper userMapper;
    
    //批量插入User对象
    public String insertUsers(List<User> userList) {
        for(User user : userList) {
            userMapper.insertUser(user);
        }
    }
}
```



## foreach标签

### userMapper.xml

```xml
<mapper namespace="com.xgc.entity.User">
    <insert id="insertUsers">
        insert into user (username, age)
        values
        <foreach collection="userList" item="user" separator=",">
            (#{user.username}, #{user.age})
        </foreach>
	</insert>
</mapper>
```



### UserMapper

```java
public interface UserMapper {
    void insertUsers(List<User> userList);
}
```



## 批处理

### userMapper.xml

```xml
<mapper namespace="com.xgc.entity.User">
    <insert id="insertUser" parameterType="User">
        insert into user (username, age)
        values (#{username}, #{age})
	</insert>
</mapper>
```



### UserMapper

```java
public interface UserMapper {
    void insertUser(User user);
}
```



### UserService

```java
public void insertUsers(List<User> userList) {
    //SqlSession openSession(ExecutorType execType, boolean autoCommit);
    SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
    UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
    
    for(User user : userList) {
        userMapper.insertUser(user);
    }
    
    sqlSession.commit();
    sqlSession.clearCache();
}
```



## 小结

上面介绍了三种批量处理的方式，它们之间的区别如下：

- 从效率上来看：foreach标签 > 批处理 > 循环插入

但是，我们推荐使用的是**批处理**的方式。

理由如下：

1. 循环插入效率最低，当数据量很大的时候，程序就会出现“卡”的感觉。这会导致程序不断在for循环，无法执行后续代码，这是很不理想的。

2. foreach标签是执行速度最快的，但是我们也不推荐使用。它其实本质上是通过foreach标签，将数据拼接成一条SQL语句从而进行执行的。但是SQL长度是有限制的，当SQL的长度超过MySQL服务器的**max_allowed_packet**变量指定的长度，那么该条SQL语句就会无法执行。

   因此，foreach标签能够批量插入的数据，根据MySQL服务器的max_allowed_packet变量值的不同而不同。所以，foreach标签不推荐用于批量处理。
