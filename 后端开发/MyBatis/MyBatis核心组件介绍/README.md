---
title: MyBatis核心组件介绍
tags:
  - MyBatis
categories:
  - MyBatis
date: 2021-06-23 22:10:45
---



## MyBatis的执行流程及核心组件图

![img](README/epub_27563388_132.png)



## Configuration

Configuration对象描述MyBatis主配置信息，同时Mapper配置信息，类型别名，TypeHandler也会注册到Configuration中。当其他组件需要获取这些配置信息的时候，可以从中获取。

同时Configuration也提供了创建Executor、StatementHandler、ParameterHandler、ResultSetHandler的工厂方法。



## MappedStatement

MappedStatement是对mapper中SQL配置信息的描述，是对Mapper配置文件中insert、update、delete、select等标签，或者@Select、@Update注解配置信息的封装。



## SqlSession

SqlSession是MyBatis提供的用户层面的接口，用来完成数据增删改查操作。SqlSession是Executor的外观，目的是为了减少操作数据库的复杂度，向用户提供简单易懂的操作接口。



## Executor

Executor是MyBatis的执行器。MyBatis中所有对数据库的增删查改操作都是通过Executor来完成的。



## StatementHandler

StatementHandler是对JDBC中的Statement操作的封装。比如为Statement对象设置参数，调用Statement接口提供的方法与数据库交互。



## ParameterHandler

当MyBatis使用的Statement类型为PreparedStatement或CallableStatement类型时，ParameterHandler为Statement对象的参数占位符设置值。



## TypeHandler

TypeHandler是MyBatis中的类型处理器，用于处理Java类型和SQL类型的映射。比如：TypeHandler能够根据Java类型调用PreparedStatement或CallableStatement对象相应的setXXX()来设置值，并且能够根据Java类型调用ResultSet对象相应的getXXX()方法来获取SQL执行结果。



## ResultSetHandler

ResultSet封装了对JDBC中的ResultSet的操作。当执行的SQL语句类型为select时，会将查询结果封装成Java对象。
