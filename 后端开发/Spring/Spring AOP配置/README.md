# Spring AOP配置

## Spring AOP的使用

### XML配置

Spring提供两种方式来进行配置

- Schema-based

  每个增强类都需要实现接口或类

  在配置文件的`<aop:config>`配置

- AspectJ

  每个增强类不需要实现接口或类

  在配置文件的`<aop:config>`的子标签`<aop:aspect>`配置



#### Schema-based方式

##### 前置增强和后置增强

###### XML配置文件

applicationContext.xml配置

```xml
<!-- 配置增强类 -->
<bean id="mybefore" class="com.xgc.advice.MyBeforeAdvice"></bean>
<bean id="myafter" class="com.xgc.advice.MyAfterAdvice"></bean>

<!-- 配置切面 -->
<aop:config>
    <!-- 配置切点 -->
    <aop:pointcut expression="execution(* com.xgc.test.Demo.demo2())" id="mypoint"/>
    <!-- 增强 -->
    <aop:advisor advice-ref="mybefore" pointcut-ref="mypoint"/>
    <aop:advisor advice-ref="myafter" pointcut-ref="mypoint"/>
</aop:config>

<!-- 配置 Demo 类,测试使用 -->
<bean id="demo" class="com.xgc.test.Demo"></bean>
```



###### 前置增强类 MyBeforeAdvice

```java
public class MyBeforeAdvice implements MethodBeforeAdvice {
    
     /**
     * 重写MethodBeforeAdvice接口的before方法，即前置增强执行的代码
     * @param arg0  被调用的切点方法对象
     * @param arg1  传递给切点方法的参数
     * @param arg2  切点方法所在类的对象
     * @throws Throwable
     */
    @Override
    public void before(Method arg0, Object[] arg1, Object arg2) throws Throwable {
    	System.out.println("执行前置增强");
    }
}
```



###### 后置通知类 MyAfterAdvice

```java
public class MyAfterAdvice implements AfterReturningAdvice {
    
    /**
     * 后置增强执行代码
     * @param arg0  切点方法的返回值
     * @param arg1  切点方法对象
     * @param arg2  传递给切点方法的参数
     * @param arg3  切点方法所在类的对象
     * @throws Throwable
     */
    @Override
    public void afterReturning(Object arg0, Method arg1, Object[] arg2, Object arg3) throws Throwable {
    	System.out.println("执行后置增强");
    }
}
```



##### 异常通知

在 spring 中只有 AspectJ 方式提供了异常通知的办法.

如果希望通过 Schema-based 实现需要按照特定的要求自己编写方法.



###### 实现步骤

1. 新建一个类实现 ThrowsAdvice 接口
   - 必须自己写方法,且必须叫 afterThrowing
   - 有两种参数方式(必须是 1 个或 4 个)
   - 异常类型要与切点报的异常类型一致
2. 进行XML配置



###### MyThrow

```java
public class MyThrow implements ThrowsAdvice{
    // public void afterThrowing(Method m, Object[] args, Object target, Exception ex) 
    // {
    // 		System.out.println("执行异常通知");
    // }
    
    public void afterThrowing(Exception ex) throws Throwable {
    	System.out.println("执行异常通知，通过 Schema-based 方式");
    }
}
```



###### XML配置

```xml
<bean id="mythrow" class="com.xgc.advice.MyThrow"></bean>

<aop:config>
    <aop:pointcut expression="execution(* com.xgc.test.Demo.demo1())" id="mypoint"/>
    <aop:advisor advice-ref="mythrow" pointcut-ref="mypoint" />
</aop:config>

<bean id="demo" class="com.xgc.test.Demo"></bean>
```



##### 环绕通知

###### XML配置

```xml
<bean id="myarround" class="com.xgc.advice.MyArround"></bean>

<aop:config>
    <aop:pointcut expression="execution(* com.xgc.test.Demo.demo1())" id="mypoint"/>
    <aop:advisor advice-ref="myarround" pointcut-ref="mypoint" />
</aop:config>

<bean id="demo" class="com.xgc.test.Demo"></bean>
```



###### 环绕通知类

```java
public class MyArround implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation arg0) throws Throwable {
        System.out.println("环绕-前置");
        Object result = arg0.proceed();//放行,调用切点方法
        System.out.println("环绕-后置");
        return result;
    }
}
```



#### AspectJ方式

##### AspectJ方式使用增强

###### XML配置

```xml
<aop:config>
    <aop:aspect ref="myadvice">
        
        <aop:pointcut expression="execution(* com.xgc.test.Demo.demo1(String,int)) and args(name1,age1)" id="mypoint"/>
        
        <aop:pointcut expression="execution(* com.bjsxt.test.Demo.demo1(String)) and args(name1)" id="mypoint1"/>
        
        <aop:before method="mybefore" pointcut-ref="mypoint" arg-names="name1,age1"/>
        <aop:before method="mybefore1" pointcut-ref="mypoint1" arg-names="name1"/>
        
        <!-- <aop:after/> 后置通知,是否出现异常都执行 -->
        <aop:after method="myafter" pointcut-ref="mypoint"/>
        <!-- <aop:after-returing/> 后置通知,只有当切点正确执行时执行 -->
        <aop:after-returning method="myaftering" pointcut-ref="mypoint"/>
        
        <aop:after-throwing method="mythrow" pointcut-ref="mypoint"/>
        <aop:around method="myarround" pointcut-ref="mypoint"/>
    </aop:aspect>
</aop:config>
```



###### 增强类

```java
public class MyAdvice {
    public void mybefore(String name1, int age1){
    	System.out.println("前置"+name1 );
    }
    public void mybefore1(String name1){
    	System.out.println("前置:"+name1);
    }
    public void myaftering(){
    	System.out.println("后置 2");
    }
    public void myafter(){
    	System.out.println("后置 1");
    }
    public void mythrow(){
    	System.out.println("异常");
    }
    public Object myarround(ProceedingJoinPoint p) throws Throwable{
        System.out.println("执行环绕");
        System.out.println("环绕-前置");
        Object result = p.proceed();
        System.out.println("环绕后置");
        return result;
    }
}
```



##### 异常增强

###### XML配置类

```xml
<!-- 配置异常增强类 -->
<bean id="mythrow" class="com.xgc.advice.MyThrowAdvice"></bean>

<aop:config>
    <aop:aspect ref="mythrow">
        <aop:pointcut expression="execution(* com.xgc.test.Demo.demo1())" id="mypoint"/>
        <!--
			method: 当触发这个通知时,调用哪个方法
			throwing: 异常对象名,必须和通知中方法参数名相同(可以不在通知中声明异常对象)
		-->
        <aop:after-throwing method="myexception" pointcut-ref="mypoint" throwing="e1"/>
    </aop:aspect>
</aop:config>

<bean id="demo" class="com.xgc.test.Demo"></bean>
```



###### 异常增强类

```java
public class MyThrowAdvice {
    public void myexception(Exception e1){
    	System.out.println("执行异常增强"+e1.getMessage());
    }
}
```



### 注解配置

#### 配置注解扫描

配置xml，告诉Spring哪些包下有注解

```xml
<context:component-scan base-package="com.xgc.advice"></context>
```



#### 配置切面类

```java
@Component
@Aspect
public class MyAspect {
    
    //配置切点
    @Pointcut("execution(* com.xgc.test.Demo.demo1())")
    public void pointCut {}
    
    @Before("com.xgc.test.Demo.demo1()")
    public void mybefore() {
        System.out.println("前置");
    }
    
    @After("com.xgc.test.Demo.demo1()")
    public void myafter(){
		System.out.println("后置通知");
	}
    
    @AfterThrowing("com.xgc.test.Demo.demo1()")
	public void mythrow(){
		System.out.println("异常通知");
	}
    
    @Around("com.xgc.test.Demo.demo1()")
    public Object myarround(ProceedingJoinPoint p) throws Throwable{
        System.out.println("环绕-前置");
        Object result = p.proceed();
        System.out.println("环绕-后置");
        return result;
	}
    
}
```





## Spring AOP配置默认使用CGlib

Spring AOP默认使用JDK动态代理，通过配置可以实现默认使用CGLib动态代理。



### XML配置

```xml
<aop:aspectj-autoproxy proxy-target-class="true"></aop:aspectj-autoproxy>
```

`proxy-target-class`设置为`true`表示使用CGLib动态代理。设置为`false`(默认)表示使用JDK动态代理。

