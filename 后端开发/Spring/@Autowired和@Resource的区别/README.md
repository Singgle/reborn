---
title: '@Autowired和@Resource的区别'
tags:
  - Spring
categories:
  - Spring
date: 2021-07-10 23:38:38
---



## 联系

1. @Autowired和@Resource注解都可以用来bean对象的注入。
2. 它们都可以标注在变量和setter方法上面。



## 区别

1. @Autowired注解是Spring提供的注解，@Resource注解是J2EE提供的注解。
2. @Autowired默认使用byType方式注入，@Resource默认使用byName方式注入



## @Autowired注解的使用

@Autowired注解默认使用byType方式注入。当存在多个实现时，使用byName方式注入。

使用示例如下：

```java
@Autowired
UserService userService;
```

当没有指定name时，默认注入的是与变量名相同的bean。

我们也可以使用@Qualifier注解来指定name。

```java
@Autowired
@Qualifier(value = "userServiceImpl")
UserService userService;
```



## @Resource注解的使用

@Resource注解默认使用byName方式注入。当没有匹配的时候，使用byType方式注入。

```java
@Resource
UserService userService;
```

同时@Resource注解还有两个重要的属性name和type，可以用来显式指定使用byName还是byType方法进行注入。

```java
@Resource(name = "userServiceImpl")
UserService userService;
```

```java
@Resource(type = "UserServiceImpl")
UserService userService;
```

```java
@Resource(name = "userServiceImpl", type = "userServiceImpl")
UserService userService;
```

1. 只指定name属性：通过byName方式注入，没有匹配报错
2. 只指定type属性：通过byType方式注入，没有匹配或匹配多个报错
3. 同时指定name属性和type属性：同时匹配则成功，否则报错

