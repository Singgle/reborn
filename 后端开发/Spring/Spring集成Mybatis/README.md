# Spring集成Mybatis

## web.xml配置

```xml
<!-- 上下文参数 -->
<context-param>
    <param-name>contextConfigLocation</param-name>
    <!-- spring 配置文件 -->
    <param-value>classpath:applicationContext.xml</param-value>
</context-param>

<!-- 封装了一个监听器,帮助加载 Spring 的配置文件 -->
<listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener
    </listener-class>
</listener>
```



## applicationContext.xml

```xml
<!-- 数据源封装类. 数据源:获取数据库连接,spring-jdbc.jar 中-->
<bean id="dataSouce" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
    <property name="driverClassName" value="com.mysql.jdbc.Driver"></property>
    <property name="url" value="jdbc:mysql://localhost:3306/ssm"></property>
    <property name="username" value="root"></property>
    <property name="password" value="smallming"></property>
</bean>

<!-- 创建 SqlSessionFactory 对象 -->
<bean id="factory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <!-- 数据库连接信息来源于 dataSource -->
    <property name="dataSource" ref="dataSouce"></property>
</bean>

<!-- 扫描器相当于mybatis.xml中mappers下package标签,扫描com.xgc.mapper包后会给对应接口创建对象-->
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <!-- 要扫描哪个包 -->
    <property name="basePackage" value="com.xgc.mapper"></property>
    <!-- 和 factory 产生关系 -->
    <property name="sqlSessionFactory" ref="factory"></property>
</bean>

<!-- 由 spring 管理 service 实现类 -->
<bean id="airportService" class="com.xgc.service.impl.AirportServiceImpl">
    <property name="airportMapper" ref="airportMapper"></property>
</bean>
```

