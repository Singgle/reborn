# RedisTemplate写入Redis数据出现无意义乱码前缀\xac\xed\x00\x05

我们在使用RedisTemplate的时候会发现，在写入数据的时候，key和value会出现无意义的乱码前缀。

以字符串essay为例：在Redis中，这个值对应`\xac\xed\x00\x05t\x00\x05essay`

这个乱码前缀为什么会产生？有什么意义？



## 乱码前缀如何产生？

`org.springframework.data.redis.core.RedisTemplate`实例化需要序列化和反序列化组件。当我们不指定的时候，默认使用的是`JdkSerializationRedisSerializer`进行序列化，而`JdkSerializationRedisSerializer`最终使用的是Java原生的`java.io.ObjectOutputStream`对象进行序列化。

这个乱码前缀就是`java.io.ObjectOutputStream`在序列化的时候添加的。



## 这个乱码前缀有什么意义？

\x对应0x，是16进制的前缀。

\xac\xed对应是0xaced，是`ObjectOutputStream`的序列化魔数。(见`java.io.ObjectStreamConstants.STREAM_MAGIC`)

\x00\x05对应是5，是`ObjectOutputStream`的序列化版本。(见`java.io.ObjectStreamConstants.STREAM_VERSION`)

这里引出一个小问题：**为什么是/x00/x05而不是/x05？**

这是因为上面2个值write的时候采用的是short，占2个字节。



在上面的样我们可以看到\x05后面有个t。t是转化后的ASCII码值对应字符，对应16进制是0x74，是ObjectOutputStream分配给String类型标记(见`java.io.ObjectStreamConstants.TC_STRING`)

\x00\x05，表示数据的字节数。



## 为什么有些16进制\x表示，有些ASCII码值对应字符表示？

推测是因为有些16进制的ASCII码值对应的是正常的字符，可以显示出来。而有些不可以。



## 上面看到使用2字节存储数据字节数，如果字符长度超长会怎样？

2个字节表述数据字节数，即最大0xffff，对应10进制65535。

那么如果字符长度超过这个值怎么办？

经试验，

key长度65535时，乱码为`\xac\xed\x00\x05t\xff\xff`

key长度65545时，乱码为`\xac\xed\x00\x05|\x00\x00\x00\x00\x00\x01\x00\x09`

t上面说过，是ObjectOutputStream分配给String类型标记。

|也是ASCII码值对应字符，对应16进制是0x7c，是ObjectOutputStream分配给长字符串类型标记。(见`java.io.ObjectStreamConstants.TC_LONGSTRING`)

同时，表示数据字节数从2个字节变成了8个字节。



## 参考文章

[RedisTemplate写入Redis数据出现无意义乱码前缀\xac\xed\x00\x05_hunger_wang的博客-CSDN博客](https://blog.csdn.net/hunger_wang/article/details/118713579)

