# Spring AOP

AOP(Aspect Oriented Programming)，面向切面编程。



## Spring AOP核心概念

### 连接点(Join Point)

程序执行的某个特定位置。

Spring仅支持方法的连接点。例如方法调用前，方法调用后，方法异常抛出时，方法调用前后。



### 切点(Pointcut)

每个程序都有多个连接点。AOP通过**切点**来定位特定的连接点。



### 增强(Advice)

增强是织入到目标类连接点的一段程序代码。



### 引介(Introduction)

引介是一种特殊的增强，它为类添加一些属性和方法。



### 切面(Aspect)

切面由切点和增强组成。



### 织入(Weaving)

织入是将增强添加到目标类连接点的过程。



## Spring AOP实现方式

AOP实现有两种方式：静态代理和动态代理。

- **静态代理**是在编译阶段生成AOP代理类。

- **动态代理**是在程序运行期间生成AOP代理类。



Spring AOP采用动态代理来实现AOP。

Spring AOP的动态代理分为两类：JDK动态代理和CGLib动态代理。

那么Spring AOP用的JDK动态代理还是CGLib动态代理呢？

1. 默认使用JDK动态代理
2. 若被代理对象没有实现任何接口，则默认使用CGLib动态代理


