---
title: '@Resource注解name属性高级用法'
tags:
  - Spring
categories:
  - @Spring
date: 2021-07-15 21:48:55
---

name属性的值可以指定配置文件中的值，当配置文件不存在值时则使用默认值。

示例代码如下：



## Person接口及其实现类

```java
@Component
public interface Person {

	void run();
}
```

```java
@Component
public class Parent implements Person {

	@Override
	public void run() {
		System.out.println("parent run");
	}

}
```

```java
@Component
public class Child implements Person {

	@Override
	public void run() {
		System.out.println("child run");
	}

}
```



## @Resource的name属性用法

```java
@Component
public class TestResource {

	@Resource(name = "${person.name:child}")
	private Person person;

	public void run() {
		person.run();
	}

}
```



## 配置文件

```yml
person:
  name: parent
```


