# Spring的@Import注解四种使用方式

来介绍`@Import`注解的使用之前，我们先看源码：

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Import {

   /**
    * {@link Configuration @Configuration}, {@link ImportSelector},
    * {@link ImportBeanDefinitionRegistrar}, or regular component classes to import.
    */
   Class<?>[] value();

}
```
从注释中，我们可以看到`@Import`注解可以将`@Configuration`、ImportSelector、ImportBeanDefinitionRegistrar和普通的component类导入。



## 准备Spring环境

### Spring依赖
```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-context</artifactId>
  <version>5.3.7</version>
</dependency>
```



## 导入@Configuration类

### 需要导入的bean：Hello
```java
public class Hello {

    public void print() {
        System.out.println("hello word");
    }
}
```



### 配置类：HelloConfiguration

```java
@Configuration
public class HelloConfiguration {

    @Bean
    public Hello createHello() {
        return new Hello();
    }
}
```



### 启动类：SpringTestApplication

```
@Import(HelloConfiguration.class)
public class SpringTestApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringTestApplication.class);
        Hello hello = applicationContext.getBean(Hello.class);
        hello.print();
    }
}
```
启动之后，控制台输出如下：
```
hello word

Process finished with exit code 0
```
说明Hello对象确实通过`@Import`注解注入到了容器中。



## 导入ImportSelector

### ImportSelector实现类：HelloImportSelector
`ImportSelector`是一个接口，实现这个接口需要重写`selectImports`方法。

`selectImports`方法会返回一个String数组。这个数组包含的元素是需要被导入容器中的类的全限定名。

下面我们实现`ImportSelector`接口并重写`selectImports`方法，将`Hello`类的全限定名返回。

```java
public class HelloImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        List<String> classNameList = new ArrayList<>();
        classNameList.add("com.xgc.entity.Hello");
        return StringUtils.toStringArray(classNameList);
    }
}
```



### 启动类：SpringTestApplication

```java
@Import(HelloImportSelector.class)
public class SpringTestApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringTestApplication.class);
        Hello hello = applicationContext.getBean(Hello.class);
        hello.print();
    }
}
```
启动之后，控制台输出如下：
```
hello word

Process finished with exit code 0
```



## 导入ImportBeanDefinitionRegistrar

### ImportBeanDefinitionRegistrar实现类：HelloImportDefinitionRegistrar
```java
public class HelloImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(Hello.class);
        registry.registerBeanDefinition("hello", rootBeanDefinition);
    }
}
```



### 启动类：SpringTestApplication

```java
@Import(HelloImportBeanDefinitionRegistrar.class)
public class SpringTestApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringTestApplication.class);
        Hello hello = applicationContext.getBean(Hello.class);
        hello.print();
    }
}
```
启动之后，控制台输出如下：
```
hello word

Process finished with exit code 0
```



## 直接导入Bean

### 启动类：SpringTestApplication
```java
@Import(Hello.class)
public class SpringTestApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringTestApplication.class);
        Hello hello = applicationContext.getBean(Hello.class);
        hello.print();
    }
}
```
启动之后，控制台输出如下：
```
hello word

Process finished with exit code 0
```