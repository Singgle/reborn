# 使用BeanUtils.copyProperties进行对象之间的属性赋值

`org.springframework.beans.BeanUtils`有一个`copyProperties`方法可以进行对象之间属性的赋值，避免通过get、set方法一个一个属性的赋值。

示例代码：

```java
BeanUtils.copyProperties(source, target);
```

我们看到copyProperties有两个参数。第一个参数source表示源对象，第二个参数target表示目标对象。该方法的作用就是把 将源对象的属性拷贝到目标对象。