---
title: Spring注入Bean到List或Map中
tags:
  - Spring
categories:
  - Spring
date: 2021-07-15 00:30:50
---

Spring支持将bean注入到List或者Map中，具体代码如下：



## 接口

```java
public interface Parser {

	void parser();
}
```



## 一个接口的多个实现

```java
@Component
public class ParserA implements Parser {

	@Override
	public void parser() {
		System.out.println("ParserA");
	}

}
```

```java
@Component
public class ParserB implements Parser {

	@Override
	public void parser() {
		System.out.println("ParserB");
	}

}
```

```java
@Component
public class ParserC implements Parser{

	@Override
	public void parser() {
		System.out.println("ParserC");
	}

}
```



## Service类

```java
@Component
public class Decoder {

	@Autowired(required = false)
	private List<Parser> parsers = new ArrayList<>();

	public int getParserTypeSize() {
		return parsers.size();
	}

}
```

这样，在service层的Decoder类中，我们可以获得一个存储了不同解析器对象的list。根据实际情况的不同，我们可以使用不同的解析器对象。
