# Spring事务-使用TransactionProxyFactoryBean创建事务代理

在Spring1.X中，声明式事务使用TransactionProxyFactoryBean来配置事务代理Bean。如它类名所示，这是一个专门为目标Bean生成事务代理的工厂Bean。

每个TransactionProxyFactoryBean为一个目标Bean生成一个事务代理Bean，事务代理改写了目标Bean的方法，就是在目标Bean的方法执行之前开始事务，在目标Bean的方法执行结束之后提交事务，如果遇到特定异常则回滚。

TransactionProxyFactoryBean在创建事务代理之前，需要了解当前事务所处的环境，该环境属性通过PlatformTransactionManager实例(或其实现类的实例)传入，而相关事务规则则在Bean定义中给出。



## 没有事务控制的简单JDBC程序

下面给出一个简单的数据库操作程序，该程序插入两条数据，这两条数据完全相同，将违反唯一键约束。

```java
package com.xgc.dao.impl;

public class NewsDaoImpl implements NewsDao {
    private DataSource dataSource;
    public void setDataSource(DataSrouce dataSource) {
        this.dataSource = dataSource;
    }
    public void insert(String title, String content) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        template.update("insert into news_table values (....)");
        //两次相同的操作，将违反主键约束
        template.update("insert into news_table values (....)");
    }
}
```

现在，我们希望这两条语句，要么同时执行成功，要么执行失败。



## 创建事务代理

为了实现上面的操作，我们需要使用事务。我们可以通过在xml中使用TransactionProxyFactoryBean，为上面Bean创建了一个事务代理，从而实现事务。

```xml
<!-- 使用C3P0数据库连接池作为数据源 -->
<bean id="dataSource" 
    class="com.mchange.v2.c3p0.ComboPolledDataSource" destroy-method="close">
    <property name="driverClass" value="com.mysql.jdbc.Driver" />
    <property name="jdbcUrl" value="jdbc:mysql://localhost/test" />
    <property name="user" value="root" />
    <property name="password" value="root" />
    <property name="maxPoolSize" value="40" />
    <property name="minPoolSize" value="4" />
    <property name="initialPoolSize" value="10" />
    <!-- 指定数据库连接池的连接的最大空闲时间 -->
    <property name="maxIdleTime" value="20" />
</bean>

<!-- 配置JDBC数据源的局部事务管理器，使用DataSourceTransactionManager类，该类实现了
PlatformTransactionManager接口，是针对采用数据源连接的特定实现 -->
<bean id="transactionManager" 
    class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <!-- 配置TransactionManager时需要注入数据源引用 -->
    <property name="dataSource" ref="dataSource" />
</bean>

<!-- 下面这个是前面定义的业务Bean -->
<bean id="newsDao" class="com.abc.dao.impl.NewsDaoImpl">
    <!-- 为业务Bean注入属性 -->
    <property name="dataSource" ref="dataSource" />
</bean>

<bean id="newsDaoTransProxy" 
    class="org.springframework.transaction.interceptor.TransactionProxyFactoryBean">
    <!-- 为事务代理工厂Bean注入事务管理器 -->
    <property name="transactionManager" ref="transactionManager" />
    <!-- 要在哪个Bean上面创建事务代理对象 -->
    <property name="target" ref="newsDao" />
    <!-- 指定事务属性 -->
    <property name="transactionAttributes">
        <props>
            <prop key="*">PROPAGATION_REQUIRED</prop>
        </props>
    </property>
</bean>
```

这样，我们就实现了事务。当我们执行上面JDBC程序的时候，就会发现上面的两条插入数据的语句会都失败。第一条插入数据的语句不会往数据库插入数据。



## 参考文章

[Spring事务——使用TransactionProxyFactoryBean创建事务代理](https://my.oschina.net/itblog/blog/212989)