# 获取ClassLoader的方法

1. 获取当前类的类加载器

   ```java
   clazz.getClassLoader()
   ```

2. 获取当前线程的上下文类加载器

   ```java
   Thread.currentThread().getContextClassLoader()
   ```

3. 获取系统类加载器

   ```java
   ClassLoader.getSystemClassLoader()
   ```

4. 获取调用者的类加载器

   ```java
   DriverManager.getCallerClassLoder()
   ```

