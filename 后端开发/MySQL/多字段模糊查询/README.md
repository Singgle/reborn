# 多字段模糊查询

多字段模糊查询：一条记录中的其中几个字段之一，可以模糊匹配给定的关键字。

MYSQL语句如下：

```mysql
SELECT * FROM magazine
where CONCAT('title', 'tag', 'description') LIKE '%keyword%'
```

但是，只有这三个字段中有其中一个的值为NULL，那么返回的也是NULL。那么这一条记录很可能就会被错过，怎么处理？如下：

```mysql
SELECT * FROM magazine
where CONCAT(IFNULL('title', ''), IFNULL('tag', ''), IFNULL('description', ''))
LIKE '%keyword%'
```

这样就能实现多字段模糊查询