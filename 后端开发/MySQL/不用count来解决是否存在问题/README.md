# 不用count来解决是否存在问题

根据某一条件从数据库表中查询 有与没有， 只有两种状态，那**为什么在写SQL的时候，还要SELECT count(\*) 呢？**



## 目前多数人的写法

```sql
SELECT count(*) FROM table_name WHERE a = 1 AND b=2
```



```java
int nums = xxDao.countXxxxByXxx(params);
if(nums > 0) {
    //当存在时，执行这里的代码
} else {
    //当不存在时，执行这里的代码
}
```



## 优化方案

```sql
SELECT 1 FROM table_name WHERE a = 1 AND b = 2 LIMIT 1
```



```java
Integer exist = xxDao.existXxxxByXxx(params);
if( exist != NULL ) {
  //当存在时，执行这里的代码
} else {
  //当不存在时，执行这里的代码
}
```

SQL不再使用count，而是改用LIMIT 1，让数据库查询到一条就返回，业务代码中直接判断是否非空即可。



## 总结

根据查询条件查出来的条数越多，性能提升的越明显，在某些情况下，还可以减少联合索引的创建。

