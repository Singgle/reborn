# LAST_INSERT_ID()函数

 当我们将新行插入到具有`AUTO_INCREMENT`列的表中，MySQL会自动生成一个唯一的整数，并将其作为主键列的值。



首先，创建一个名为`tb1`的新表进行测试。在`tb1`表中，在`id`列上设置了`AUTO_INCREMENT`属性。

```mysql
USE testdb;

CREATE TABLE tb1 (
	id INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(250) NOT NULL
)
```

*其次*，向`tb1`表中插入一个新行记录。

```sql
INSERT INTO tbl(description)
VALUES('MySQL last_insert_id');
```

*第三步*，使用MySQL `LAST_INSERT_ID`函数来获取MySQL生成的最后一个插入ID的值。

```sql
mysql> SELECT LAST_INSERT_ID() as lastID;
+--------+
| lastID |
+--------+
|      1 |
+--------+
1 row in set
```



**需要注意的是**：如果使用单个[INSERT](http://www.yiibai.com/mysql/insert-statement.html)语句将多行插入到表中，则`LAST_INSERT_ID`函数将返回第一行的最后一个插入ID。

假设`AUTO_INCREMENT`列的当前值为`1`，并在表中插入`3`行。当您使用`LAST_INSERT_ID`函数获取最后一个插入`ID`时，将获得`2`而不是`4`。



`LAST_INSERT_ID` 函数基于客户端独立原则。这意味着特定客户端的`LAST_INSERT_ID`函数返回的值是该客户端生成的值。这样就确保每个客户端可以获得自己的唯一ID。