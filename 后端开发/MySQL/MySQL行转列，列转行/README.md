---
title: MySQL行转列，列转行
tags:
  - MySQL
categories:
  - MySQL
date: 2021-06-06 21:08:26
---

## MySQL行转列

现有学生成绩表记录如下：

![image-20210606200046340](README/image-20210606200046340.png)

查询数据并按以下格式显示：

![image-20210606201006634](README/image-20210606201006634.png)

SQL语句如下：

```mysql
select user_name,
	max(case course when '数学' then score end) '数学',
	max(case course when '语文' then score end) '语文',
	max(case course when '英语' then score end) '英语'
from score
group by user_name
```



## MySQL列转行

现有学生成绩表如下：

![image-20210606203134769](README/image-20210606203134769.png)

查询成如下格式：

![image-20210606204952463](README/image-20210606204952463.png)

SQL语句：

```mysql
select user_name, '语文' as course, ch_score as score
from score
union
select user_name, '数学' as course, math_score as score
from score
union
select user_name, '英语' as course, en_score as score
from score
order by user_name
```




