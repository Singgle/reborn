# where不能在join前面使用

```MySQL
select a.name, b.score from
student a
where a.id = '1'
inner join score b
on a.id = b.sid;
```

上面这条语句是错误的，where子句不能在select子句前面使用。

正确的语句如下：

```MySQL
select a.name, b.score from
student a
inner join score b
on a.id = b.sid
where a.id = '1';
```

如果说，我们一定要先查询出id为1的学生记录，然后再去连接其他表呢？

可以使用子查询来解决这个问题：

```
select a.name, b.score from
(select a.id, a.name 
from student a
where a.id = '1') a
inner join score b
on a.id = b.sid;
```