# MySQL 结果集分组并将其他列去重复值合并成一行

现在我们有这样一个需求：

就是将下面查询出来的结果集，按照id进行分组，并将name这一列的结果去重复值 放在一行显示。

```
mysql> select * from student;
+------+------+
| id| name |
+------+------+
|1 | 10|
|1 | 20|
|1 | 20|
|2 | 20|
|3 | 200 |
|3 | 500 |
+------+------+
6 rows in set (0.00 sec)
```

实现MySQL语句如下：

```mysql
select id, group_concat(distinct name)
from student
group by id;
```

结果如下：
```
+------+------+
| id| group_concat(distinct name) |
+------+------+
|1 | 10,20|
|2 | 20|
|3 | 200,500 |
+------+------+
```

## 扩展

### 结果不想去重

去掉上述MySQL语句中的 `distinct` 即可

### 指定分隔符

当我们不指定分隔符的时候，默认使用的是逗号 `,`

当然，我们也可以指定分隔符，假设我们想要以分号 `;` 进行分隔

```MySQL
select id, group_concat(name separator ';')
from student
group by id;
```

### 对name列进行排序

我们可以对name列进行排序，以name排倒序

```MySQL
select id, group_concat(name order by name desc)
from student
group by id;
```

## 参考文章

[mysql 结果集去重复值并合并成一行](https://blog.csdn.net/a355586533/article/details/52085905)

