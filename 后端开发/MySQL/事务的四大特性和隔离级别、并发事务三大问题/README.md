---
title: 事务的四大特性和隔离级别、并发事务三大问题
tags:
  - 事务
  - 数据库
categories:
  - MySQL
date: 2021-06-10 20:43:55
---



## 什么是事务？

事务是一个原子操作单位。一个事务内的操作要么全部成功，要么全部失败。



## 事务的四大特性

事务的四大特性就是ACID。

- Atomicity，原子性。事务内的操作要么全部成功，要么全部失败。
- Consistency，一致性。数据库的数据要么处于事务前，要么处于事务后的状态，不能同时存在新值和旧值。
- Isolation，隔离性。并行事务之间是互不影响的。
- Duration，持久性。事务提交之后，对数据的修改是持久的。



## 并发事务引发的三大问题

事务并发执行会引起三大问题：脏读、幻读、不可重复读。

- 脏读：一个事务读取到了另一个事务未提交的数据。(**读取到未提交数据**)

  例如，事务A修改了数据，但是还未提交。此时并行执行的事务B读取到了事务A修改的数据。然后，事务A因为发生了某些错误，造成了回滚。此时，事务B读取的数据就是脏数据。

- 不可重复读：在同一事务内，事务两次读取的数据不一致。(**同一条数据但不一致**)

  例如：事务A读取了一条数据之后，事务B对该数据进行修改并提交了。然后事务A再次读取这条记录，就会发现数据不一致。

- 幻读：在同一事务内，事务的两次读取的记录数不相同。(**记录数不一致**)

  例如：事务A使用一定条件进行查询，事务B插入或删除了符合条件的记录。那么事务A在第二次查询时，查询出的记录数就会不一致。



看到这里，我们就会很奇怪。不是说事务具有隔离性，并行事务执行互不影响吗？

不急，我们先来看看下面的事务隔离级别。



## 事务的隔离级别

事务有四个隔离级别

- 读未提交(Read Uncommitted)：在这个隔离级别下，一个事务可以读取到另一个事务的未提交数据。

  这就会造成上面的脏读、不可重复读、幻读。

- 读已提交(Read Commiteed)：在这个隔离级别下，一个事务可以读取到另一个事务的已提交数据，而不能读取未提交数据。

  因为一个事务不能读取到另一个事务的未提交数据。因此，可以避免脏读问题。

  但是，不可重复读和幻读问题依然会产生。因此，这无法阻止另一个事务对数据的修改、增加和删除等操作的提交。

- 可重复读(Repeatable Read)：在这个隔离级别下，一个事务在读取数据的时候，另一个事务不能修改该数据。

  这个隔离级别限制了事务对数据的修改，因此避免了事务的脏读，不可重复读问题。

  但是还是会存在幻读的问题。

- 序列化(Serializable)：在这个隔离级别下，事务是串行执行的，效率最低。

  事务串行执行，也不存在上述并行事务引发的问题，可以避免脏读、不可重复读、幻读问题。



> 到这里，我们就知道为什么并发事务会出现上述的3种问题。这是因为事务并不是完全遵循事务的隔离性。



> 划分事务隔离级别的原因是为了能够根据不同的场景选择不同的隔离级别。隔离级别越高的事务，数据库的性能就会越低。
