# mybatis中useGeneratedKeys和keyProperty的作用

背景：

最近在看队友的代码，发现个问题， 后觉是自己out了。在做关联表插入操作时，需要根据主表的主键id作详情表的属性值，最笨的方法就是，先插入主表，然后通过查询返回刚刚插入的主键id，继续添加详情表数据。下面介绍一下我从队友代码中get的新技能~



方案：

在mybatis的配置文件中，有个叫keyProperty和useGeneratedKeys的属性。

useGeneratedKeys 参数只针对 insert 语句生效，默认为 false。当设置为 true 时，表示如果插入的表以自增列为主键，则允许 JDBC 支持自动生成主键，并可将自动生成的主键返回。

keyProperty用于指定对应主键字段。

```xml
<insert id="addUser" parameterType="com.xgc.MyUser" keyProperty="uid" useGeneratedKeys="true">
    insert into user (uname,usex) values(#{uname},#{usex})
</insert>
```

执行

```java
MyUser user = new MyUser();
user.setUname("小南");
user.setUsex("男");
int add = userDao.addUser(user);
```





## 参考资料

转载自： [mybatis中useGeneratedKeys和keyProperty的作用](https://blog.csdn.net/fengkungui/article/details/82772302)

