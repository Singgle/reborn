# Mybatis批量保存

Mybatis中有两种批量保存的方式



**实体类TrainRecord结构如下：**

```java
public class TrainRecord implements Serializable {
    private static final long serialVersionUID = -1206960462117924923L; 
    private long id; 
    private long activityId; 
    private long empId; 
    private int flag; 
    private String addTime; 
    //setter and getter 
}
```



**对应的mapper.xml中定义如下：**

```xml
<resultMap type="TrainRecord" id="trainRecordResultMap"> 
    <id column="id" property="id" jdbcType="BIGINT" /> 
    <result column="add_time" property="addTime" jdbcType="VARCHAR" /> 
    <result column="emp_id" property="empId" jdbcType="BIGINT" /> 
    <result column="activity_id" property="activityId" jdbcType="BIGINT" /> 
    <result column="flag" property="status" jdbcType="VARCHAR" /> 
</resultMap> 
```



## 使用mybatis foreach标签

```xml
<insert id="addTrainRecordBatch" useGeneratedKeys="true" parameterType="java.util.List"> 
    <selectKey resultType="long" keyProperty="id" order="AFTER"> 
    SELECT 
    LAST_INSERT_ID() 
    </selectKey> 
    insert into t_train_record (add_time,emp_id,activity_id,flag) 
    values 
    <foreach collection="list" item="item" index="index" separator="," > 
    	(#{item.addTime},#{item.empId},#{item.activityId},#{item.flag}) 
    </foreach> 
</insert>
```

**foreach标签解释：**

foreach标签可以在SQL语句中进行迭代一个集合。foreach元素的属性主要有item，index，collection，open，seperator，close。

item表示集合中每一个元素进行迭代时的别名

index表示迭代的次数，从0开始

open表示语句以什么开始

separator表示在每次进行迭代之间以什么符号作为分隔符

close表示语句以什么结束

**在使用foreach的时候最关键的也是最容易出错的就是collection属性**，该属性是必须指定的，但是在不同情况 下，该属性的值是不一样的，主要有一下3种情况：

1. 如果传入的参数是单参数且参数类型为List，collection属性值为list
2. 如果传入的参数是单参数且参数类型是一个array数组的时候，collection的属性值为array
3. 如果传入的参数是多个的时候，我们就需要把它们封装成一个Map了，当然单参数也可以封装成map



## 使用mybatis ExecutorType.BATCH

Mybatis内置的ExecutorType有3种，默认为simple,该模式下它为每个语句的执行创建一个新的预处理语句，单条提交sql；而batch模式重复使用已经预处理的语句，并且批量执行所有更新语句，显然batch性能将更优； 但batch模式也有自己的问题，比如在Insert操作时，在事务没有提交之前，是没有办法获取到自增的id，这在某些情形下是不符合业务要求的。



```java
SqlSessionFactory sqlSessionFactory = getSqlSessionFactory;
SqlSession openSession = sqlSessionFactory.openSession(ExecutorType.BATCH);

EmployeeMapper mapper = openSession.getMapper(EmployeeMapper.class);
for(int i = 0; i < 100; i++) {
    mapper.add(new Employee(UUID.randomUUID().toString(), "name" + i));
}

openSession.commit();
```









## 参考资料

[MyBatis批量插入(insert)数据操作](https://www.jb51.net/article/87225.htm)