---
title: 什么是MySQL的ONLY_FULL_GROUP_BY模式？
tags:
  - MySQL
categories:
  - MySQL
date: 2021-06-11 22:05:56
---



## ONLY_FULL_GROUP_BY模式讲解

MySQL从版本5.7开始就默认开启了**ONLY_FULL_GROUP_BY**的sql_mode。

这个SQL模式是为了提供对SQL语句GROUP BY合法性的检查。

只有满足了这个SQL模式的规则的SQL才被MySQL认为是合法的SQL，否则就会报错，无法执行。

**SQL模式核心规则如下：**

select 、having或order by后面的非聚合列必须全部在group by后面存在。



## 实例证明

### 测试表 orders

```mysql
CREATE TABLE orders  (
  id int AUTO_INCREMENT PRIMARY KEY ,
  user_id int ,
  create_time datetime ,
  price int
);
```



### 测试数据

```mysql
INSERT INTO `orders`(`id`, `user_id`, `create_time`, `price`) VALUES (1, 123, '2021-06-10 17:32:39', 56);
INSERT INTO `orders`(`id`, `user_id`, `create_time`, `price`) VALUES (2, 123, '2021-06-09 17:33:04', 78);
INSERT INTO `orders`(`id`, `user_id`, `create_time`, `price`) VALUES (3, 124, '2021-06-08 17:33:19', 65);
INSERT INTO `orders`(`id`, `user_id`, `create_time`, `price`) VALUES (4, 125, '2021-06-08 17:33:35', 65);
```



### 证明

经过上面SQL执行之后的表如下：

![image-20210612193847075](README/image-20210612193847075.png)



要执行的SQL：(这条SQL，select后面存在group by后面不存在的非聚合列)

```mysql
select user_id, create_time from orders
group by user_id;
```

**在MySQL5.6版本执行结果：**

![image-20210612194321563](README/image-20210612194321563.png)

**在MySQL8.0版本执行结果：**

```
select user_id, create_time from orders
group by user_id
> 1055 - Expression #2 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'learntest.orders.create_time' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
> 时间: 0.001s
```

可以看到这条执行结果执行错误。
