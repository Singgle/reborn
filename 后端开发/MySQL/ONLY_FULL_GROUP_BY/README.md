# ONLY_FULL_GROUP_BY

在MySQL5.0版本的数据库进行如下的操作：

```mysql
mysql> select sno,sname from student;
+------+-------+
| sno  | sname |
+------+-------+
| 001  | tom   |
| 001  | tom2  |
+------+-------+
2 rows in set (0.00 sec)

mysql> select sno,sname from student group by sno;
+------+-------+
| sno  | sname |
+------+-------+
| 001  | tom   |
+------+-------+
1 row in set (0.01 sec)

mysql> select sno,group_concat(sname) from student group by sno;
+------+---------------------+
| sno  | group_concat(sname) |
+------+---------------------+
| 001  | tom,tom2            |
+------+---------------------+
1 row in set (0.01 sec)
```



上面的第二条语句，我们希望它的查询结果是会将tom和tom2同时查询查出来，即结果是第三条语句查询出来的结果。但在MySQL5.7.5之前的版本中，就会出现只显示tom,而没有tom2的效果。为了避免这种情况，MySQL5.7.5及其以上的版本中，默认开启了`ONLY_FULL_GROUP_BY`。



## `ONLY_FULL_GROUP_BY`的含义

对于GROUP_BY聚合操作，如果在SELECT中的列既没有在GROUP_BY中出现，本身也不是聚合列（使用SUM，ANG等修饰的列），那么这句SQL是不合法的，因为那一列是不确定的。

以上面的操作为例，当仅仅返回name字段的时候，MySQL是不确定到底返回tom还是tom2，所以应该对其进行额外的操作，比如连接起来一起显示。



也就是说，当我们开启了`ONLY_FULL_GROUP_BY`之后，上面的第二条语句`select sno,sname from student group by sno;`是不合法的，会报错。



## 参考资料

[ONLY_FULL_GROUP_BY](https://blog.csdn.net/qq_38234015/article/details/90017695)