# CASE WHEN THEN 用法

case具有两种格式：简单case函数和case搜索函数。



## 简单case函数

```mysql
case sex
	when '1' then '男'
	when '2' then '女'
	else '其他' end
```



## case搜索函数

```mysql
case when sex = '1' then '男'
	 when sex = '2' then '女'
	 else '其他' end
```



## 总结

简单case函数的写法相对比较简洁，但是和case搜索函数相比，功能方面会有限制，比如写判定式。

需要注意的是：case函数只返回第一个符合条件的值。



