---
title: MySQL查询最近一条记录的SQL（优化篇）
tags:
  - MySQL
  - SQL优化
categories:
  - MySQL
date: 2021-06-10 17:04:08
---



**问题：**

现有一张订单表orders，有用户、订单时间、金额。需要查询最近的一条订单时间。

![image-20210610183509667](README/image-20210610183509667.png)



**要查询最近一条记录的SQL最简单的写法如下：**

```mysql
select * from orders
order by create_time desc
limit 1
```

这条语句会先将a表的数据全部查询出来，按照时间倒序排序后，再取第一条。

这意味着这条语句需要遍历表中的全部数据，这效率是十分低下的。



**优化：**

为了加快SQL的效率，我们需要减少遍历的记录。

因此，我们可以对user_id进行分组，并借助max函数查询出每个用户最近的订单记录。

然后对每个用户最近的订单记录按 **创建时间**(要**注意**的是，这里指的是**已经查询出来数据的创建时间**，即max_time) 倒序排序，取第一条数据即可。

```mysql
select user_id, max(create_time) max_time
from orders
group by user_id
order by max_time desc
limit 1;
```

从上面的SQL语句可以看到，需要遍历的数据从全表缩减到用户数的范围了。

这样就加快了SQL的执行效率。

